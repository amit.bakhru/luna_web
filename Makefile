.PHONY: clean clean-build clean-pyc clean-out docs help
.DEFAULT_GOAL := help
SHELL = /bin/bash
IMAGE_NAME := luna-web
IMAGE_TAG := $(or ${DOCKER_TAG}, test)
VENV_DIR := ${PWD}/.venv

## help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)";echo;sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /---/;s/\\n/ /g;p;}" ${MAKEFILE_LIST}|LC_ALL='C' sort -f|awk -F --- -v n=$$(tput cols) -v i=19 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"%s%*s%s ",a,-i,$$1,z;m=split($$2,w," ");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;printf"\n%*s ",-i," ";}printf"%s ",w[j];}printf"\n";}'|more $(shell test $(shell uname) == Darwin && echo '-Xr')

## build the python virtual env for the project
venv:
	uv venv -p python3.12 --seed;
	uv pip install setuptools wheel pip ruff poetry-plugin-export poetry;
	if [ -f "pyproject.toml" ]; then ${VENV_DIR}/bin/poetry install; fi;
	uv pip install -e .
	${VENV_DIR}/bin/playwright install
	${VENV_DIR}/bin/playwright install-deps

## clean pyc files
clean:
	(for i in "*.py[co]" "[.]*cache" "*.egg*" "build" "dist*" "*test-reports" "[.]coverage*" \
	"coverage*" "o" "__pycache__"; \
	do find . -name "$$i" -not -path "./.venv/*" -not -path "./.scannerwork/*" -exec rm -rv {} + ; done)

## lint python files using black
lint:
	uv run --no-project ruff format --config ./ruff.toml src/ bin/ tests/

## run UI automation
test:
	uv run --no-project bin/runner.py --filepath="tests/soar/case_management/test_case_listing.py" --n=1 --tags="smoke"

## run UI performance
test-perf:
	uv run --no-project bin/performance_runner.py
