#!/usr/bin/env python3
import sys
import json
import time
import requests
import traceback
import subprocess
from pathlib import Path
from runner import Runner
from src.utils.helper import Helper
from report_generator import ReportGenerator


class Config:
    """Configuration Constants"""

    HEADFUL = False
    REMOTE_DEBUGGING_PORT = 9222
    NUM_OF_RUNS = 5

    # Paths
    PERFORMANCE_OUTPUT_PATH = Path("o/output/performance")
    LOG_FILE_PATH = PERFORMANCE_OUTPUT_PATH.joinpath("performance.log")
    LIGHTHOUSERC_PATH = PERFORMANCE_OUTPUT_PATH.joinpath("lighthouserc.json")
    SCRIPT_PATH = "src/performance/index.js"
    OUTPUT_REPORT_PATH = PERFORMANCE_OUTPUT_PATH.joinpath("report/individual")
    REPORT_TEMPLATE_PATH = "src/performance/report_template.html"
    CONFIG_FILE_PATH = Path("src/config.json")

    # Browser settings
    CDP_SESSION_URL = f"http://127.0.0.1:{REMOTE_DEBUGGING_PORT}"
    ARGS_LIST = [f"--remote-debugging-port={REMOTE_DEBUGGING_PORT}", "--no-sandbox"]

    # Test configuration
    DEFAULT_COMMAND = ["pytest", "-p", "no:logging", "-sv"]
    TEST_FILE_PATH = "tests/soar/test_performance.py"
    TEST_CLASS = "PerformanceTest"
    try:
        config_data = json.loads(CONFIG_FILE_PATH.read_text())
        SOAR_URL = config_data.get("servers", {}).get("soar", {}).get("url", None)
    except Exception as e:
        print(f"Error loading configuration: {e}")
        SOAR_URL = "https://app.stage.devo.com"


class PerformanceRunner:
    """Performance Runner"""

    def __init__(self):
        self.original_stdout = sys.stdout
        self.config = Config.config_data
        self._build_url_list()

    def _build_url_list(self):
        """Build the list of full URLs for testing"""
        soar_url = self.config.get("servers", {}).get("soar", {}).get("url", Config.SOAR_URL)
        self.url_list = [
            f"{soar_url}{url_item['path']}"
            for url_item in self.config.get("performance", {}).get("urls", [])
        ]

    def url_name_mapping(self, url):
        """Map URLs to their corresponding page names"""
        # Extract the path from the full URL
        soar_url = self.config.get("servers", {}).get("soar", {}).get("url", Config.SOAR_URL)
        if url.startswith(soar_url):
            path = url[len(soar_url) :]
            # Find matching URL in config
            for url_item in self.config.get("performance", {}).get("urls", []):
                if url_item["path"] == path:
                    return url_item["name"]
        return ""

    def get_flow_category_from_report_title(self, title):
        """Determine the category of a page based on its title"""
        for url_item in self.config.get("performance", {}).get("urls", []):
            if url_item["name"] == title:
                return url_item.get("category", None)
        return None

    def setup_environment(self):
        """Initialize directories and clear previous reports"""
        # Make sure Runner can handle Path objects
        try:
            Runner().clear_output_folder(Config.PERFORMANCE_OUTPUT_PATH)
        except Exception as e:
            print(f"Error clearing folder: {e}")
            Runner().clear_output_folder(str(Config.PERFORMANCE_OUTPUT_PATH))
        Config.PERFORMANCE_OUTPUT_PATH.mkdir(parents=True, exist_ok=True)

    def configure_lighthouse(self, chromium_path):
        """Configure and write lighthouse settings"""

        # Use SOAR URL from urls_config if available
        self.config["lighthouse"]["ci"]["collect"].update(
            {
                "url": self.url_list,
                "numberOfRuns": Config.NUM_OF_RUNS,
                "chromePath": chromium_path,
                "puppeteerScript": Config.SCRIPT_PATH,
                "puppeteerLaunchOptions": {"args": Config.ARGS_LIST},
                "headful": Config.HEADFUL,
            }
        )
        self.config["lighthouse"]["ci"]["upload"]["outputDir"] = str(Config.OUTPUT_REPORT_PATH)
        Config.LIGHTHOUSERC_PATH.write_text(json.dumps(self.config["lighthouse"]))

    def run_lighthouse_tests(self, log_file):
        """Execute Lighthouse tests with login handling"""
        data_file = Config.PERFORMANCE_OUTPUT_PATH.joinpath("data.txt")
        data_file.parent.mkdir(parents=True, exist_ok=True)
        data_file.write_text("false")

        process = subprocess.Popen(
            ["lhci", "autorun", f"--config={str(Config.LIGHTHOUSERC_PATH)}"],
            stdout=log_file,
            stderr=log_file,
            text=True,
        )
        # Wait for browser to open (max 10s)
        self._wait_for_browser()
        # Handle login
        login_process = subprocess.Popen(
            Config.DEFAULT_COMMAND
            + [
                f"--ws={Config.CDP_SESSION_URL}",
                f"{Config.TEST_FILE_PATH}::{Config.TEST_CLASS}::test_login",
            ],
            stdout=log_file,
            stderr=log_file,
            text=True,
        )
        login_process.wait()
        data_file.write_text("true")
        process.wait()

    def _wait_for_browser(self, max_attempts=10):
        """Wait for browser to be ready"""
        for _ in range(max_attempts):
            try:
                response = requests.get(f"{Config.CDP_SESSION_URL}/json")
                if response.status_code == 200:
                    return
            except Exception as e:
                print(e)
            time.sleep(1)

    def run(self):
        """Main execution flow"""
        self.setup_environment()

        # Make sure log file directory exists
        log_path = Config.LOG_FILE_PATH
        log_path.parent.mkdir(parents=True, exist_ok=True)

        with open(log_path, "w") as log_file:
            # Redirect stdout to log file
            original_stdout = sys.stdout
            sys.stdout = log_file

            try:
                # Run lighthouse tests
                chromium_path = Helper.get_chromium_path()
                self.configure_lighthouse(chromium_path)
                self.run_lighthouse_tests(log_file)
            except Exception as e:
                print(f"Error running lighthouse tests: {e}")
                traceback.print_exc(file=log_file)
            finally:
                # Restore stdout
                sys.stdout = original_stdout

        report_generator = ReportGenerator(
            str(Config.OUTPUT_REPORT_PATH), str(Config.REPORT_TEMPLATE_PATH), self
        )
        report_generator.generate()


if __name__ == "__main__":
    runner = PerformanceRunner()
    runner.run()
