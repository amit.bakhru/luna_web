import os
import json
from datetime import datetime
from pathlib import Path

Config = json.loads(Path("src/config.json").read_text())


class Colors:
    RED = "#CC0000"
    ORANGE = "#FFA500"
    GREEN = "#008800"


class ReportGenerator:
    def __init__(self, output_path, report_template_path, runner):
        self.output_path = output_path
        self.report_template_path = report_template_path
        self.runner = runner  # Store reference to PerformanceRunner instance
        self.audit_metrics = [
            {"marker": '"id":"performance","score":'},
            {"marker": '"id":"accessibility","score":'},
            {"marker": '"id":"best-practices","score":'},
        ]
        self.performance_metrics = [
            {"marker": "First Contentful Paint"},
            {"marker": "Largest Contentful Paint"},
            {"marker": "Total Blocking Time"},
            {"marker": "Cumulative Layout Shift"},
            {"marker": "Speed Index"},
        ]
        self.metric_thresholds = Config["metrics"]["thresholds"]

    def consolidate_reports(self):
        """Keep only median reports, rename them according to URL mapping, and return the ReportName - EndPoint mapping"""
        name_endpoint_mapping = {}
        median_reports = ["manifest.json"]
        manifest_path = f"{self.output_path}/manifest.json"

        with open(manifest_path, "r") as f:
            reports = json.load(f)

        for report in reports:
            if report["isRepresentativeRun"]:
                name = self.runner.url_name_mapping(report["url"])
                if name:
                    name_endpoint_mapping[name] = report["url"].split("#")[1]
                    old_path = f"{self.output_path}/{report['htmlPath'].split('/')[-1]}"
                    new_path = f"{self.output_path}/{name}.html"
                    os.rename(old_path, new_path)
                    median_reports.append(f"{name}.html")

        # Remove non-median reports
        for report in os.listdir(self.output_path):
            if report not in median_reports:
                os.remove(f"{self.output_path}/{report}")

        return name_endpoint_mapping

    def extract_metrics(self):
        """Extract performance metrics from HTML reports"""
        lighthouse_data = []
        chromium_version = "0.0.0.0"

        for report in os.listdir(self.output_path):
            if not report.endswith(".html"):
                continue

            with open(f"{self.output_path}/{report}", "r") as f:
                content = f.read()

            metrics = [report.split(".")[0]]  # Start with report name

            # Extract audit scores
            for audit in self.audit_metrics:
                score = self._extract_score(content, audit["marker"])
                metrics.append(f"{score}%")

            # Extract performance metrics
            for metric in self.performance_metrics:
                value = self._extract_metric(content, metric["marker"])
                metrics.append(value)

            lighthouse_data.append(metrics)

            # Extract Chromium version (only need to do this once)
            if chromium_version == "0.0.0.0":
                chromium_version = self._extract_chromium_version(content)

        return lighthouse_data, chromium_version

    def organize_data_by_category(self, data):
        """Organize performance data by category and sort in descending order based on SI metric"""
        categories = {"case_management": [], "automations": [], "other": []}

        for entry in data:
            category = self.runner.get_flow_category_from_report_title(entry[0])
            if category:
                categories[category].append(entry)

        # Sort entries
        for category, entries in categories.items():
            categories[category] = sorted(
                entries, key=lambda x: float(x[len(x) - 1][:-1]), reverse=True
            )

        # Add headers
        result = []
        for category, entries in categories.items():
            if entries:
                header = [category.replace("_", " ").title()] + [""] * 8
                result.extend([header] + entries)

        return result

    def generate_html_rows(self, data, name_endpoint_mapping):
        """Generate HTML rows with appropriate styling"""
        rows = []
        metric_types = [
            "performance",
            "accessibility",
            "best-practices",
            "fcp",
            "lcp",
            "tbt",
            "cls",
            "si",
        ]

        for row in data:
            if row[1] == "" and row[8] == "":  # Header row
                cells = [f'<td style="border: none;"><b>{row[0]}</b></td>']
                cells.extend([f'<td style="border: none;">{val}</td>' for val in row[1:]])
                rows.append(f'<tr style="background-color: #D9D9D9;">{" ".join(cells)}</tr>')
            else:  # Data row
                cells = [
                    f'<td><b><a href="./individual/{row[0]}.html" target="_blank">{name_endpoint_mapping[row[0]]}</a></b></td>'
                ]
                for i, value in enumerate(row[1:], 1):
                    color = self._get_color_for_metric(
                        metric_types[i - 1], self._parse_value(value)
                    )
                    cells.append(f'<td style="color:{color};"><b>{value}</b></td>')
                rows.append(f"<tr>{' '.join(cells)}</tr>")

        return "\n".join(rows)

    def generate(self):
        """Generate the final performance report"""
        name_endpoint_mapping = self.consolidate_reports()
        data, chromium_version = self.extract_metrics()
        organized_data = self.organize_data_by_category(data)
        rows_html = self.generate_html_rows(organized_data, name_endpoint_mapping)

        # Generate timestamp
        timestamp = datetime.now().strftime("%d-%b-%Y at %H:%M:%S")

        # Read template and inject data
        with open(self.report_template_path, "r") as f:
            template = f.read()

        report = template.replace("<!-- datetime -->", f"Report generated on {timestamp}")
        report = report.replace("<!-- chromiumversion -->", chromium_version)
        report = report.replace("<!-- data -->", rows_html)

        # Write final report
        with open(f"{Path(self.output_path).parent}/ui_performance_report.html", "w") as f:
            f.write(report)

    def _extract_score(self, content, marker):
        """Extract and format score from report content"""
        temp = content[content.index(marker) + len(marker) :]
        return str(int(float(temp[: temp.index("}")]) * 100))

    def _extract_metric(self, content, marker):
        """Extract metric value from report content"""
        temp = content[content.index(marker) :]
        temp = temp[temp.index('"displayValue":"') + len('"displayValue":"') :]
        return temp[: temp.index('","')].replace("\xa0", "")

    def _extract_chromium_version(self, content):
        """Extract Chromium version from report content"""
        marker = "Chrome/"
        temp = content[content.index(marker) + len(marker) :]
        return temp[0 : temp.index(" ")]

    def _parse_value(self, value):
        """Convert string metric value to numeric"""
        if not value or value == "":
            return -1

        value = value.replace("ms", "").replace("s", "").replace("%", "").replace(",", "")
        return float(value) if "." in value else int(value)

    def _get_color_for_metric(self, metric, value):
        """Determine color based on metric value and thresholds"""
        if value == -1:
            return Colors.GREEN

        thresholds = self.metric_thresholds.get(metric, [])
        for (min_val, max_val), color in zip(thresholds["ranges"], thresholds["colors"]):
            if min_val <= value <= float(max_val):
                return color

        return Colors.RED
