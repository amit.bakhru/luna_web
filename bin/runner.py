import os
import re
import sys
import ast
import copy
import html
import json
import shutil
import requests
import subprocess
import multiprocessing
from pathlib import Path
from typing import List, Dict
from src.utils.helper import Helper
from datetime import datetime, timedelta
from dataclasses import dataclass, field
from luna.framework.common.logger import LOGGER

Master_Config = json.loads(Path("src/config.json").read_text())


@dataclass
class Config:
    """Configuration settings for test runner"""

    DEFAULT_COMMAND: List[str] = field(
        default_factory=lambda: ["pytest", "-p", "no:logging", "-sv"]
    )
    IGNORED_FILES: List[str] = field(default_factory=lambda: ["tests/soar/test_performance.py"])
    OUTPUT_FOLDER: Path = Path("o/output/ui")
    TESTS_FOLDER: Path = Path("tests")
    LOGS_FOLDER_NAME = str = "o"
    REPORT_FOLDER: Path = OUTPUT_FOLDER / "report"
    PROCESSES: int = 1


@dataclass
class Colors:
    """ANSI color codes for terminal output"""

    GREEN: str = "\033[32m"
    BOLD: str = "\033[1m"
    UNDERLINE: str = "\033[4m"
    RESET: str = "\033[0m"


@dataclass
class TestCase:
    """Test case data structure"""

    name: str
    tags: List[str]
    class_name: str
    path: str


class Runner:
    """
    Test runner for executing and managing automated tests.

    Usage:
        python runner.py -c --filepath="tests/soar/test_case_listing.py" --tags="sanity smoke" --n=5
        python runner.py --folderpath="tests/soar" --n=5

    Args:
        -c: Don't clear previous outputs
        --n: Number of parallel processes
        --filepath: Specific test file to run
        --folderpath: Directory of test files to run
        --tags: Space-separated test tags to run
    """

    def __init__(self):
        self.clear_prev_outputs: bool = True
        self.tags_enabled: bool = False
        self.processes: int = Config.PROCESSES

        # Test execution settings
        self.run_command: List[str] = []
        self.file_path: str = ""
        self.folder_path: str = ""
        self.tags_list: List[str] = []

        # Test collection
        self.class_name: str = ""
        self.current_file_path: str = ""
        self.artifacts_config = Master_Config["artifacts"]
        self.soar_url = Master_Config["servers"]["soar"]["url"]
        self.env_dict: dict = dict()
        self.case_data_list: List[TestCase] = []
        self.test_case_list: List[TestCase] = []
        self.test_case_skipped_list: List[TestCase] = []

        # Test results
        self.reports: List[str] = []
        self.test_duration: List[str] = []
        self.test_artifacts: set[str] = set()
        self.test_results: Dict[str, Dict] = {}
        self.passed: int = 0
        self.failed: int = 0
        self.total_tests: int = 0

    def clear_output_folder(self, output_folder=Config.OUTPUT_FOLDER) -> None:
        """Clear previous test outputs if enabled"""
        if not self.clear_prev_outputs:
            return

        try:
            if Path(output_folder).exists():
                for item in output_folder.iterdir():
                    if item.is_dir():
                        shutil.rmtree(item)
                    else:
                        item.unlink()
            print(f"{Colors.GREEN}{Colors.BOLD}Previous outputs cleared{Colors.RESET}")
        except Exception as e:
            print(f"Error clearing output folder: {e}")

    def clear_logs_folder(self) -> None:
        """Clear previous test logs"""
        try:
            for root, _, _ in os.walk(Config.TESTS_FOLDER):
                if os.path.basename(root) == Config.LOGS_FOLDER_NAME:
                    shutil.rmtree(root)
            print(f"{Colors.GREEN}{Colors.BOLD}Previous logs cleared{Colors.RESET}")
        except Exception as e:
            print(f"Error clearing logs folder: {e}")

    def parse_args(self) -> None:
        """Parse command line arguments"""
        args = sys.argv[1:]
        for arg in args:
            match arg:
                case "-c":
                    self.clear_prev_outputs = False
                case _ if "--filepath=" in arg:
                    self.file_path = arg.split("=", 1)[1]
                    self.extract_data_from_file()
                case _ if "--folderpath=" in arg:
                    self.folder_path = arg.split("=", 1)[1]
                    self.extract_data_from_folder()
                case _ if "--tags=" in arg:
                    self.tags_enabled = True
                    self.tags_list = arg.split("=", 1)[1].split()
                case _ if "--n=" in arg:
                    self.processes = int(arg.split("=", 1)[1])

    def extract_data_from_file(self) -> None:
        """Extract test data from specified file"""
        for file_path in self.file_path.split():
            self.current_file_path = file_path
            content = Path(file_path).read_text()
            self.class_name = self._extract_class_name(content)
            self._extract_test_methods(content)

    def extract_data_from_folder(self) -> None:
        """Extract test data from all test files in folder"""
        test_files = [str(path) for path in Path(self.folder_path).rglob("test_*.py")]
        [test_files.remove(file) for file in Config().IGNORED_FILES]
        self.file_path = " ".join(test_files)
        self.extract_data_from_file()

    def _extract_class_name(self, content: str) -> str:
        """Extract test class name from file content"""
        if match := re.search(r"class\s+([A-Za-z0-9_]+)\s*\(", content):
            return match.group(1)
        return ""

    def _extract_test_methods(self, content: str) -> None:
        """Extract test methods and their tags"""
        test_methods = re.finditer(r"def (test_[^(]+)", content)
        prev_end = 0

        for match in test_methods:
            method_name = match.group(1)
            method_start = match.start()
            section = content[prev_end:method_start]
            tags = re.findall(r"@pytest\.mark\.(\w+)", section)

            self.case_data_list.append(
                TestCase(
                    name=method_name,
                    tags=tags,
                    class_name=self.class_name,
                    path=self.current_file_path,
                )
            )

            prev_end = method_start

    def process_tags(self) -> None:
        """Process and filter tests based on tags"""
        for case in self.case_data_list:
            if self.tags_enabled and set(self.tags_list) & set(case.tags):
                self.test_case_list.append(case)
            elif not self.tags_enabled and "skip" not in case.tags:
                self.test_case_list.append(case)
            elif not self.tags_enabled and "skip" in case.tags:
                self.test_case_skipped_list.append(case)

    def setup_run_command(self) -> None:
        """Construct pytest command with appropriate flags"""
        self.run_command = [
            *Config().DEFAULT_COMMAND,
            "--self-contained-html",
            f"--html={Config.REPORT_FOLDER}/{{report_name}}.html",
            f"{{file_path}}::{{class_name}}::{{test_method}}",
        ]

    def run_tests(self) -> None:
        """Execute tests in parallel"""
        if not self.test_case_list:
            return

        with multiprocessing.Pool(self.processes) as pool:
            pool.map(self._execute_test, self.test_case_list)
            print(f"{Colors.GREEN}{Colors.BOLD}Tests completed{Colors.RESET}")

    def _execute_test(self, test_case: TestCase) -> None:
        """Execute single test"""
        cmd = [
            arg.format(
                file_path=test_case.path,
                class_name=test_case.class_name,
                test_method=test_case.name,
                report_name=test_case.name,
            )
            for arg in self.run_command
        ]
        try:
            subprocess.run(cmd, check=True)
        except Exception as e:
            print(e)

    def display_test_info(self) -> None:
        """Display collected and skipped tests"""

        def print_test_list(title: str, tests: List[TestCase]) -> None:
            print(
                f"{Colors.GREEN}{Colors.BOLD}{Colors.UNDERLINE}{title}: {len(tests)}{Colors.RESET}"
            )
            for test in tests:
                print(f"{Colors.GREEN}{Colors.BOLD}{test.name}{Colors.RESET}")

        print_test_list("Tests collected", self.test_case_list)
        print("\n")
        print_test_list("Tests skipped", self.test_case_skipped_list)

    def run(self) -> None:
        """Main execution flow"""
        self.clear_logs_folder()
        self.parse_args()
        self.clear_output_folder()
        self.process_tags()
        self.setup_run_command()
        self.display_test_info()
        self.run_tests()
        self.generate_report()

    def get_all_reports(self):
        self.reports = os.listdir(Config.REPORT_FOLDER)

    def extract_test_results(self, report, tests=True):
        result = json.loads(html.unescape((re.search('data-jsonblob="([^"]*)"', report)).group(1)))
        test_key = list(result["tests"].keys())[0]
        test_method = (test_key.split("::"))[2]
        # Test Status
        test_status = result["tests"][test_key][0]["result"]
        extras = []
        if self.artifacts_config["videos"]["enabled"] and (
            (self.artifacts_config["videos"]["onlyOnFailure"] and test_status == "Failed")
            or (not self.artifacts_config["videos"]["onlyOnFailure"])
        ):
            extras.append(
                {
                    "name": test_method,
                    "format_type": "video",
                    "content": f"../videos/{test_method}.webm",
                }
            )
            self.test_artifacts.add(f"{test_method}.webm")
        if self.artifacts_config["screenshots"]["enabled"] and (
            (self.artifacts_config["screenshots"]["onlyOnFailure"] and test_status == "Failed")
            or (not self.artifacts_config["screenshots"]["onlyOnFailure"])
        ):
            extras.append(
                {
                    "name": test_method,
                    "format_type": "image",
                    "content": f"../screenshots/{test_method}.png",
                }
            )
            self.test_artifacts.add(f"{test_method}.png")
        result["tests"][test_key][0]["extras"] = extras
        if test_status == "Passed":
            self.passed += 1
        elif test_status == "Failed":
            self.failed += 1
        # Test Duration
        self.test_duration.append(result["tests"][test_key][0]["duration"])
        # Remove Links
        links_list = result["tests"][test_key][0]["resultsTableRow"]
        links_list.pop()
        result["tests"][test_key][0]["resultsTableRow"] = links_list

        if tests:
            return test_key, result["tests"][test_key]
        else:
            ENV_INCLUDE_LIST = ["Python", "Platform", "CI_PIPELINE_ID"]
            _result = copy.deepcopy(result)
            for key in _result["environment"]:
                try:
                    if key not in ENV_INCLUDE_LIST:
                        del result["environment"][key]
                except:
                    pass
            result["renderCollapsed"] = ["passed", "failed", "skipped"]
            result["title"] = "Artemis Web UI Report"
            return result

    def consolidate_test_results(self):
        for i in range(len(self.reports)):
            report = open(f"{Config.REPORT_FOLDER}/{self.reports[i]}", "r").read()
            if i == 0:
                self.test_results = self.extract_test_results(report, False)
            else:
                test_key, test_value = self.extract_test_results(report)
                self.test_results["tests"][test_key] = test_value

        skip_result_key_template = "{file_path}::{class_name}::{test_method}"
        skip_result_value_template = '[{{"extras":[],"result":"Skipped","testId":"{file_path}::{class_name}::{test_method}","duration":"00:00:00","resultsTableRow":["<td class=\\"col-result\\">Skipped</td>","<td class=\\"col-testId\\">{file_path}::{class_name}::{test_method}</td>","<td class=\\"col-duration\\">00:00:00</td>","<td class=\\"col-links\\"></td>"],"log":"No log output captured."}}]'
        for i in self.test_case_skipped_list:
            self.test_results["tests"][
                skip_result_key_template.format(
                    file_path=i.path, class_name=i.class_name, test_method=i.name
                )
            ] = ast.literal_eval(
                skip_result_value_template.format(
                    file_path=i.path, class_name=i.class_name, test_method=i.name
                )
            )

        # Add env data
        for key, value in self.env_dict.items():
            self.test_results["environment"][key] = value

    def create_report_template(self):
        shutil.copy(
            f"{Config.REPORT_FOLDER}/{self.reports[0]}", f"{Config.REPORT_FOLDER}/ui_report.html"
        )

    def injection_type_1(self, report, start_marker, end_marker, data):
        start_index = report.index(start_marker) + len(start_marker)
        end_index = report.index(end_marker)
        return report[:start_index] + data + report[end_index:]

    def populate_test_results(self):
        report = open(f"{Config.REPORT_FOLDER}/ui_report.html", "r").read()
        start_marker = "data-jsonblob="
        end_marker = "></div>"
        start_index = report.index(start_marker) + len(start_marker)
        end_index = start_index + (report[start_index:]).index(end_marker)
        report = (
            report[:start_index]
            + '"'
            + (html.escape(json.dumps(self.test_results))).replace("&quot", "&#34")
            + '"'
            + report[end_index:]
        )

        # Test Result Status
        data = f"""
            <div class="filters">
                <input checked="true" class="filter" name="filter_checkbox" type="checkbox" data-test-result="failed" {"disabled" if not self.failed else ""}/>
                <span class="failed">{self.failed} Failed,</span>
                <input checked="true" class="filter" name="filter_checkbox" type="checkbox" data-test-result="passed" {"disabled" if not self.passed else ""}/>
                <span class="passed">{self.passed} Passed,</span>
                <input checked="true" class="filter" name="filter_checkbox" type="checkbox" data-test-result="skipped" {"disabled" if not len(self.test_case_skipped_list) else ""}/>
                <span class="skipped">{len(self.test_case_skipped_list)} Skipped,</span>
            </div>
        """
        report = self.injection_type_1(
            report, '<div class="controls">', '<div class="collapse">', data
        )

        start_marker = "const possibleFilters ="
        end_marker = "]"
        start_index = report.index(start_marker) + len(start_marker)
        end_index = start_index + (report[start_index:]).index(end_marker) + len(end_marker)
        report = report[:start_index] + "['passed','skipped','failed']" + report[end_index:]
        (open(f"{Config.REPORT_FOLDER}/ui_report.html", "w")).write(report)

    def add_pie_chart(self):
        report = open(f"{Config.REPORT_FOLDER}/ui_report.html", "r").read()
        start_marker = "</script>"
        start_index = report.index(start_marker)
        self.total_tests = self.passed + self.failed + len(self.test_case_skipped_list)
        script = (
            """
            // Data for the pie chart
            const data = {labels: ["""
            + f"""{"'Passed'," if self.passed else ""}"""
            + f"""{"'Failed'," if self.failed else ""}"""
            + f"""{"'Skipped'" if len(self.test_case_skipped_list) else ""}"""
            + """],datasets: [{label: 'Test Result Breakdown',data: ["""
            + f"""{str(round((self.passed / self.total_tests) * 100, 2)) if self.passed else ""}"""
            + f"{',' if self.passed else ''}"
            + f"""{str(round((self.failed / self.total_tests) * 100, 2)) if self.failed else ""}"""
            + f"{',' if self.failed else ''}"
            + f"""{str(round((len(self.test_case_skipped_list) / self.total_tests) * 100, 2)) if len(self.test_case_skipped_list) else ""}"""
            + """],backgroundColor: ["""
            + f"""{"'#28a745'," if self.passed else ""}"""
            + f"""{"'#dc3545'," if self.failed else ""}"""
            + f"""{"'#ffc107'" if len(self.test_case_skipped_list) else ""}"""
            + """],borderColor: ['#ffffff', '#ffffff', '#ffffff'],borderWidth: 1}]};
            // Configuration for the pie chart
            const config = {type: 'pie',data: data,options: {responsive: true,plugins: {legend: {position: 'top',},tooltip: {callbacks: {label: function(tooltipItem) {return tooltipItem.label + ': ' + tooltipItem.raw + '%';}}}}}};
            // Create the pie chart
            const ctx = document.getElementById('myPieChart').getContext('2d');const myPieChart = new Chart(ctx, config);
            function showCounting() {const randomNumberElement = document.getElementById('total_tests');const finalNumber ="""
            f"""{self.total_tests}"""
            + """;const duration = 800;const intervalTime = 10;const totalSteps = duration / intervalTime;let currentNumber = 0;const increment = finalNumber / totalSteps;const interval = setInterval(() => {currentNumber += increment;if (currentNumber >= finalNumber) {clearInterval(interval);randomNumberElement.textContent = finalNumber;} else {randomNumberElement.textContent = Math.floor(currentNumber);}}, intervalTime);}showCounting();
        """
        )
        report = report[:start_index] + script + report[start_index:]
        start_marker = '<style type="text/css">'
        start_index = report.index(start_marker)
        script = '<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>'
        report = report[:start_index] + script + report[start_index:]
        # CSS
        start_marker = "</style>"
        start_index = report.index(start_marker)
        css = ".chart-container {display: flex;justify-content: flex-start;align-items: center;}canvas {max-width: 300px;max-height: 300px;}"
        report = report[:start_index] + css + report[start_index:]
        (open(f"{Config.REPORT_FOLDER}/ui_report.html", "w")).write(report)

    def get_soar_app_env_data(self) -> str:
        headers = {"Authorization": f"Bearer {Helper.get_bearer_token()}"}
        response = requests.get(
            f"{self.soar_url}/case-management/api/casemanagement/version", headers=headers
        )
        return json.loads(response.content)

    def get_env_data(self) -> None:
        # SOAR URL
        self.env_dict["SOAR_URL"] = self.soar_url
        # SOAR version
        _soar_env_data = self.get_soar_app_env_data()
        self.env_dict["SOAR_VERSION"] = _soar_env_data["version"]
        # SOAR commitSha
        self.env_dict["SOAR_COMMIT_SHA"] = _soar_env_data["commitSha"]
        # Playwright version
        result = subprocess.run(
            ["playwright", "--version"], capture_output=True, text=True, check=True
        )
        self.env_dict["Playwright"] = result.stdout.split()[-1]
        # Chromium version
        result = subprocess.run(
            [Helper.get_chromium_path(), "--version"], capture_output=True, text=True, check=True
        )
        self.env_dict["Chromium"] = result.stdout.split()[-1]

    def calculate_total_duration(self):
        total_time = timedelta()
        for time_str in self.test_duration:
            hours, minutes, seconds = map(int, time_str.split(":"))
            total_time += timedelta(hours=hours, minutes=minutes, seconds=seconds)
        total_seconds = int(total_time.total_seconds())
        hours = total_seconds // 3600
        minutes = (total_seconds % 3600) // 60
        seconds = total_seconds % 60
        return f"{hours:02}:{minutes:02}:{seconds:02}"

    def populate_data(self):
        report = open(f"{Config.REPORT_FOLDER}/ui_report.html", "r").read()
        # Title
        report = self.injection_type_1(report, '<title id="head-title">', "</title>", "Report")

        # Heading
        report = self.injection_type_1(report, '<h1 id="title">', "</h1>", "Artemis Web UI Report")

        # Report generated on
        data = f"Report generated on {(datetime.now()).strftime('%d-%b-%Y')} at {(datetime.now()).strftime('%H:%M:%S')}"
        report = self.injection_type_1(report, "</h1>", '<div id="environment-header">', data)

        # Total test count and total duration
        data = f"<b>{self.total_tests} {'tests' if self.total_tests > 1 else 'test'} took {self.calculate_total_duration()}</b>"
        report = self.injection_type_1(report, '<p class="run-count">', '<p class="filter">', data)

        # Result section
        start_marker = '<div id="environment-header">'
        end_marker = '<template id="template_environment_row">'
        start_index = report.index(start_marker)
        end_index = report.index(end_marker)
        result_section_html = f'<h2>Result</h2><div class="container"><div class="box1"><div><span id="total_tests" style="font-size: 80px; color: black;">{self.total_tests}</span><br /><span style="font-size: 40px; color: black;">{"Tests" if self.total_tests > 1 else "Test"}</span></div><div class="chart-container"><canvas id="myPieChart"></canvas></div></div><div style="width: 0.5%;"></div><div class="box2"><div><div id="environment-header"><h2 style="margin-top: 0px;">Environment</h2></div><table id="environment"></table></div></div></div>'
        result_section_css = ".container {display: flex;align-items: flex-start;}.box1 {width: 50%;height: 310px;}.box1, .box2 {padding: 10px;text-align: left;display: flex;justify-content: space-evenly;align-items: center;border: 2px solid black;}"
        report = report[:start_index] + result_section_html + report[end_index:]
        start_marker = "</style>"
        start_index = report.index(start_marker)
        report = report[:start_index] + result_section_css + report[start_index:]

        # Checkbox info
        report = report.replace(
            '<p class="filter">(Un)check the boxes to filter the results.</p>', ""
        )

        # Remove links column in result
        report = report.replace("<th>Links</th>", "")
        (open(f"{Config.REPORT_FOLDER}/ui_report.html", "w")).write(report)

    def clean_report_folder(self):
        reports = os.listdir(Config.REPORT_FOLDER)
        for report in reports:
            if report != "ui_report.html":
                os.remove(f"{Config.REPORT_FOLDER}/{report}")

    def clean_artifacts_folder(self):
        screenshots_path = self.artifacts_config["screenshots"]["path"]
        videos_path = self.artifacts_config["videos"]["path"]
        artifacts = (os.listdir(screenshots_path) if Path(screenshots_path).exists() else []) + (
            os.listdir(videos_path) if Path(videos_path).exists() else []
        )
        for artifact in artifacts:
            _, ext = artifact.split(".")
            if artifact not in self.test_artifacts:
                os.unlink(os.path.join(screenshots_path if ext == "png" else videos_path, artifact))

    def generate_report(self):
        self.get_all_reports()
        self.get_env_data()
        self.consolidate_test_results()
        self.create_report_template()
        self.populate_test_results()
        self.add_pie_chart()
        self.populate_data()
        self.clean_report_folder()
        self.clean_artifacts_folder()
        print(f"{Colors.GREEN}{Colors.BOLD}Report Generated Successfully{Colors.RESET}")


if __name__ == "__main__":
    runner = Runner()
    runner.run()
