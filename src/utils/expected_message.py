from enum import Enum


class ExpectedMessage(Enum):
    """Entity group related message"""

    ENTITIES_ADDED_MESSAGE = "Entities were added to the risk group successfully."
