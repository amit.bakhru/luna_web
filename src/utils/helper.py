import os
import re
import sys
import string
import random
import requests


class Helper:
    def random_string_generator():
        return "".join(random.choices(string.ascii_uppercase + string.digits, k=10))

    def get_css_locator(locator):
        return (re.search("(?<=css=)(.*?)(?='>)", str(locator()))).group()

    def get_chromium_path():
        """Get Chromium executable path directly using playwright"""
        print("Finding Chromium executable path...")
        try:
            from playwright.sync_api import sync_playwright

            with sync_playwright() as playwright:
                browser = playwright.chromium.launch()
                executable_path = playwright.chromium.executable_path
                browser.close()
                if executable_path:
                    print(f"Found Chromium path using Playwright: {executable_path}")
                    return executable_path
        except Exception as e:
            print(f"Could not get Chromium path from Playwright: {e}")
            print("Please install playwright and try again")
            sys.exit(1)

    @staticmethod
    def get_bearer_token():
        url = "https://api-internal.stage.devo.com/tapu/token"
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": f"Basic {os.getenv('TAPU_AUTHORIZATION_TOKEN')}",
        }
        data = {
            "grant_type": "client_credentials",
            "user": "indrajeetkumar.sah@devo.com",
            "token_type": "bearer",
            "domain": "luna_automation",
            "owner": "indrajeetkumar.sah@devo.com",
            "expires_in": ""
        }

        response = requests.post(url, headers=headers, data=data)
        return response.json().get("access_token")

    @staticmethod
    def random_string_generator_with_preset(prefix="Test_Case"):
        """
        Generate a unique case name with a random number.

        :param prefix: Prefix for the case name (default: 'Test_Case')
        :return: String with the formatted case name
        """
        return f"{prefix}_{random.randint(10000, 9999999)}"
