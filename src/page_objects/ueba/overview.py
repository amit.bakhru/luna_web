from src.lib.base.UIBase import UIBase
from src.page_objects.ueba.locators import overview_config
from luna.driver.ui.wuipage import WUIPage
from playwright.sync_api import Page
from src.lib.base.UIBase import UIBase


class OverviewPage(WUIPage, UIBase):
    def __init__(self, page: Page):
        super().__init__(config=overview_config, page=page, launch=True, runvalidations=True)
        self.ui_base = UIBase(page=self.page)
        self.notable_entities_path = "//*[@data-testid='ueba-overview-route__notable-entities-widget']//*[@data-testid='ueba-entity-details-link']"

    def verify_entity_in_notables_list(self, entity_name):
        self.overview_button().click()
        self.notable_reload_button().click()
        self.page.wait_for_timeout(3000)
        assert entity_name in self.get_list_element_text_by_path(self.notable_entities_path), (
            "Incorrect entity name is shown"
        )

    def verify_entity_not_in_notables_list(self, entity_name):
        self.overview_button().click()
        self.notable_reload_button().click()
        assert entity_name not in self.get_list_element_text_by_path(self.notable_entities_path), (
            "Incorrect entity name is shown"
        )

    def verify_1(self):
        s = "from entity.behavior.risk.events"
        self.data_search_button.click()
        self.free_text_query_button.click()
        self.page.wait_for_timeout(5000)
        print(s)
        self.query_content.nth(1).fill(s)
        self.run_button.click()
        self.page.wait_for_timeout(100000)

    def go_to_entities_by_index(self, index):
        self.overview_button().click()
        self.entities_value_list().nth(index).locator("div > div > p > a").first.click()

    def get_critical_risk_entities_count(self):
        self.overview_button().click()
        critical_entities_count_text = self.critical_risk_entities_count().inner_text()
        return critical_entities_count_text[
            critical_entities_count_text.find("(") + 1 : critical_entities_count_text.find(")")
        ]

    def get_high_risk_entities_count(self):
        self.overview_button().click()
        return self.high_risk_entities_count().inner_text()

    def get_medium_risk_entities_count(self):
        self.overview_button().click()
        medium_entities_count_text = self.medium_risk_entities_count().inner_text()
        return medium_entities_count_text[
            medium_entities_count_text.find("(") + 1 : medium_entities_count_text.find(")")
        ]

    def got_to_notable_entities(self):
        self.overview_button().click()
        self.notable_entities_link().click()

    def got_to_top_users(self):
        self.overview_button().click()
        self.top_users_link().click()

    def got_to_top_devices(self):
        self.overview_button().click()
        self.top_devices_link().click()

    def got_to_top_domains(self):
        self.overview_button().click()
        self.top_domains_link().click()

    def got_to_top_unique_alerts(self):
        self.overview_button().click()
        self.top_unique_alerts().click()

    def got_to_top_tactics(self):
        self.overview_button().click()
        self.top_tactics().click()

    def go_to_top_techniques(self):
        self.overview_button().click()
        self.top_techniques().click()

    def get_top_users_first_entity_name(self):
        self.overview_button().click()
        return self.top_users_entities_name().nth(0).inner_text()

    def get_top_users_first_entity_risk_score(self):
        return self.top_users_entities_risk_score().nth(0).inner_text()

    def go_to_top_users_first_entity_name(self):
        self.top_users_entities_name().nth(0).click()
        self.page.wait_for_timeout(2000)
