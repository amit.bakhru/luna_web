from pathlib import Path
from playwright.sync_api import Page
from src.page_objects.ueba.locators import content_manager_config
from luna.driver.ui.wuipage import WUIPage


class ContentManagerPage(WUIPage):
    def __init__(self, page: Page):
        super().__init__(config=content_manager_config, page=page, launch=True, runvalidations=True)
        self.page = page
        self.delete_risk_group_button = page.locator("button").filter(has_text="Delete Risk Group")
        self.remove_to_risk_group_button = page.locator("button").filter(
            has_text="Remove Risk Group Member"
        )
        self.remove_to_notable_button = page.locator("button").filter(
            has_text="Remove Entity List Member"
        )
        self.delete_alert_button = page.locator("button").filter(has_text="Delete Alert")
        self.rate_of_change_button = page.get_by_text("Rate of change", exact=True)
        self.delete_risk_score = page.locator("button").filter(has_text="Remove Risk Score")
        self.paused_status_button = page.get_by_test_id(
            "ueba-admin-behaviors-route__status-select"
        ).get_by_text("Paused", exact=True)
        self.stopped_status_button = page.get_by_test_id(
            "ueba-admin-behaviors-route__status-select"
        ).get_by_text("Stopped", exact=True)
        self.stop_alert_confirmation_btn = page.locator("button").filter(has_text="Stop Model")
        self.pause_alert_confirmation_btn = page.locator("button").filter(has_text="Pause Model")
        self.resume_alert_confirmation_btn = page.locator("button").filter(has_text="Resume Model")
        self.signals_button = page.get_by_test_id("ueba-use-case-editor__tabs").get_by_text(
            "Signals"
        )
        self.alerts_button = page.get_by_test_id("ueba-use-case-editor__tabs").get_by_text("Alerts")
        self.drag_and_drop = page.get_by_label("Drag & Drop your file here or")
        self.whitelist_user_btn = page.locator("button").filter(has_text="Users")
        self.whitelist_device_btn = page.locator("button").filter(has_text="Devices")
        self.whitelist_domain_btn = page.locator("button").filter(has_text="Domains")
        self.notification_message_locator = "[class*='toast__title']"
        self.notification_message = page.locator(self.notification_message_locator)
        self.close_notification_icon = page.locator("button[title='Close notification']")
        self.action_button = page.locator("button[title='Alert Actions']")
        self.behavior_actions_button = page.locator("button[title='Model Actions']")
        self.no_data_found_message = page.get_by_text("No data found")
        self.delete_button = page.get_by_text("Delete")
        self.edit_button = page.get_by_text("Edit")
        # self.disable_button = page.get_by_text("Disable")
        self.stop_button = page.get_by_role("menuitem", name=" Stop")
        self.pause_button = page.get_by_role("menuitem", name=" Pause")
        self.resume_button = page.get_by_role("menuitem", name=" Resume")
        self.data_search_button = page.get_by_label("Data search")
        self.free_text_query_button = page.get_by_text("Free text query")
        self.query_content = page.locator("textarea[aria-label*='Editor content']")
        self.run_button = page.get_by_role("button", name="Run")

    def open_entity_group_page(self):
        self.content_manager_button().click()
        self.entity_groups_button().click()
        self.page.wait_for_url(
            "https://us.devo.com/#/vapps/app.custom.Behavior_Analytics_V2/settings/risk-groups"
        )

    def add_new_entity_group(self, risk_group_name, risk_score_multiplier):
        self.new_group_button().click()
        self.risk_group_name().fill(risk_group_name)
        self.risk_score_multiplier().clear()
        self.risk_score_multiplier().fill(risk_score_multiplier)
        self.save_settings_button().click()
        self.page.wait_for_timeout(2000)

    def verify_group_entity_details_on_listing_page(self, risk_group_name, risk_score_multiplier):
        self.search_entity_group_on_listing_page(risk_group_name)
        self.page.wait_for_timeout(2000)
        assert self.entity_group_name().inner_text() == risk_group_name, (
            "Incorrect risk_group_name is shown"
        )
        assert self.entity_risk_score().inner_text() == risk_score_multiplier, (
            "Incorrect risk_score_multiplier is shown"
        )

    def edit_entity_group(self, risk_group_name, risk_score_multiplier):
        self.search_entity_group_on_listing_page(risk_group_name)
        self.page.wait_for_timeout(4000)
        self.action_button.nth(0).click()
        self.edit_score_multiplier().click()
        self.risk_score_multiplier().clear()
        self.risk_score_multiplier().fill(risk_score_multiplier)
        self.save_settings_button().click()
        self.reload_button().click()
        self.page.wait_for_timeout(2000)

    def delete_entity_group(self, risk_group_name):
        self.search_entity_group_on_listing_page(risk_group_name)
        self.action_button.nth(0).click()
        self.delete_risk_group().click()
        self.delete_risk_group_button.click()
        self.page.wait_for_timeout(2000)

    def search_entity_group_on_listing_page(self, risk_group_name):
        self.search_entity_group().clear()
        self.search_entity_group().fill(risk_group_name)
        self.reload_button().click()
        self.page.wait_for_timeout(2000)

    def verify_entity_in_group_list(self, risk_group_name, entity_name):
        self.content_manager_button().click()
        self.search_entity_group_on_listing_page(risk_group_name)
        self.action_button.nth(0).click()
        self.edit_list_button().click()
        assert self.entity_name().nth(0).inner_text() == entity_name, (
            "Incorrect entity name is shown"
        )

    def get_first_entity_group_name(self):
        return self.entity_group_name().nth(0).inner_text()

    def edit_risk_score_multiplier_from_detail_page(self, risk_group_name, risk_score_multiplier):
        self.search_entity_group_on_listing_page(risk_group_name)
        self.action_button.nth(0).click()
        self.edit_list_button().click()
        self.page.wait_for_timeout(2500)
        self.edit_risk_score_button().click()
        self.risk_score_multiplier().clear()
        self.risk_score_multiplier().fill(risk_score_multiplier)
        self.save_settings_button().click()

    def get_risk_score_multiplier_on_detail_page(self):
        return self.risk_score_multiplier_detail_page().inner_text()

    def add_entity_in_risk_group_on_detail_page(self, risk_group_name, entity_name):
        self.content_manager_button().click()
        self.search_entity_group_on_listing_page(risk_group_name)
        self.action_button.nth(0).click()
        self.edit_list_button().click()
        self.page.wait_for_timeout(3000)
        self.add_entities_button().click()
        self.input_entities_name().fill(entity_name)
        self.add_to_risk_group_button().click()
        self.page.wait_for_timeout(2500)
        self.reload_button().click()

    def get_entity_in_group_detail_page(self):
        return self.entity_name().nth(0).inner_text()

    def delete_entity_in_risk_group_on_detail_page(self):
        self.content_manager_button().click()
        self.delete_entities_button().click()
        self.remove_to_risk_group_button.click()

    def verify_entity_in_notables_list(self, entity_name):
        self.content_manager_button().click()
        self.notable_entities_button().click()
        self.search_notable_entity_on_listing_page(entity_name)
        self.reload_button().click()
        self.page.wait_for_timeout(3000)
        assert self.entity_group_name().nth(0).inner_text() == entity_name, (
            "Incorrect entity name is shown"
        )

    def search_notable_entity_on_listing_page(self, entity_name):
        self.search_notable_entity().clear()
        self.search_notable_entity().fill(entity_name)
        self.page.wait_for_timeout(1500)
        self.reload_button().click()
        self.page.wait_for_timeout(1500)
        assert self.entity_group_name().inner_text() == entity_name, (
            "Incorrect entity_name is shown"
        )

    def add_entity_in_notable_detail_page(self, entity_name):
        self.content_manager_button().click()
        self.notable_entities_button().click()
        self.page.wait_for_timeout(10000)
        self.add_entities_notable_button().click()
        self.input_entities_name().fill(entity_name)
        self.add_to_notable_button().click()
        self.page.wait_for_timeout(5000)

    def delete_entity_in_notable_detail_page(self, entity_name):
        self.content_manager_button().click()
        self.search_notable_entity_on_listing_page(entity_name)
        self.reload_button().click()
        self.page.wait_for_timeout(5000)
        self.delete_notable_entities_button().click()
        self.remove_to_notable_button.click()

    def add_risk_threshold_based_alert(
        self,
        alert_name,
        risk_threshold,
        minimum_unique_alerts,
        minimum_unique_tactics,
        maximum_unique_techniques,
    ):
        self.content_manager_button().click()
        self.risk_based_alert_button().click()
        self.create_button().click()
        self.alert_name().nth(0).fill(alert_name)
        self.advance_option().click()
        self.risk_based_number_inputs().nth(0).fill(minimum_unique_alerts)
        self.risk_based_number_inputs().nth(1).fill(minimum_unique_tactics)
        self.risk_based_number_inputs().nth(2).fill(maximum_unique_techniques)
        self.save_risk_alert_button().click()
        self.page.wait_for_timeout(1500)

    def search_risk_based_alerts_on_listing_page(self, risk_alert_name):
        self.search_risk_alerts().clear()
        self.search_risk_alerts().fill(risk_alert_name)
        self.reload_button().click()
        self.page.wait_for_timeout(2000)

    def verify_risk_based_alerts_details_on_listing_page(self, alert_name, risk_threshold):
        self.search_risk_based_alerts_on_listing_page(alert_name)
        self.reload_button().click()
        # print("actual",self.entity_group_name.inner_text())
        # print("expect",f"SecOpsRisk{alert_name}")
        assert self.entity_group_name().inner_text() == f"SecOpsRisk{alert_name}", (
            "Incorrect alert_name is shown"
        )
        # print("actualR",self.risk_threshold_listing_page.inner_text())
        # print("expectR",risk_threshold)
        assert self.risk_threshold_listing_page().inner_text() == risk_threshold, (
            "Incorrect risk_threshold is shown"
        )

    def delete_risk_based_alerts(self, alert_name):
        self.search_risk_based_alerts_on_listing_page(alert_name)
        self.action_button.nth(0).click()
        self.delete_button.click()
        self.delete_alert_button.click()

    def add_risk_rate_of_change_based_alert(
        self,
        alert_name,
        risk_score_change,
        evaluation_period,
        minimum_unique_alerts,
        minimum_unique_tactics,
        maximum_unique_techniques,
    ):
        self.content_manager_button().click()
        self.risk_based_alert_button().click()
        self.create_button().click()
        self.alert_name().nth(0).fill(alert_name)
        self.rate_of_change_button.click()
        self.risk_based_number_inputs().nth(0).clear()
        self.risk_based_number_inputs().nth(0).fill(risk_score_change)
        self.risk_based_number_inputs().nth(1).clear()
        self.risk_based_number_inputs().nth(1).fill(evaluation_period)
        self.advance_option().click()
        self.risk_based_number_inputs().nth(2).fill(minimum_unique_alerts)
        self.risk_based_number_inputs().nth(3).fill(minimum_unique_tactics)
        self.risk_based_number_inputs().nth(4).fill(maximum_unique_techniques)
        self.save_risk_alert_button().click()

    def verify_rate_of_change_risk_based_alerts_details_on_listing_page(
        self, alert_name, risk_score_change
    ):
        self.search_risk_based_alerts_on_listing_page(alert_name)
        assert self.entity_group_name().inner_text() == f"SecOpsRisk{alert_name}", (
            "Incorrect alert_name is shown"
        )
        assert self.risk_rate_of_change_listing_page().inner_text() == risk_score_change, (
            "Incorrect rate_of_change is shown"
        )

    def edit_risk_based_alerts(self, alert_name, risk_threshold):
        self.search_risk_based_alerts_on_listing_page(alert_name)
        self.action_button.nth(0).click()
        self.edit_button.click()
        self.risk_threshold().clear()
        self.risk_threshold().fill(risk_threshold)
        self.save_risk_alert_button().click()

    def edit_risk_score_change(self, alert_name, risk_score_change):
        self.search_risk_based_alerts_on_listing_page(alert_name)
        self.action_button.nth(0).click()
        self.edit_button.click()
        self.risk_based_number_inputs().nth(0).clear()
        self.risk_based_number_inputs().nth(0).fill(risk_score_change)
        self.save_risk_alert_button().click()

    def get_first_secops_alerts_name(self):
        self.content_manager_button().click()
        self.secops_alerts_risk_score().click()
        self.search_secops_alerts_risk_score().clear()
        self.page.wait_for_timeout(5000)
        return self.secops_alert_name().nth(0).inner_text()

    def search_secops_alerts_on_listing_page(self, risk_alert_name):
        self.search_secops_alerts_risk_score().clear()
        self.search_secops_alerts_risk_score().fill(risk_alert_name)
        self.reload_button().click()
        self.page.wait_for_timeout(2000)

    def edit_secops_alerts_risk_score(self, risk_score, secops_alert_name):
        self.search_secops_alerts_on_listing_page(secops_alert_name)
        self.page.wait_for_timeout(2000)
        self.action_button.nth(0).click()
        self.edit_button.click()
        self.risk_score().clear()
        self.risk_score().fill(risk_score)
        self.page.wait_for_timeout(3000)
        self.save_alert_risk_score_button().click()

    def verify_secops_alert_risk_score_on_listing_page(self, secops_alert_name, risk_score):
        self.search_secops_alerts_on_listing_page(secops_alert_name)
        assert self.entity_risk_score().nth(0).inner_text() == risk_score, (
            "Incorrect risk_score is shown"
        )

    def remove_secops_alerts_risk_score(self, secops_alert_name):
        self.search_secops_alerts_on_listing_page(secops_alert_name)
        self.action_button.nth(0).click()
        self.remove_risk_score().click()
        self.delete_risk_score.click()

    def go_to_disabled_use_case_listing_page(self):
        self.content_manager_button().click()
        self.behavior_alert_definitions_button().click()
        self.status_usecase_button().click()
        self.stopped_status_button.click()
        self.page.wait_for_timeout(2000)

    def go_to_pause_use_case_listing_page(self):
        self.content_manager_button().click()
        self.behavior_alert_definitions_button().click()
        self.status_usecase_button().click()
        self.paused_status_button.click()
        self.page.wait_for_timeout(2000)

    def get_first_usecase_name(self):
        self.search_use_case().clear()
        self.page.wait_for_timeout(1000)
        return self.usecase_name().nth(0).inner_text()

    def search_use_case_on_listing_page(self, use_case_name):
        self.search_use_case().clear()
        self.search_use_case().fill(use_case_name)
        self.reload_button().click()
        self.page.wait_for_timeout(2000)

    def configure_use_case_V2(self, use_case_name):
        self.search_use_case_on_listing_page(use_case_name)
        self.configure_enable_button().click()
        # self.signal_risk_score.fill(signal_risk_score)
        self.whitelist_tab().click()
        self.whitelist_user_btn.click()
        # self.whitelist_inputs.fill(whitelist_user)
        self.whitelist_device_btn.click()
        # self.whitelist_inputs.fill(whitelist_device)
        self.whitelist_domain_btn.click()
        # self.whitelist_inputs.fill(whitelist_domain)
        self.alerts_tab().click()
        self.enable_button().click()
        self.page.wait_for_timeout(2000)

    def clear_use_case_status_on_listing_page(self):
        self.clear_use_case_status().click()
        self.page.wait_for_timeout(2000)

    def verify_use_case_status_on_listing_page(self, use_case_name, status):
        self.search_use_case_on_listing_page(use_case_name)
        assert self.risk_threshold_listing_page().inner_text() == status, (
            "Incorrect status is shown"
        )

    def wait_for_usecase_to_run(self):
        while True:
            assert not self.usecase_error_state().is_visible(), (
                f"Failed: Error thrown in usecase pipeline"
            )
            try:
                self.usecase_running_state().wait_for(state="visible", timeout=10000)
                break
            except:
                self.reload_button().click()

    def disable_use_case(self, use_case_name):
        self.search_use_case_on_listing_page(use_case_name)
        self.behavior_actions_button.nth(0).click()
        self.stop_button.click()
        self.stop_alert_confirmation_btn.click()
        self.page.wait_for_timeout(2000)

    def pause_use_case(self, use_case_name):
        self.search_use_case_on_listing_page(use_case_name)
        self.behavior_actions_button.nth(0).click()
        self.pause_button.click()
        self.pause_alert_confirmation_btn.click()
        self.page.wait_for_timeout(2000)

    def resume_use_case(self, use_case_name):
        self.search_use_case_on_listing_page(use_case_name)
        self.behavior_actions_button.nth(0).click()
        self.resume_button.click()
        self.resume_alert_confirmation_btn.click()
        self.page.wait_for_timeout(2000)
        self.reload_button().click()

    def upload_and_download_notable_entities(self, download_filepath):
        self.content_manager_button().click()
        self.notable_entities_button().click()
        script_dir = Path(__file__).resolve().parent
        file_path = script_dir.parents[2] / download_filepath
        file_path.unlink(missing_ok=True)
        with self.page.expect_download() as download_info:
            self.download_notable_entity_button().click()
        download = download_info.value
        download.save_as(file_path)
        if (
            file_path.is_file()
            and file_path.suffix.lower() == ".csv"
            and file_path.stat().st_size > 0
        ):
            download_complete = True
        else:
            download_complete = False
        self.upload_notable_entity_button().click()
        self.drag_and_drop.set_input_files(file_path)
        self.save_notable_entity_button().click()
        toast_msg = self.toast_msg().inner_text()
        print(toast_msg)

    def verify_notification_message(self, expected_message):
        self.notification_message.wait_for(state="visible", timeout=10000)
        msg = self.notification_message.inner_text()
        # self.close_notification_icon.click()
        assert msg == expected_message, "Incorrect notification message is shown"

    def wait_for_notification_message_to_be_shown(self):
        # self.notification_message_locator.wait_for(state="visible", timeout=10000)
        self.page.wait_for_selector(self.notification_message_locator, state="visible")

    def get_notification_message(self):
        self.notification_message.wait_for(state="visible", timeout=10000)
        return self.notification_message.inner_text()

    def verify_no_data_found_message(self):
        self.no_data_found_message.wait_for(state="visible", timeout=10000)
        assert self.no_data_found_message.inner_text() == "No data found", (
            "Incorrect message is shown"
        )
