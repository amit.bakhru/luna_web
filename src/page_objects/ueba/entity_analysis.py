from playwright.sync_api import Page
from luna.driver.ui.wuipage import WUIPage
from src.page_objects.ueba.locators import entity_analysis_config


class EntityAnalysisPage(WUIPage):
    def __init__(self, page: Page):
        super().__init__(config=entity_analysis_config, page=page, launch=True, runvalidations=True)
        self.page = page
        self.entity_analysis_button = page.locator(
            "//span[text()='Entity Analysis']//parent::button"
        )
        self.risk_group_button = page.locator(
            "span.ueba-risk-group-select.ueba-drop-down-menu > button"
        )
        self.risk_group_entity = page.locator("//tbody//td[9]")
        self.entity_name = page.locator("//tbody//td[3]")
        self.entity_group_name = page.locator("//span[contains(@class,'ueba-risk-group-select')]")
        self.entity_name_link = page.locator("//tbody//td[3]//a")
        self.search_entity_name = page.get_by_placeholder("Search by entity name")
        self.notable_entities_button = page.get_by_test_id("ueba-notable-entity-toggle")
        self.filter_button = page.get_by_test_id("ueba-entities-route__filter-expander")
        self.last_risk_days = page.locator("div[class*='react-select__value-wrapper ']")
        self.users_filter = page.locator("label[class*='is-users'] input")
        self.devices_filter = page.locator("label[class*='is-devices'] input")
        self.domains_filter = page.locator("label[class*='is-domains'] input")
        self.unique_alerts = page.locator("[data-column-id='unique_alerts'] button")
        self.unique_tactics = page.locator("[data-column-id='unique_tactics'] button")
        self.unique_techniques = page.locator("[data-column-id='unique_techniques'] button")
        self.entity_name_on_detail_page = page.locator("div.ueba-entity-details-route-header__name")
        self.risk_score_on_detail_page = page.locator(
            "div[data-testid*='ueba-risk-score'] [data-testid*='ueba-risk-score__celldata']"
        )

    def add_and_verify_entity_to_group_from_listing_page(self, entity_group_name):
        self.entity_analysis_button.click()
        self.page.get_by_test_id("spinner_dark").wait_for(state="hidden", timeout=10000)
        # self.risk_group_button.nth(0).click()
        self.risk_group_entity.nth(0).click()
        self.page.wait_for_timeout(1000)
        self.page.get_by_title(entity_group_name).click()
        self.page.wait_for_timeout(2500)
        assert self.risk_group_entity.nth(0).inner_text() == entity_group_name, (
            "Incorrect risk group is shown"
        )
        # assert self.risk_group_entity.first.inner_text() == entity_group_name, "Incorrect risk group is shown"

    def get_first_entity_name(self):
        self.entity_analysis_button.click()
        self.page.wait_for_timeout(2000)
        self.search_entity_name.clear()
        self.page.wait_for_timeout(2000)
        return self.entity_name.nth(0).inner_text()

    def add_and_verify_entity_to_group_from_detail_page(self, entity_group_name):
        self.entity_name_link.nth(0).click()
        self.page.wait_for_timeout(1000)
        self.risk_group_button.nth(0).click()
        self.page.get_by_title(entity_group_name).click()
        self.page.wait_for_timeout(2500)
        assert self.entity_group_name.inner_text() == entity_group_name, (
            "Incorrect risk group is shown"
        )

    def search_entity_on_listing_page(self, entity_name):
        self.search_entity_name.clear()
        self.search_entity_name.fill(entity_name)
        self.page.keyboard.press("Enter")
        self.page.wait_for_timeout(6000)

    def verify_entity_to_group_from_listing_page(self, entity_group_name, entity_name):
        self.entity_analysis_button.click()
        self.search_entity_on_listing_page(entity_name)
        self.reload_button().click()
        self.page.wait_for_timeout(2500)
        assert self.risk_group_entity.nth(0).inner_text() == entity_group_name, (
            "Incorrect risk group is shown"
        )

    def add_entity_to_notable_from_listing_page(self, entity_name):
        self.search_entity_on_listing_page(entity_name)
        self.notable_entities_button.nth(0).click()
        self.page.wait_for_timeout(5000)

    def get_entity_name_by_index(self, index):
        self.entity_analysis_button.click()
        self.page.wait_for_url(
            "https://us.devo.com/#/vapps/app.custom.Behavior_Analytics_V2/entities"
        )
        self.search_entity_name.clear()
        self.entity_name.nth(0).wait_for(state="visible", timeout=10000)
        return self.entity_name.nth(index).inner_text()

    def add_entity_to_notable_from_detail_page(self, entity_name):
        self.search_entity_on_listing_page(entity_name)
        # self.page.pause()
        self.page.wait_for_timeout(3000)
        self.entity_name_link.nth(0).click()
        self.page.locator("button[data-testid='ueba-notable-entity-toggle']")
        self.notable_entities_button.nth(0).click()
        self.page.wait_for_timeout(5000)

    def verify_entity_to_notable_from_listing_page(self, entity_name, title_message):
        self.entity_analysis_button.click()
        self.search_entity_on_listing_page(entity_name)
        self.reload_button().click()
        self.page.wait_for_timeout(5000)
        assert title_message in self.notable_entities_button.nth(0).get_attribute("title"), (
            "Incorrect notable entity is shown"
        )

    def verify_total_entities_tracked_last_7_days_filters(self):
        self.filter_button.click()
        self.page.wait_for_timeout(2000)
        assert self.last_risk_days.nth(0).inner_text() == "Last 7 days", (
            "Incorrect last risk days data is shown"
        )

    def verify_critical_entities_filters(self):
        self.page.wait_for_timeout(2000)
        assert self.last_risk_days.nth(0).inner_text() == "Last 7 days", (
            "Incorrect last risk days data is shown"
        )
        assert self.last_risk_days.nth(1).inner_text() == "Critical (90-100)", (
            "Incorrect data is shown"
        )

    def verify_high_entities_filters(self):
        self.page.wait_for_timeout(2000)
        assert self.last_risk_days.nth(0).inner_text() == "Last 7 days", (
            "Incorrect last risk days data is shown"
        )
        assert self.last_risk_days.nth(1).inner_text() == "High (70-90)", "Incorrect data is shown"

    def verify_medium_entities_filters(self):
        self.page.wait_for_timeout(2000)
        assert self.last_risk_days.nth(0).inner_text() == "Last 7 days", (
            "Incorrect last risk days data is shown"
        )
        assert self.last_risk_days.nth(1).inner_text() == "Medium (50-70)", (
            "Incorrect data is shown"
        )

    def verify_total_entities_tracked_last_24_hours_filters(self):
        self.page.wait_for_timeout(2000)
        assert self.last_risk_days.nth(0).inner_text() == "Last 24 hours", (
            "Incorrect last risk days data is shown"
        )

    def verify_notable_entities_filters(self):
        self.page.wait_for_timeout(2000)
        assert self.last_risk_days.nth(0).inner_text() == "Last 7 days", (
            "Incorrect last risk days data is shown"
        )
        assert self.last_risk_days.nth(2).inner_text() == "Notables List", (
            "Incorrect notable entities data is shown"
        )

    def verify_top_users_filters(self):
        self.verify_total_entities_tracked_last_7_days_filters()
        assert self.users_filter.get_attribute("checked") == "", "Incorrect value is shown"

    def verify_top_devices_filters(self):
        self.verify_total_entities_tracked_last_7_days_filters()
        assert self.devices_filter.get_attribute("checked") == "", "Incorrect value is shown"

    def verify_top_domains_filters(self):
        self.verify_total_entities_tracked_last_7_days_filters()
        assert self.domains_filter.get_attribute("checked") == "", "Incorrect value is shown"

    def verify_top_unique_alert_filters(self):
        self.verify_total_entities_tracked_last_7_days_filters()
        assert self.unique_alerts.get_attribute("title") == "Sorting in descending order", (
            "Incorrect value is shown"
        )

    def verify_top_unique_tactics_filters(self):
        self.verify_total_entities_tracked_last_7_days_filters()
        assert self.unique_tactics.get_attribute("title") == "Sorting in descending order", (
            "Incorrect value is shown"
        )

    def verify_top_unique_techniques_filters(self):
        self.verify_total_entities_tracked_last_7_days_filters()
        assert self.unique_techniques.get_attribute("title") == "Sorting in descending order", (
            "Incorrect value is shown"
        )

    def verify_details_on_entities_detail_page(self, entity_name, risk_score):
        assert self.entity_name_on_detail_page.inner_text() == entity_name, (
            "Incorrect entity name is shown"
        )
        assert self.risk_score_on_detail_page.inner_text() == risk_score, (
            "Incorrect risk score is shown"
        )
