title = "Behavior Analytics (Dev) | Devo"
startUrl = "/#/vapps/app.custom.Behavior_Analytics_V2"

elements = dict(
    overview_button=("xpath", "//span[text()='Overview']//parent::button"),
    notable_entities_path=(
        "xpath",
        "//*[@data-testid='ueba-overview-route__notable-entities-widget']//*[@data-testid='ueba-entity-details-link']",
    ),
    entities_value_list=("xpath", "//div[contains(@class, 'ueba-donut__value')]"),
    notable_entities_link=(
        "xpath",
        "//div[contains(@data-testid, 'notable')]//button[contains(@data-testid, 'browse-link')]",
    ),
    top_users_link=(
        "xpath",
        "//div[contains(@data-testid, 'users')]//button[contains(@data-testid, 'browse-link')]",
    ),
    top_devices_link=(
        "xpath",
        "//div[contains(@data-testid, 'devices')]//button[contains(@data-testid, 'browse-link')]",
    ),
    top_domains_link=(
        "xpath",
        "//div[contains(@data-testid, 'domains')]//button[contains(@data-testid, 'browse-link')]",
    ),
    top_unique_alerts=(
        "xpath",
        "//div[contains(@data-testid, 'unique_alerts')]//button[contains(@data-testid, 'browse-link')]",
    ),
    top_tactics=(
        "xpath",
        "//div[contains(@data-testid, 'unique_tactics')]//button[contains(@data-testid, 'browse-link')]",
    ),
    top_techniques=(
        "xpath",
        "//div[contains(@data-testid, 'unique_techniques')]//button[contains(@data-testid, 'browse-link')]",
    ),
    critical_risk_entities_count=(
        "xpath",
        "//div[contains(@class, 'ueba-donut') and contains(@class, 'ueba-overview-route-summary-widget__critical')]//div[contains(@class, 'ueba-donut__label')]",
    ),
    high_risk_entities_count=(
        "xpath",
        "//div[contains(@class, 'highRiskEntities')]//div[contains(@class, 'ueba-donut__value')]",
    ),
    medium_risk_entities_count=(
        "xpath",
        "//div[contains(@class, 'ueba-donut') and contains(@class, 'ueba-overview-route-summary-widget__medium')]//div[contains(@class, 'ueba-donut__label')]",
    ),
    top_users_entities_name=(
        "css",
        "div[data-testid*='top-entities-widget__users'] [data-testid*='ueba-entity-details']",
    ),
    top_users_entities_risk_score=(
        "xpath",
        "//div[contains(@data-testid, 'top-entities-widget__users')]//div[contains(@data-testid, 'ueba-risk-score__celldata')]",
    ),
    reload_button=("xpath", "//*[@data-testid='ueba-reload-button']"),
    notable_reload_button=(
        "css",
        "div[data-testid=ueba-overview-route__notable-entities-widget] button[data-testid=ueba-reload-button]",
    ),
)

validations = [
    # dict(WaitForElementDisplayed=elements["overview_button"])
]
