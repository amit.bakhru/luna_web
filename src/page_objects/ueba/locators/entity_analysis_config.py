title = "Behavior Analytics (Dev) | Devo"
startUrl = "/#/vapps/app.custom.Behavior_Analytics_V2/entities"

elements = dict(
    entity_analysis_button=("xpath", "//span[text()='Entity Analysis']//parent::button"),
    risk_group_button=("xpath", "span.ueba-risk-group-select.ueba-drop-down-menu > button"),
    risk_group_entity=("xpath", "//tbody//td[9]"),
    entity_name=("xpath", "//tbody//td[3]"),
    entity_group_name=("xpath", "//span[contains(@class,'ueba-risk-group-select')]"),
    entity_name_link=("xpath", "//tbody//td[3]//a"),
    last_risk_days=("css", "div[class*='react-select__value-wrapper ']"),
    users_filter=("css", "label[class*='is-users'] input"),
    devices_filter=("css", "label[class*='is-devices'] input"),
    domains_filter=("css", "label[class*='is-domains'] input"),
    unique_alerts=("css", "[data-column-id='unique_alerts'] button"),
    unique_tactics=("css", "[data-column-id='unique_tactics'] button"),
    unique_techniques=("css", "[data-column-id='unique_techniques'] button"),
    entity_name_on_detail_page=("css", "div.ueba-entity-details-route-header__name"),
    risk_score_on_detail_page=(
        "css",
        "div[data-testid*='ueba-risk-score'] [data-testid*='ueba-risk-score__celldata']",
    ),
    notable_entities_button=("xpath", "//*[@data-testid='ueba-notable-entity-toggle']"),
    filter_button=("xpath", "//*[@data-testid='ueba-entities-route__filter-expander']"),
    search_entity_name=("placeholder", "Search by entity name"),
    reload_button=("xpath", "//*[@data-testid='ueba-reload-button']"),
)

validations = [
    # dict(WaitForElementDisplayed=elements["entity_analysis_button"]),
    # dict(WaitForElementDisplayed=elements["risk_group_button"]),
]
