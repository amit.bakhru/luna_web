from src.utils.helper import Helper
from playwright.sync_api import Page
from luna.driver.ui.wuipage import WUIPage
from src.base.base_config import BaseConfig
from src.page_objects.soar.case_management.locators import case_settings_config

from src.constants import NEW_CASE_TYPE_TASK_SUCCESSFULLY_CREATED_MESSAGE


class CaseSettings(WUIPage):
    def __init__(self, page: Page, launch: bool = True):
        super().__init__(config=case_settings_config, page=page, launch=launch, runvalidations=True)

    # Status Workflow
    def navigate_to_status_workflow_tab(self, wait=True):
        self.status_workflow_tab().first.click()
        if wait:
            self.wait_until_status_workflow_loading_is_complete()

    def wait_until_status_workflow_loading_is_complete(self):
        self.page.wait_for_selector(Helper.get_css_locator(self.status_workflow_list_container))

    def wait_until_status_workflow_details_page_heading_is_loaded(self):
        while (
            self.get_status_workflow_heading() == "Status workflow"
            or self.get_status_workflow_heading() == "General "
        ):
            continue

    def create_new_status_workflow(self):
        self.new_status_workflow_btn().click()

    def get_toast_msg_text(self):
        return self.toast_message().inner_text()

    def close_toast_message(self):
        self.toast_message_close_button().click()
        while self.toast_message().is_visible():
            continue

    def get_status_workflow_heading(self):
        return self.status_workflow_heading().last.inner_text()

    def get_status_workflow_name(self):
        return self.status_workflow_name().get_attribute("value")

    def get_status_workflow_description(self):
        return self.status_workflow_description().input_value()

    def click_status_workflow_save_btn(self):
        self.status_workflow_save_btn().click()

    def click_new_status_update_btn(self):
        self.status_workflow_update_btn().click()

    def navigate_to_status_workflow_details_page(self, status_workflow_name):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_row).format(
                status_workflow_name=status_workflow_name
            )
        ).click()

    def edit_status_workflow(self, name, description):
        self.fill_status_workflow_name(name)
        self.fill_status_workflow_description(description)

    def add_new_status(self, new_status_name, new_status_type):
        self.click_new_status_btn()
        self.fill_new_status_name(new_status_name)
        self.select_new_status_type(new_status_type)
        self.status_workflow_save_btn().last.click()

    def fill_status_workflow_name(self, name):
        self.status_workflow_name().fill(name)

    def fill_status_workflow_description(self, description):
        self.status_workflow_description().fill(description)

    def click_new_status_btn(self):
        self.status_workflow_new_status_btn().click()

    def fill_new_status_name(self, name):
        self.status_workflow_new_status_name().fill(name)

    def select_new_status_type(self, choice):
        self.status_workflow_new_status_type_dropdown().click()
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_new_status_type_choice).format(
                new_status_type=choice
            )
        ).click()

    def get_new_status_display_name(self, nth_child=3):
        return self.page.locator(
            Helper.get_css_locator(self.status_workflow_new_status_display_name).format(
                nth_child=nth_child
            )
        ).inner_text()

    def get_status_workflow_delete_confirmation_popup_content(self) -> str:
        return self.status_workflow_delete_confirmation_popup_content().inner_text()

    def delete_status_workflow(self, name):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=name, nth_child=5
            )
        ).click()
        self.status_workflow_menu_del_btn().click()
        self.status_workflow_yes_btn().click()

    def edit_new_status(self, old_name, new_name):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=old_name, nth_child=6
            )
        ).last.click()
        self.status_workflow_menu_edit_btn().click()
        self.fill_new_status_name(new_name)
        self.click_new_status_update_btn()

    def clone_new_status(self, old_name, new_name):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=old_name, nth_child=6
            )
        ).last.click()
        self.status_workflow_menu_clone_btn().click()
        self.fill_new_status_name(new_name)
        self.status_workflow_save_btn().last.click()

    def delete_new_status(self, name):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=name, nth_child=6
            )
        ).last.click()
        self.status_workflow_menu_del_btn().last.click()
        if self.get_status_workflow_delete_confirmation_popup_content().find(name):
            self.status_workflow_yes_delete_btn().click()

    def clone_status_workflow(self, name):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=name, nth_child=5
            )
        ).click()
        self.status_workflow_menu_clone_btn().click()

    def fill_status_workflow_search_box(self, name):
        self.status_workflow_search_box().fill(name)

    def wait_for_status_workflow_table_to_update(self):
        def _get_num_of_rows():
            return len(self.page.query_selector_all("tbody.ant-table-tbody tr[name]"))

        rows = _get_num_of_rows()
        while rows == _get_num_of_rows():
            continue

    def get_status_workflow_row_display_name(self):
        return self.status_workflow_row_display_name().first.inner_text()

    # Priority
    def navigate_to_priority_tab(self):
        self.priority_tab().first.click()
        self.wait_until_status_workflow_loading_is_complete()

    def wait_until_priority_loading_is_complete(self):
        self.page.wait_for_selector(Helper.get_css_locator(self.status_workflow_list_container))

    def create_new_priority(self, name, type="Very Low"):
        self.new_priority_level_btn().click()
        self.fill_new_priority_name(name)
        self.select_new_priority_icon(type)
        self.click_new_priority_add_btn()

    def fill_new_priority_name(self, name):
        self.new_priority_name_input_field().fill(name)

    def select_new_priority_icon(self, type):
        self.new_priority_icon_dropdown().click()
        self.page.locator(
            Helper.get_css_locator(self.new_priority_icon_type).format(priority_icon_type=type)
        ).click()

    def click_new_priority_add_btn(self):
        self.new_priority_add_btn().click()

    def navigate_to_case_listing_page(self):
        self.page.goto(f"{self.master_config['servers']['soar']['url']}/#/cases")

    def navigate_to_case_configuration_page(self):
        self.page.goto(f"{self.master_config['servers']['soar']['url']}/#/admin/cases")

    def delete_priority(self, name, type="Low"):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=name, nth_child=6
            )
        ).click()
        self.status_workflow_menu_del_btn().click()
        self.select_new_priority_icon(type)
        self.priority_delete_and_reassign_btn().click()

    # Fields
    def navigate_to_fields_tab(self):
        self.fields_tab().first.click()
        self.wait_until_status_workflow_loading_is_complete()

    def create_new_field(self, name, type="Text"):
        self.click_new_field_btn()
        self.fill_new_field_name(name)
        self.fill_new_field_display_name(name)
        self.select_field_type(type)
        self.click_new_field_save_btn()

    def select_field_type(self, type):
        self.click_field_type_dropdown()
        self.click_field_type(type)

    def click_field_type_dropdown(self):
        self.field_type_dropdown().click()

    def click_field_type(self, type):
        self.page.locator(
            Helper.get_css_locator(self.choose_field_type).format(field_type=type)
        ).click()

    def verify_edit_field_types(self, name):
        self.page.locator(
            Helper.get_css_locator(self.field_menu_icon).format(field_name=name)
        ).click()
        self.status_workflow_menu_edit_btn().click()
        _allowed_field_types = ["Text", "Textarea", "Markdown"]
        for type in _allowed_field_types:
            self.select_field_type(type)

    def click_new_field_btn(self):
        self.new_field_btn().click()

    def fill_new_field_name(self, name):
        self.new_field_name_field().fill(name)

    def fill_new_field_display_name(self, name):
        self.new_field_display_name_field().fill(name)

    def click_new_field_save_btn(self):
        self.status_workflow_save_btn().click()

    def delete_field(self, name):
        self.page.locator(
            Helper.get_css_locator(self.field_menu_icon).format(field_name=name)
        ).click()
        self.status_workflow_menu_del_btn().click()
        self.status_workflow_yes_delete_btn().click()

    def fill_field_search_box(self, name):
        self.status_workflow_search_box().first.fill(name)

    def wait_for_field_search_results_to_be_displayed(self):
        while len(self.page.query_selector_all("tbody.ant-table-tbody tr[fieldname]")) != 1:
            continue

    def get_field_name(self):
        return self.field_rows_fieldname_column().inner_text()

    def click_hide_system_fields_toggle(self):
        self.hide_system_fields_toggle().click()
        self.wait_until_status_workflow_loading_is_complete()

    def verify_hide_system_fields_toggle(self):
        created_by_list = self.page.query_selector_all(
            Helper.get_css_locator(self.field_rows_created_by_column)
        )
        for item in created_by_list:
            if item.inner_text() == "system":
                return False
        return True

    # General
    def change_case_id(self, id):
        self.case_id_display_box().click()
        self.case_id_input_field().fill(id)
        self.case_id_input_field_check_btn().click()

    # Case Types
    def navigate_to_case_types_tab(self):
        self.case_types_tab().first.click()
        self.wait_until_status_workflow_loading_is_complete()

    def create_case_type(self, name, desc):
        self.click_new_case_type_btn()
        self.fill_case_type_name(name)
        self.fill_case_type_description(desc)
        self.status_workflow_save_btn().click()

    def click_new_case_type_btn(self):
        self.new_case_type_btn().click()

    def fill_case_type_name(self, name):
        self.case_type_name_field().fill(name)

    def fill_case_type_description(self, desc):
        self.case_type_description_field().fill(desc)

    def edit_case_type(self, name, nth_child, case_details, field, status_workflow, task):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=name, nth_child=nth_child
            )
        ).click()
        self.status_workflow_menu_edit_btn().click()
        case_details.update_assigned_to_case_details(BaseConfig.get_username())
        self.navigate_to_case_type_fields_tab()
        assert True == self.verify_newly_created_field_in_case_types_field_tab(field)
        self.navigate_to_case_type_status_workflow_tab()
        self.select_newly_created_status_workflow_on_case_types(status_workflow)
        self.navigate_to_case_type_task_tab()
        self.add_manual_task(case_details, task)
        assert NEW_CASE_TYPE_TASK_SUCCESSFULLY_CREATED_MESSAGE == self.get_toast_msg_text()
        self.close_toast_message()
        self.navigate_to_case_type_details_tab()
        self.click_status_workflow_save_btn()

    def navigate_to_case_type_fields_tab(self):
        self.case_types_field_tab().click()

    def navigate_to_case_type_status_workflow_tab(self):
        self.case_types_status_workflow_tab().click()

    def navigate_to_case_type_task_tab(self):
        self.case_types_tesk_tab().click()

    def navigate_to_case_type_details_tab(self):
        self.case_types_details_tab().click()

    def verify_newly_created_field_in_case_types_field_tab(self, field):
        fields = self.case_types_field_container().inner_text()
        if fields.find(field) != -1:
            return True
        return False

    def select_newly_created_status_workflow_on_case_types(self, name):
        self.new_priority_icon_dropdown().first.click()
        self.page.locator(
            Helper.get_css_locator(self.select_case_type_status_workflow).format(
                status_workflow=name
            )
        ).click()
        self.page.wait_for_selector(Helper.get_css_locator(self.case_types_sw_existing_status_map))
        statuses = self.page.query_selector_all(
            Helper.get_css_locator(self.case_types_sw_existing_status_map)
        )
        for status in statuses:
            status.click()
            self.case_types_sw_existing_status_new_map().last.click()
        self.case_types_status_workflow_submit_btn().click()
        self.close_toast_message()

    def add_manual_task(self, case_details, task):
        self.case_types_add_task_btn().click()
        self.case_type_add_task_select_manual_task().click()
        self.case_types_manual_task_name().fill(task)
        case_details.update_priority_case_details("Low")
        self.status_workflow_save_btn().last.click()

    def delete_case_type(self, name):
        self.page.locator(
            Helper.get_css_locator(self.status_workflow_menu_icon).format(
                status_workflow_name=name, nth_child=7
            )
        ).click()
        self.status_workflow_menu_del_btn().click()
        self.ok_btn().click()
