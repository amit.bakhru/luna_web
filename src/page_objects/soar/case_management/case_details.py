from playwright.sync_api import expect
from luna.config import config
from src.utils.helper import Helper
from playwright.sync_api import Page
from luna.driver.ui.wuipage import WUIPage
from src.page_objects.soar.case_management.locators import case_details_config
import os
import requests


class CaseDetails(WUIPage):
    def __init__(self, page: Page, launch: bool = True):
        super().__init__(config=case_details_config, page=page, launch=launch, runvalidations=True)
        self.timeout = self.master_config["common"]["timeout"] * 1000

    def update_title_case_details(self, case_update_title):
        self.case_title_edit_text_icon().click()
        self.case_title_edit_text_input().clear()
        self.case_title_edit_text_input().fill(case_update_title)
        self.case_title_edit_text_confirm_button().first.click()
        self.page.locator(
            (Helper.get_css_locator(self.case_title_dynamic_text)).format(title=case_update_title)
        ).wait_for(state="visible")

    def update_priority_case_details(self, case_update_priority):
        self.case_priority_dropdown().click()
        self.page.locator(
            (Helper.get_css_locator(self.case_priority_dropdown_option)).format(
                case_priority=case_update_priority
            )
        ).click()
        self.page.locator(
            (Helper.get_css_locator(self.case_priority_dynamic_text)).format(
                priority=case_update_priority
            )
        ).last.wait_for(state="visible")

    def update_status_case_details(self, case_update_status):
        self.case_status_dropdown().click()
        self.page.locator(
            (Helper.get_css_locator(self.case_status_dropdown_option)).format(
                case_status=case_update_status
            )
        ).click()
        self.page.locator(
            (Helper.get_css_locator(self.case_status_dynamic_text)).format(
                status=case_update_status
            )
        ).wait_for(state="visible")

    def update_assigned_to_case_details(self, username):
        self.case_assigned_to_dropdown().click()
        self.page.locator(
            Helper.get_css_locator(self.case_choose_assignee).format(username=username)
        ).click()
        self.page.locator(
            (Helper.get_css_locator(self.case_assigned_to_dynamic_text)).format(
                assigned_to_email=username
            )
        ).wait_for(state="visible")

    def refresh_case_details(self):
        self.refresh_icon().click()
        self.page.locator(Helper.get_css_locator(self.case_title_text)).wait_for(state="visible")

    def get_title_on_case_details(self):
        return self.case_title_text().inner_text()

    def get_priority_on_case_details(self):
        return self.case_priority_text().inner_text()

    def get_status_on_case_details(self):
        return self.case_status_text().first.inner_text()

    def get_assigned_to_on_case_details(self):
        return self.case_assigned_to_dropdown().inner_text().split("\n", 1)[-1]

    def get_case_type(self):
        return self.case_type().inner_text().split()[2]

    def verify_newly_created_field(self, name):
        fields = self.page.query_selector_all("label.lh-input__label")
        for field in fields:
            if field.inner_text() == name:
                return True
        return False

    def verify_task_names(self, name):
        tasks = self.page.query_selector_all(Helper.get_css_locator(self.task_name))
        for task in tasks:
            if name == task.inner_text():
                return True
        return False

    def wait_for_integration_task_window_to_load(self) -> None:
        while not self.page.locator(
            Helper.get_css_locator(self.add_integration_task_headings)
        ).first.is_visible():
            pass

    def wait_for_integration_task_window_to_disappear(self) -> None:
        while self.page.locator(
            Helper.get_css_locator(self.add_integration_task_headings)
        ).first.is_visible():
            pass

    def add_integration_task(self, name: str, data: dict) -> None:
        self.add_task_btn().click()
        self.select_integration_task().click()
        dropdown_values = ["ARIN Whois", "Test_Connection_UI_Auto", "Lookup IP V2"]
        self.wait_for_integration_task_window_to_load()
        dropdown_list = self.page.query_selector_all(
            Helper.get_css_locator(self.arin_whois_connection_dropdowns)
        )[2:]
        for _ in range(len(dropdown_list)):
            dropdown_list[_].click()
            self.page.locator(
                Helper.get_css_locator(self.arin_whois_connection_dropdown_values).format(
                    value=dropdown_values[_]
                )
            ).click()
        self.next_btn().click()
        self.arin_whois_ip_field().first.fill(data["ip"])
        self.next_btn().click()
        self.fill_task_name().fill(name)
        self.save_btn().click()
        self.wait_for_integration_task_window_to_disappear()
        assert self.verify_task_names(name) == True

    def delete_a_case_on_case_details_page(self):
        self.case_delete_btn_case_details().click()
        self.page.get_by_text("Delete").first.click()
        self.page.get_by_role("button", name="Ok").click()

    def get_extracted_fields_on_case_details_page(self, case_ip, case_file_hash, case_url):
        self.page.get_by_role("tab", name="Additional fields").click()
        actual_case_ip = self.page.get_by_text(f"{case_ip}", exact=True).inner_text()
        displayed_case_file_hash = case_file_hash[:20] + "..."
        displayed_case_url = case_url[:20] + "..."
        self.page.get_by_text(f"{displayed_case_file_hash}").click()
        actual_file_hash = self.page.get_by_role("tooltip", name=f"{case_file_hash}").inner_text()
        self.refresh_btn_case_details().first.click()
        self.page.get_by_text(f"{displayed_case_url}").click()
        actual_case_url = self.page.get_by_role("tooltip", name=f"{case_url}").inner_text()
        return actual_case_ip, actual_file_hash, actual_case_url

    def goto_alerts_page(self):
        self.leftmenu_alerts_btn().click()

    def link_multiple_alerts_to_a_case_from_alerts_page(self, case_title, no_of_alerts=4):
        self.fetch_last_30days_alerts()
        for i in range(1, no_of_alerts):
            self.page.wait_for_selector(
                f"table[data-testid='react-table'] > tbody > tr:nth-child({i}) > td > div > label > div > span:nth-child(2)"
            ).click()
        self.alerts_dropdown().click()
        self.page.get_by_role("menuitem", name="Escalate to case").click()
        self.page.wait_for_selector("input[aria-label='Available cases']").click()
        self.page.get_by_text(f"{case_title}").click()
        self.page.locator("button").filter(has_text="Escalate").click()
        alert_escalation_toast_locator = self.page.locator(
            "//div[text()='Escalation to case successfully completed']"
        )
        expect(alert_escalation_toast_locator).to_be_visible()

    def fetch_last_30days_alerts(self):
        if self.page.locator("//p[text()='There are no alerts']").is_visible():
            print("There are no alerts")
        self.page.locator("//input[@placeholder='Date to']").click()
        self.page.locator("//span[text()='Last month']").click()
        self.page.wait_for_timeout(2000)
        self.page.locator("//span[text()='Apply']").click()
        self.page.wait_for_timeout(2000)

    def goto_query_section_for_linked_alerts_on_case_details(self):
        self.goto_case_listing_leftmenu_btn().click()
        self.first_case_on_case_listing_page().first.click()
        # self.first_alert_menu_icon().click()
        with self.page.expect_popup() as page2:
            # self.page.get_by_role("menuitem", name="Go to query").click()
            self.first_alert_query_button().first.click()
        alert_page = page2.value
        return alert_page.url

    def disconnect_alerts_on_case_details_page(self, no_of_alerts):
        self.goto_case_listing_leftmenu_btn().click()
        self.first_case_on_case_listing_page().first.click()
        self.page.get_by_role("checkbox").first.check()
        self.disconnect_all_alerts_dropdown().hover()
        self.page.get_by_text("Disconnect").click()
        expect(self.no_alerts_label()).to_be_visible()
        return self.no_alerts_label().is_visible()

    def goto_view_in_alerts_page_from_case_details(self):
        self.goto_case_listing_leftmenu_btn().click()
        self.first_case_on_case_listing_page().first.click()
        with self.page.expect_popup() as page2:
            self.page.get_by_role("button", name="View in alerts page").click()
        view_in_alerts_page = page2.value
        return view_in_alerts_page.url

    def update_a_field_under_additional_fields_on_case_details(
        self, test_field_name, update_field_input
    ):
        self.page.get_by_role("tab", name="Additional fields").click()
        self.page.get_by_role("button", name="Show empty fields").click()
        self.page.locator(
            f"//label[text()='{test_field_name}']/ancestor::div[3]//div[@class='lh-editable-text editable-text']"
        ).click()
        self.page.locator("#app-container").get_by_role("textbox").fill(update_field_input)
        self.page.get_by_role("button", name="icon: check").click()
        field_input_text_locator = self.page.locator(f"//p[text()='{update_field_input}']")
        expect(field_input_text_locator).to_be_visible()
        return field_input_text_locator.is_visible()

    def create_a_manual_task_on_case_details(self, manual_task_name, priority):
        self.page.get_by_role("button", name="Add Task/Automation").click()
        self.page.get_by_text("Manual Task").click()
        self.task_name_manual_input().fill(manual_task_name)
        self.page.get_by_placeholder("Task name").fill(f"{manual_task_name}")
        self.page.locator("section").get_by_role("combobox").nth(1).click()
        self.page.get_by_role("option", name=f"{priority}").click()
        self.save_btn().click()
        locator = self.page.locator(f"//p[text()='{manual_task_name}']")
        expect(locator).to_be_visible()

    def delete_a_task_on_case_details_page(self, task_name):
        self.three_dots_task_delete_icon_case_details().click()
        self.page.get_by_text("Delete").click()
        self.page.get_by_role("button", name="Ok").click()
        locator = self.page.locator(f"//p[text()='{task_name}']")
        expect(locator).not_to_be_visible()

    def update_a_task_on_case_details_page(self, case_update_assignee):
        self.page.get_by_text("Un").last.click()
        self.assignee_input().first.fill(case_update_assignee)
        self.page.locator(f"//li[text()='{case_update_assignee}']").first.click()
        locator = self.tasks_assignee_dropdown()
        expect(locator).to_have_text("P")

    def validate_casetype_tasks_on_a_case(
        self, manual_task_name, auto_task_name, onclick_task_name
    ):
        locator = self.page.locator(f"//p[text()='{manual_task_name}']")
        expect(locator).to_be_visible()
        locator = self.page.locator(f"//p[text()='{auto_task_name}']")
        expect(locator).to_be_visible()
        locator = self.page.locator(f"//p[text()='{onclick_task_name}']")
        expect(locator).to_be_visible()

    def validate_task_details_page_on_case_details(self, task_name, task_type):
        locator = self.page.locator("span.lh-case-detail__tasks-task-title > a")
        with self.page.expect_popup() as tasks_page:
            locator.click()
        tasks_details_page = tasks_page.value
        tasks_details_page.locator("p.lh-text.editable-text--text").wait_for(
            state="visible", timeout=15000
        )
        expect(tasks_details_page.locator("p.lh-text.editable-text--text")).to_contain_text(
            task_name
        )
        expect(
            tasks_details_page.locator("div.lh-case-detail-info-date > div:nth-child(4)")
        ).to_contain_text(f"{task_type}")

    def validate_manual_tasks_norun_default_msg(self):
        self.task_expand_btn().click()
        return self.manual_task_norun_default_msg().inner_text()

    def search_a_command_task(self, command_task_name):
        self.add_task_btn().click()
        self.page.get_by_text("Command Task ").click()
        expect(self.add_tasks_modal()).to_be_visible(timeout=self.timeout)
        expect(self.search_command_box()).to_be_visible(timeout=self.timeout)
        self.search_command_box().type(command_task_name)

    def validate_command_visible_after_search(self):
        expect(self.command_search_result_first_row()).to_be_visible(timeout=self.timeout)
        return self.command_search_result_first_row().text_content()

    def validate_command_no_visible_after_search(self):
        expect(self.no_commands_text()).to_be_visible(timeout=self.timeout)
        return self.no_commands_text().text_content()

    def create_command_task_using_command(self):
        self.page.locator("section").filter(has_text="1Choose command2Configure").locator(
            "label"
        ).click()
        self.next_btn().click()
        self.page.get_by_role("button", name="Insert").first.click()
        self.dropdown_description_option().first.click()
        self.page.get_by_role("button", name="Insert").nth(1).click()
        self.dropdown_description_option().last.click()
        self.next_btn().click()
        self.save_btn().click()

    def validate_command_run(self):
        expect(self.command_rerun_btn().first).to_be_visible(timeout=self.timeout * 2)

    def add_command_task_on_click(self, name: str, data: dict) -> None:
        self.add_task_btn().click()
        self.select_command_task().click()
        self.select_ip_whois_command().click()
        self.next_btn().click()
        self.arin_whois_ip_field().first.fill(data["ip"])
        self.next_btn().click()
        self.fill_task_name().fill(name)
        self.page.locator(
            Helper.get_css_locator(self.add_command_trigger).format(trigger="On Click")
        ).click()
        self.save_btn().click()
        self.wait_for_integration_task_window_to_disappear()
        assert self.verify_task_names(name) == True
