title = "Case Listing - Devo SOAR"
startUrl = "/#/cases"

elements = dict(
    select_priority_dropdown=(
        "css",
        "#priority > .rc-select-selection > .rc-select-selection__rendered",
    ),
    case_details_back_btn=("css", ".lh-case-detail-info-back > .anticon"),
    case_settings_toast_msg=("css", "div.ant-notification.ant-notification-topRight"),
    delete_multiple_cases_dropdown=("css", "span.ant-dropdown-trigger"),
    download_as_csv_btn=(
        "css",
        "button.ant-btn.lh-button.lh-secondary.lh-medium.lh-case-listing-table-actions-button:nth-child(2)",
    ),
    case_listing_reporter_column=("css", "span.lh-case-listing-table-column-user"),
    user_dropdown_card_username=("css", "div.lh-user-info-name"),
    preset_reset_button=("css", "svg.lh-case__saved-search-reset-icon"),
    filter_dropdown_button=("css", 'span.lh-button-text:has-text("Filter")'),
    save_preset_button=("css", "div.lh-case__saved-search-save"),
    preset_title_textbox=("css", 'input[placeholder="Enter name"]'),
    preset_description_textbox=("css", 'textarea[placeholder="Enter description"]'),
    save_preset_apply_button=("css", 'button[text="Apply"]'),
    saved_preset_delete_button=(
        "css",
        'div.lh-case__saved-search-list-item-right > button > span[role="img"]',
    ),
    saved_preset_delete_ok_button=("css", 'button[text="Ok"]'),
    preset_search_bar_dropdown=("css", "div.rc-select-selection__rendered"),
    preset_search_bar_input_field=("css", "input.rc-select-search__field"),
    preset_toast_msg_close_btn=("css", "a.ant-notification-notice-close"),
    selected_tags_of_a_preset=("css", "div.lh-filter-display-selected-tags"),
    empty_description_text=("css", "div.lh-editable-text-wrapper.lh-editable-text-wrapper-sticky"),
    cases_menu_btn=("css", 'tr[title="{title}"] td:last-child'),
    select_case_type=("css", 'div.rc-select-selection__placeholder:has-text("Select case type")'),
    multiple_cases_del_btn=("css", 'div.lh-custom-table-selection-item:has-text("Delete")'),
    first_case_in_the_list=("css", "tbody.ant-table-tbody tr:nth-child(1)"),
    clear_all_btn=("css", 'button[text="Clear all"]'),
    add_more_filters_btn=("css", 'button[text="+ Add more filters"]'),
    add_more_filter_container=("css", "div.lh-cascaded-field-filter-container"),
    filter_key=("css", 'span.lh-ellipsis-overflow.lh-filter-name:has-text("{filter_key}")'),
    filter_value=(
        "css",
        'div.lh-checkbox.lh-filter-menu-items-checkbox.lh-checkbox-small > label.ant-checkbox-wrapper > span:nth-child(2) > span:has-text("{filter_value}")',
    ),
    listing_data_wrapper=("css", "div.lh-custom-table-wrapper"),
    search_box=("css", "div.lh-search-box-with-search input"),
    additional_fields_tab=("css", 'div[data-node-key="Additional fields"]'),
    show_empty_fields_btn=("css", "button.lh-secondary span"),
    case_id_on_details_page=("css", "div.lh-case-detail-info-date div:nth-child(2)"),
    case_id_on_listing_page=("css", "tr:nth-child(1) td:nth-child(2) a div span:nth-child(2)"),
)
