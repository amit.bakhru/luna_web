title = "Case Listing - Devo SOAR"
startUrl = "/#/cases"

elements = dict(
    refresh_btn_case_details=("css", ".lh-case-detail-info-actions-button-options"),
    leftmenu_alerts_btn=("css", "li[id='menu.alerts'] > a"),
    alerts_dropdown=("css", "button[title='Show bulk actions']"),
    navigate_to_case_detail_from_alerts=("text", "Go to case"),
    first_alert_menu_icon=(
        "css",
        "tbody.ant-table-tbody > tr:nth-child(1) > td:last-child > div > div:nth-child(2) > i > i > svg",
    ),
    goto_case_listing_leftmenu_btn=("css", "li[id$='menu.case_management']"),
    first_case_on_case_listing_page=(
        "css",
        "tbody > tr > td:nth-child(2) > a > div > span:nth-child(2)",
    ),
    add_attachment_btn=("text", "Add attachment"),
    show_empty_fields_btn=("text", "Show empty fields"),
    select_checkbox=(
        "css",
        "div.lh-checkbox > label.ant-checkbox-wrapper > span.ant-checkbox > input.ant-checkbox-input",
    ),
    disconnect_all_alerts_dropdown=(
        "css",
        ".lh-case-detail__alerts-select-all > .ant-dropdown-trigger > .anticon > svg",
    ),
    no_alerts_label=("css", "div.lh-case-detail__alerts-empty"),
    alerts_row_on_case_details=("css", "tbody[class='ant-table-tbody'] > tr"),
    case_title_container_on_case_details=(
        "css",
        "div.lh-editable-text.editable-text.lh-case-detail-info-name-title.lh-editable-text--has-value",
    ),
    add_comment_textbox_container_on_case_details=(
        "css",
        ".lh-editable-text-wrapper > .lh-editable-text",
    ),
    attached_file_text=("css", "div.lh-case-detail__attachment-name"),
    description_section=("css", "div.lh-editable-text-wrapper.lh-editable-text-wrapper-sticky"),
    text_editor_container=("xpath", "//section[contains(@class, 'lh-case-detail__summary')])"),
    task_name_manual_input=("css", "input.lh-input.lh-medium.lh-case-manual-task-name"),
    select_priority_dropdown=(
        "css",
        "#priority > .rc-select-selection > .rc-select-selection__rendered",
    ),
    onclick_trigger_btn=(
        "css",
        "div.lh-case-integration-task-trigger  > label.ant-radio-button-wrapper:nth-of-type(2)",
    ),
    tasks_assignee_dropdown=(
        "css",
        "div.lh-case-detail__tasks-task-assignee.ant-dropdown-trigger > span > div > span > span",
    ),
    assignee_input=("css", "input.ant-input[placeholder][type='text']"),
    three_dots_task_delete_icon_case_details=(
        "css",
        "div.ant-dropdown-trigger.lh-case-detail-info-actions-button-options.lh-dropdown-trigger ",
    ),
    add_tasks_automation_button=(
        "css",
        "button.ant-btn.lh-button.ant-dropdown-trigger > span.lh-button-text",
    ),
    tasks_details_input=("css", "p.lh-text.editable-text--text"),
    tasks_details_casetype=("css", "div.lh-case-detail-info-date > div:nth-of-type(4)"),
    task_expand_btn=("css", "div.ant-collapse-item.lh-case-detail__tasks-collapse-panel"),
    manual_task_norun_default_msg=("css", "div.lh-case-detail__tasks-task-output-empty"),
    first_alert_query_button=(
        "css",
        "div.lh-case-detail__alerts-alert-query > span.lticon-database",
    ),
    case_delete_btn_case_details=(
        "css",
        "div.ant-dropdown-trigger.lh-case-detail-info-actions-button-options > span > svg",
    ),
    case_title_edit_text_icon=("css", "div.lh-case-detail-info-name > div > div"),
    case_title_edit_text_input=("css", "input.editable-text--input"),
    case_title_edit_text_confirm_button=(
        "css",
        "button.ant-btn.lh-button.lh-tertiary.lh-icon-small",
    ),
    case_title_edit_text_cancel_button=(
        "css",
        "button.ant-btn.lh-button.lh-tertiary.lh-icon-small",
    ),
    case_priority_dropdown=("css", "div.lh-form-select > #priority"),
    case_priority_dropdown_option=(
        "css",
        'ul[role="listbox"] li[role="option"][text="{case_priority}"]',
    ),
    case_status_dropdown=("css", "div.lh-form-select>#status"),
    case_status_dropdown_option=(
        "css",
        'ul[role="listbox"] li[role="option"][text="{case_status}"]',
    ),
    case_assigned_to_dropdown=("css", "div.lh-group-select-label"),
    case_choose_assignee=("css", 'span.lh-group-select-option-name:has-text("{username} (me)")'),
    case_title_text=("css", "div.lh-case-detail-info-name > div > p"),
    case_title_dynamic_text=("css", 'div.lh-case-detail-info-name > div > p:has-text("{title}")'),
    case_priority_text=("css", "#priority div.lh-case-detail-default-fields-dropdown-option"),
    case_priority_dynamic_text=(
        "css",
        'div.rc-select-selection--single div div div:has-text("{priority}")',
    ),
    case_status_text=("css", "#status div.rc-select-selection-selected-value"),
    case_status_dynamic_text=(
        "css",
        '#status div.rc-select-selection-selected-value:has-text("{status}")',
    ),
    case_assigned_to_text=("css", 'div[class*="dropdown-assignee"] div.lh-group-select-label'),
    case_assigned_to_dynamic_text=(
        "css",
        'div.lh-group-select-label:has-text("{assigned_to_email}")',
    ),
    refresh_icon=("css", 'div[class*="case-detail-info-actions-button-reload"] > svg'),
    left_arrow_icon_navigate_case_listing=("css", "div.lh-case-detail-info-back svg"),
    case_type=("css", "div.lh-case-detail-info-date div:nth-child(4)"),
    task_name=("css", "p.lh-case-detail__tasks-task-title-text"),
    add_tasks_modal=("css", "div.lh-modal-body"),
    search_command_box=("css", "div.lh-modal-body  input[placeholder='Search']"),
    no_commands_text=("css", ".lh-case-command-task-commands-empty"),
    command_search_result_first_row=(
        "css",
        ".lh-case-command-task-commands-item-right  span.ant-collapse-header-text",
    ),
    command_rerun_btn=("css", "span[type='gi-reload_refresh_update']"),
    dropdown_description_option=("xpath", "//span[text()='Description']"),
    add_task_btn=("css", 'button span:has-text("Add Task/Automation")'),
    add_integration_task_headings=("css", "div.ant-steps-item-title"),
    select_integration_task=(
        "css",
        'div.lh-case-detail__tasks-create-type-name:has-text("Integration Task")',
    ),
    select_command_task=(
        "css",
        'div.lh-case-detail__tasks-create-type-name:has-text("Command Task")',
    ),
    select_ip_whois_command=("css", 'div.lh-input-radio-container label[for="/_IP_Whois_Lookup"]'),
    add_command_trigger=("css", 'label.ant-radio-button-wrapper span:has-text("{trigger}")'),
    arin_whois_connection_dropdowns=("css", "div.rc-select-selection__rendered"),
    arin_whois_connection_dropdown_values=("css", 'li[text="{value}"]'),
    arin_whois_ip_field=("css", 'input[placeholder="Choose a field from case or enter a value"]'),
    fill_task_name=("css", "input#title"),
    next_btn=("css", 'button[text="Next"]'),
    save_btn=("css", 'button[text="Save"]'),
)

validations = []
