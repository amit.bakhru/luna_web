from pathlib import Path
from src.utils.helper import Helper
from playwright.sync_api import Page
from luna.driver.ui.wuipage import WUIPage
from src.page_objects.soar.case_management.locators import case_listing_config


class CaseListing(WUIPage):
    def __init__(self, page: Page, launch: bool = True):
        super().__init__(config=case_listing_config, page=page, launch=launch, runvalidations=True)

    def create_a_case(self, title, case_type="Default", priority="Low"):
        self.page.get_by_role("button", name="Create new case").click()
        self.select_case_type().first.click(force=True)
        self.page.get_by_role("option", name=case_type).click()
        self.page.get_by_placeholder("Enter title").first.fill(title)
        self.select_priority_dropdown().first.click(force=True)
        self.page.locator('[role="option"]', has_text=priority).first.click()
        self.page.get_by_role("button", name="Create", exact=True).first.click(force=True)
        self.page.wait_for_selector(f"css={Helper.get_css_locator(self.empty_description_text)}")

    def create_multiple_cases(self, n, title=[], type=[], priority=[]):
        """
        This function will create n number of cases
        """
        case_titles = []
        for i in range(n):
            case_title = Helper.random_string_generator() if len(title) == 0 else title[i]
            case_type = "Default" if len(type) == 0 else type[i]
            case_priority = "Low" if len(priority) == 0 else priority[i]
            self.create_a_case(case_title, case_type, case_priority)
            self.click_case_details_back_btn()
            case_titles.append(case_title)
        return case_titles

    def click_case_details_back_btn(self):
        self.case_details_back_btn().click()

    def delete_a_case(self, case_title):
        self.page.locator(
            (Helper.get_css_locator(self.cases_menu_btn)).format(title=case_title)
        ).click()
        self.page.get_by_role("menuitem", name="Delete").click()
        self.page.get_by_role("button", name="OK").click()

    def get_toast_msg(self):
        return self.case_settings_toast_msg().inner_text()

    def delete_multiple_cases_on_case_listing_page(self, case_titles):
        for case_title in case_titles:
            self.page.locator(f'css=tr[title="{case_title}"] td label span input').click()
        self.delete_multiple_cases_dropdown().first.click()
        self.multiple_cases_del_btn().click()
        self.page.get_by_role("button", name="OK").click()

    def download_and_validate_csv_on_case_listing_page(self, download_filepath):
        """
        validates download of file by clicking the download as csv button on case listing page
        Args: download_filepath (str): filepath to download csv file
        """
        script_dir = Path(__file__).resolve().parent
        file_path = script_dir.parents[2] / download_filepath
        with self.page.expect_download() as download_info:
            self.download_as_csv_btn().click()
        download = download_info.value
        download.save_as(file_path)
        if (
            file_path.is_file()
            and file_path.suffix.lower() == ".csv"
            and file_path.stat().st_size > 0
        ):
            file_path.unlink(missing_ok=True)
            return True
        else:
            file_path.unlink(missing_ok=True)
            return False

    def get_user_card_on_case_listing_page(self):
        while len(self.page.query_selector_all("tbody.ant-table-tbody tr")) != 1:
            continue
        for _ in range(2):
            self.click_filter_button()
        self.case_listing_reporter_column().hover()
        return self.user_dropdown_card_username().inner_text()

    def populate_search_box(self, data):
        self.search_box().fill(data)
        self.wait_until_case_loading_is_complete()

    def wait_for_search_results_to_be_displayed(self):
        while len(self.page.query_selector_all("tbody.ant-table-tbody tr")) != 1:
            continue

    def click_preset_reset_button(self):
        self.preset_reset_button().is_visible()
        self.preset_reset_button().click()

    def select_a_filter(self, filter_key, filter_value, click=True):
        """
        filter_key: name of the filter
        filter_value: value of the filter
        """
        if click:
            self.click_filter_button()
        self.page.locator(
            (Helper.get_css_locator(self.filter_key)).format(filter_key=filter_key)
        ).click()
        self.page.locator(
            (Helper.get_css_locator(self.filter_value)).format(filter_value=filter_value)
        ).first.click()

    def get_first_case_attribute(self, attribute_name="title"):
        return self.first_case_in_the_list().get_attribute(attribute_name)

    def clear_all_filters(self):
        self.clear_all_btn().click()

    def click_add_more_filters_btn(self):
        self.add_more_filters_btn().click()

    def is_add_more_filter_container_visible(self):
        self.page.wait_for_selector(f"css={Helper.get_css_locator(self.add_more_filter_container)}")
        return self.add_more_filter_container().is_visible()

    def save_preset(self, title, description):
        self.save_preset_button().click()
        self.preset_title_textbox().fill(title)
        self.preset_description_textbox().fill(description)
        self.save_preset_apply_button().click()

    def populate_preset_search_bar(self, title):
        self.preset_search_bar_dropdown().first.click()
        self.preset_search_bar_input_field().first.fill(title)

    def validate_saved_preset(self, title):
        self.populate_preset_search_bar(title)
        selector = (
            f'css=div.lh-case__saved-search-list-item-name.lh-ellipsis-overflow:has-text("{title}")'
        )
        self.page.locator(selector).wait_for(state="visible", timeout=10000)
        return self.page.locator(selector).is_visible()

    def delete_saved_preset(self, title):
        self.populate_preset_search_bar(title)
        selector = 'css=div.lh-case__saved-search-list-item-right > button > span[role="img"]'
        self.page.locator(selector).wait_for(state="visible", timeout=10000)
        self.saved_preset_delete_button().first.click()
        self.saved_preset_delete_ok_button().click()

    def select_a_preset_from_saved_presets(self, title):
        self.populate_preset_search_bar(title)
        self.page.locator(
            f'css=div.lh-case__saved-search-list-item-name.lh-ellipsis-overflow:has-text("{title}")'
        ).click()

    def get_list_of_selected_tags_of_a_saved_preset(self):
        filter_list = self.selected_tags_of_a_preset().all()
        tags = []
        for i in filter_list:
            tags.append(i.text_content())
        return tags

    def preset_toast_message_close_button(self):
        self.preset_toast_msg_close_btn().click()

    def click_filter_button(self):
        self.filter_dropdown_button().first.click()

    def navigate_to_additional_fields_tab(self):
        self.additional_fields_tab().click()

    def click_show_empty_fields_btn(self):
        self.show_empty_fields_btn().click()

    def get_case_id_on_case_details_page(self):
        return (self.case_id_on_details_page().inner_text().split()[1]).split("-")[0]

    def get_case_id_on_case_listing_page(self):
        return self.case_id_on_listing_page().inner_text().split("-")[0]

    def wait_until_case_loading_is_complete(self):
        self.page.wait_for_selector(f"css={Helper.get_css_locator(self.listing_data_wrapper)}")
