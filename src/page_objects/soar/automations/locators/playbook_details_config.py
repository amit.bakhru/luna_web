title = "playbooks - Automations - Devo SOAR"
startUrl = "/#/automations"

elements = dict(
    goto_automations_leftmenu_btn=("css", "li[id$='menu.automations']"),
    automation_tab_title=("text", "AUTOMATIONS Beta"),
    add_new_playbook_btn=("css", "button[text='Add new']"),
    playbook_name=("css", "p.lh-text.editable-text--text"),
    playbook_name_input_box=("css", ".editable-text--input"),
    playbook_listing_searchbox=("css", ".lh-playbooks-container input[placeholder='Search']"),
    playbook_listing_first_row=("css", ".lh-playbooks-container tr:nth-of-type(2) .lh-table-link"),
    connect_to_tool_btn=("css", "div.lh-graph-cell-containers > div:nth-child(2)"),
    tool_search_box=("css", "input[placeholder='Search for steps (ex. Extract URL)']"),
    playbook_listing_table=("css", "div.ant-table-container"),
    playbook_nodes_search_btn=("css", ".lh-playbook-builder-search-btn"),
    node_search_input_box=("css", "input[placeholder='Search Node']"),
    version_btn=("css", "span.lh-playbook-builder-version-btn"),
    save_filter_btn=("css", "button.lh-flow-search-header-save-filter"),
    filter_name_textbox=("css", ".lh-input.lh-medium.lh-flow-search-save-overlay-input"),
    save_btn=("css", ".ant-btn.lh-flow-search-save-overlay-button-save"),
    select_first_result=("css", "ul > li:first-child .lh-flow-builder-result-row-content"),
    connection_dropdown=(
        "css",
        "div[id='**connectionId**'] div[class='rc-select-selection__rendered']",
    ),
    select_imap_connection=("css", "li[text='imap_read_emails_prashant (3.0.10)'] > span"),
    create_new_imap_connection=(
        "css",
        ".rc-select-dropdown-menu-item[text='Create a new connection...(3.0.10)']",
    ),
    create_new_smtp_connection=(
        "css",
        ".rc-select-dropdown-menu-item[text='Create a new connection...(3.1.13)']",
    ),
    create_new_virus_total_connection=(
        "css",
        ".rc-select-dropdown-menu-item[text='Create a new connection...(4.1.4)']",
    ),
    next_page_btn=(
        "css",
        "button[class='ant-btn lh-button lh-alt-primary lh-medium'] span[class='lh-button-text']",
    ),
    imap_mail_box=("css", "#imap_mailbox_name"),
    imap_read_all_or_unread_dropdown=(
        "css",
        "div#imap_filter_criteria > div >div.rc-select-selection__rendered",
    ),
    imap_get_attachments=("css", "div#imap_get_attachments"),
    node_run_btn=(
        "css",
        "button[class='ant-btn ant-btn-submit ant-btn-compact-item ant-btn-compact-first-item'] > span",
    ),
    node_json_result_output=("css", "div.lh-json-viewer-row-wrapper"),
    no_records_footer=("css", "span.lh-flow-builder__output__footer__size"),
    output_skeleton=("css", "ul.ant-skeleton-paragraph"),
    connection_label_input=("css", "#__lh_label"),
    virus_total_api_key=("css", "#vt_apikey"),
    virus_total_action_type_dropdown=(
        "css",
        "div[id='vt_action_type'] div[class='rc-select-selection__rendered']",
    ),
    virus_total_action_submit_for_scan_type=(
        "css",
        ".rc-select-dropdown-menu-item[text='Submit for scan']",
    ),
    virus_total_column_name=("css", "#vt_column_name"),
    imap_connection_server_name_input=("css", "#imap_server_name"),
    smtp_connection_server_name_input=("css", "#smtp_server_name"),
    imap_connection_server_port=("css", "#imap_server_port"),
    smtp_connection_server_port=("css", "#smtp_server_port"),
    smtp_auth_type_dropdown=(
        "css",
        "div[id='smtp_authentication_type'] div[class='rc-select-selection__rendered']",
    ),
    smtp_auth_type=("css", ".rc-select-dropdown-menu-item[text='Basic']"),
    smtp_encrption_dropdown=(
        "css",
        "div[id='smtp_use_encryption'] div[class='rc-select-selection__rendered']",
    ),
    smtp_encrption=("css", ".rc-select-dropdown-menu-item[text='Yes']"),
    imap_email_input=("css", "#imap_username"),
    smtp_email_input=("css", "#smtp_email_address"),
    imap_email_password=("css", "#imap_password"),
    run_node_add_next_step_dropdown=(
        "css",
        "button.ant-btn.ant-dropdown-trigger.ant-dropdown-open > svg",
    ),
    run_node_add_next_step_btn=("css", "span.ant-dropdown-menu-title-content"),
    split_array_field=("css", "#splitArray-field"),
    split_array_name_of_new_field=("css", "#splitArray-nameOfNewField"),
    streams_btn=(
        "css",
        "button.ant-btn.lh-button.lh-alt-primary.lh-medium.lh-playbook-builder-run-btn.ant-dropdown-trigger.lh-dropdown-trigger",
    ),
    streaming_btn=(
        "css",
        'button.ant-dropdown-trigger.lh-dropdown-trigger > span:has-text("{btn_txt}")',
    ),
    streams_btn_state=(
        "css",
        "button.ant-btn.lh-button.lh-alt-primary.lh-medium.lh-playbook-builder-run-btn.ant-dropdown-trigger.lh-dropdown-trigger > svg",
    ),
    stream_name=("css", "div.lh-stream-edit input.lh-input.lh-medium "),
    stream_batch_length=("css", "input[value='15']"),
    stream_execution_delay=("css", "input#executionDelay"),
    stream_save_btn=("css", "button[text='Save']"),
    search_result_length=("css", "div.lh-flow-search-matched-nodes-header-length"),
    search_results=("css", "p.lh-highlighted-text.lh-flow-search-matches-match-value"),
    version_list=("css", ".lh-playbook-versions-dropdown-list-item"),
    sql_node_text_area_input=("css", "textarea.ace_text-input"),
    sql_run_btn=("css", "button[text='Run'] > span"),
    open_stream_batch_detail_page=(
        "css",
        ".lh-playbook-builder-streams-row-name span.lh-playbook-builder-streams-row-name-icon",
    ),
    batch_details_first_row=(
        "css",
        "tr[total='2'][state='ready'][version='3'][type='Batch']:last-child > td:nth-of-type(5)",
    ),
    batch_details_updated_last_row=(
        "css",
        "tr[total='0'][state='ready'][version='5'][type='Batch']:last-child > td:nth-of-type(5)",
    ),
    used_by_stream_tag=("css", "span.lh-playbook-versions-dropdown-list-tag"),
    stream_creation_notification_msg=("css", "div.ant-notification-notice-message"),
    output_node=("css", "div.lh-graph-cell-output div.lh-graph-cell-content-title"),
    set_as_latest=(
        "css",
        "div.lh-playbook-versions-dropdown-list-item:nth-of-type({node_version}) .lh-playbook-versions-dropdown-list-btn:nth-of-type(1)",
    ),
    total_number_of_nodes=(
        "css",
        "g.joint-cell.joint-type-standard.joint-type-standard-headeredrectangle.joint-element.joint-theme-default",
    ),
    reverted_version=(
        "css",
        "div.lh-playbook-versions-dropdown-list-item:nth-of-type({node_version}) .lh-playbook-versions-dropdown-list-description",
    ),
    batch_data_table=("css", "table.lh-dynamic-grid-table"),
    batch_data_rows=("css", "tr.lh-dynamic-grid-table-row"),
    extract_json_table_row=("css", "div.lh-sql-table-row"),
    extract_json_url=("xpath", "//*[contains(text(), '$.result.URLs')]"),
    extract_json_subject=("xpath", "//*[contains(text(), '$.result.Subject')]"),
    three_dots_node_menu=("css", "span.lh-flow-builder-sidebar-titlebar-actions-dropdown > svg"),
    stream_name_menu=("css", "span.lh-playbook-builder-streams-row-name"),
    active_first_step=("xpath", "//div[contains(text(), '1') and contains(@class, 'step active')]"),
    active_two_step=("xpath", "//div[contains(text(), '2') and contains(@class, 'step active')]"),
    active_third_step=("xpath", "//div[contains(text(), '3') and contains(@class, 'step active')]"),
    side_drawer=("css", "div.lh-flow-builder-catalog-header"),
    top_drop_down_menu=("css", "div.lh-graph-actions-dropdown"),
    latest_version=("css", "div.lh-playbook-versions-dropdown-list-item-selected"),
    search_result_title=("css", "div.lh-flow-builder__container__data__content-header-title"),
    refresh_button=("css", "button.lh-stream-refresh-btn"),
    publish_to_old_version=(
        "css",
        "div.lh-playbook-versions-dropdown-list-item:nth-of-type(3) .lh-playbook-versions-dropdown-list-btn:nth-of-type(2)",
    ),
    run_and_next_step=("css", "span.ant-dropdown-menu-title-content"),
    run_dropdown_btn=("css", "button.ant-btn-compact-last-item"),
    pb_action_dropdown_btn=(
        "css",
        "div.lh-flow-builder__header__wrapper-right > span.ant-dropdown-trigger.lh-dropdown-trigger > span",
    ),
    pb_action_list=("css", "div.ant-dropdown-placement-bottomRight ul.lh-playbook-builder-menu"),
    # pb_delete_menu_btn=('css', "li[data-menu-id*='Delete']"),
    pb_delete_menu_btn=("xpath", "//span[text()='Delete']"),
    close_notification_btn=("css", "span.lh-notification-close-icon"),
    delete_pb_confirmation_btn=("css", "div.lh-form__button-group > button:nth-of-type(2)"),
)

validations = []
