title = "playbooks - Automations - Devo SOAR"
startUrl = "/#/automations"

elements = dict(
    add_new_playbook_btn=("css", "button[text='Add new']"),
    playbook_name=("css", "p.lh-text.editable-text--text"),
    playbook_name_input_box=("css", ".editable-text--input"),
)

validations = []
