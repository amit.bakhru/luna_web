title = "commands - Automations - Devo SOAR"
startUrl = "/#/automations"

elements = dict(
    goto_automations_leftmenu_btn=("css", "li[id$='menu.automations']"),
    playbook_listing_table=("css", "div.ant-table-container"),
    create_new_command_btn=(
        "css",
        "button[class='ant-btn lh-button lh-primary lh-medium lh-case-config-commands-header-add-button'] span[class='lh-button-text']",
    ),
    command_name=(
        "css",
        "div.lh-flow-builder__header__wrapper__command-name p.lh-text.editable-text--text",
    ),
    command_name_input=(
        "css",
        "div.lh-flow-builder__header__wrapper__command-name input.editable-text--input",
    ),
    command_description=(
        "css",
        "div.lh-flow-builder__header__wrapper__command-description p.lh-text.editable-text--text",
    ),
    command_description_input=(
        "css",
        "div.lh-editable-text-editing.editable-text--wrapper  input.editable-text--input",
    ),
    parameter_node=("css", "div.lh-graph-cell-content-title"),
    parameter0=("css", "#parameterName0"),
    parameter_desc0=("css", "#parameterDescription0"),
    required_parameter0=("css", "#parameterMandatory0"),
    parameter1=("css", "#parameterName2"),
    parameter_desc1=("css", "#parameterDescription2"),
    required_parameter1=("css", "#parameterMandatory2"),
    parameter2=("css", "#parameterName3"),
    parameter_desc2=("css", "#parameterDescription3"),
    parameter_default2=("css", "#parameterDefault3"),
    add_param=(
        "css",
        "button[class='ant-btn lh-button lh-secondary lh-medium lh-margin--top--md'] span[class='lh-button-text']",
    ),
    next_page_btn=(
        "css",
        "button[class='ant-btn lh-button lh-alt-primary lh-medium'] span[class='lh-button-text']",
    ),
    enter_param0_sample=("css", "#parameterName0_sample"),
    enter_param1_sample=("css", "#parameterName2_sample"),
    parameter_node_json_result=("css", ".lh-json-viewer-value-string"),
    mark_as_down_command=(
        "css",
        "button[class='ant-btn lh-button lh-primary lh-medium lh-playbook-builder-menu-btn'] span[class='lh-button-text']",
    ),
    step_to_outputs_dropdown=(
        "css",
        "div.lh-flow-builder-output-modal-form div.rc-select-selection__rendered",
    ),
    parameter_node_option=("css", "span[title='Parameter_Node'] > p"),
    save_btn=("css", "button[text='Save']"),
    help_link_save_btn=("css", "ul[role='menu'] button[text='Save']"),
    command_listing_first_configured_row=(
        "css",
        ".ant-table-tbody tr:nth-of-type(2) td:nth-of-type(3)",
    ),
    command_help_link=("css", "span.lh-playbook-builder-link-btn"),
    help_link_menu=("css", "ul[role='menu']"),
    help_link_input=("css", "ul[role='menu'] input[placeholder='Enter link']"),
    command_back_btn=("css", "button.lh-flow-builder__header__wrapper__back-button"),
    add_node_btn=("css", ".lh-graph-cell-actions-action"),
    side_drawer=("css", "div.lh-flow-builder__container__data"),
    search_result_title=("css", "div.lh-flow-builder__container__data__content-header-title"),
    search_first_result=("css", "ul > li:first-child .lh-flow-builder-result-row-content"),
    connection_dropdown=(
        "css",
        "div[id='**connectionId**'] div[class='rc-select-selection__rendered']",
    ),
    connection_label_input=("css", "#__lh_label"),
    active_third_step=("xpath", "//div[contains(text(), '3') and contains(@class, 'step active')]"),
    arin_whois_connection=("xpath", '//li[contains(text(),"Create a new connection")]'),
    arin_whois_column_name=("css", "#arin_whois_column_name"),
    node_run_btn=(
        "css",
        "button[class='ant-btn ant-btn-submit ant-btn-compact-item ant-btn-compact-first-item'] > span",
    ),
    node_json_result_output=("css", "div.lh-json-viewer-row-wrapper"),
    skeleton_after_run=(
        "css",
        "div.ant-skeleton.ant-skeleton-active.lh-json-viewer-row-wrapper-loading:nth-of-type(1)",
    ),
    parameter_node_action_menu_btn=("css", ".lh-graph-cell-actions > .ant-dropdown-trigger"),
    parameter_node_action_menu=(
        "css",
        "div.ant-dropdown.lh-graph-actions-dropdown.ant-dropdown-placement-bottomLeft",
    ),
    notification_msg=("css", "div.ant-notification-notice-message"),
    pn_delete_error_description=("css", "div.ant-notification-notice-description"),
    arin_node_option=("css", "span[title='ARIN Whois - Lookup IP V2'] > p"),
    pb_action_dropdown_btn=(
        "css",
        "div.lh-flow-builder__header__wrapper-right > span.ant-dropdown-trigger.lh-dropdown-trigger > span",
    ),
    pb_action_list=("css", "div.ant-dropdown-placement-bottomRight ul.lh-playbook-builder-menu"),
    pb_delete_menu_btn=("xpath", "//span[text()='Delete']"),
    close_notification_btn=("css", "span.lh-notification-close-icon"),
    delete_pb_confirmation_btn=("css", "div.lh-form__button-group > button:nth-of-type(2)"),
    confirm_btn=("css", "div.editable-text--options button:first-child"),
)

validations = []
