from luna.driver.ui.wuipage import WUIPage
from playwright.sync_api import expect
from playwright.sync_api import Page
from src.page_objects.soar.automations.locators import playbook_listing_config


class PlaybookListing(WUIPage):
    def __init__(self, page: Page, launch: bool = True):
        super().__init__(
            config=playbook_listing_config, page=page, launch=launch, runvalidations=True
        )
        self.timeout = self.master_config["common"]["timeout"] * 1000

    def create_a_playbook(self, pb_name):
        self.add_new_playbook_btn().click()
        expect(self.playbook_name()).to_be_visible(timeout=self.timeout)
        self.playbook_name().click()
        self.playbook_name_input_box().clear()
        self.playbook_name_input_box().fill(pb_name)
        self.playbook_name_input_box().press("Enter")
