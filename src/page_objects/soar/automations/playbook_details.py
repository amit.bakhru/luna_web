from playwright.sync_api import Page
from luna.driver.ui.wuipage import WUIPage
from src.page_objects.soar.automations.locators import playbook_details_config
from playwright.sync_api import expect

# import random
from src.utils.helper import Helper
from luna.framework.common.logger import LOGGER


class PlaybookDetails(WUIPage):
    def __init__(self, page: Page, launch: bool = True):
        super().__init__(
            config=playbook_details_config, page=page, launch=launch, runvalidations=True
        )
        self.batch_detail_page = None
        self.timeout = self.master_config["common"]["timeout"] * 1000

    def goto_automation_page(self):
        if "#/automations" not in self.page.url:
            self.goto_automations_leftmenu_btn().click()
            assert self.playbook_listing_table().is_visible()

    def search_tool(self, tool_name):
        expect(self.tool_search_box()).to_be_visible(timeout=self.timeout)
        self.tool_search_box().fill(tool_name)
        self.page.wait_for_timeout(500)

    def click_connect_to_tool_btn(self):
        self.connect_to_tool_btn().click()

    def add_imap_read_email_node(
        self,
        imap_mail_box,
        imap_read_all_or_unread,
        connection_name="Test IMAP Connection 6025090 (3.0.10)",
    ):
        self.search_tool("IMAP - Read Emails")
        self.select_first_result().click()
        expect(self.active_first_step()).to_be_visible(timeout=self.timeout)
        self.check_imap_connection(connection_name)
        self.imap_mail_box().fill(imap_mail_box)
        self.imap_read_all_or_unread_dropdown().click()
        self.page.get_by_role("option", name=imap_read_all_or_unread).click()
        self.page.get_by_role("button", name="Show Optional Fields").click()
        self.imap_get_attachments().click()
        self.page.get_by_role("option", name="False").click()
        self.page.get_by_placeholder("Filtering: Message Limit").click()
        self.page.get_by_placeholder("Filtering: Message Limit").fill("10")

    def add_smtp_node(
        self, recipient, body, subject, connection_name="Test SMTP Connection 5515500 (3.1.13)"
    ):
        self.page.get_by_text("All", exact=True).click()
        self.search_tool("SMTP - Send Email")
        self.select_first_result().click()
        expect(self.active_first_step()).to_be_visible(timeout=self.timeout)
        self.check_smtp_connection(connection_name)
        self.page.get_by_role("textbox", name="Recipients To *").fill(recipient)
        self.page.get_by_role("textbox", name="Body *").fill(body)
        self.page.get_by_role("textbox", name="Subject *").fill(subject)

    def add_virus_total_analyze_url_node(self, column_name, connection_name="vt_cred_2 (4.1.4)"):
        self.search_tool("Virus Total - Analyze URL")
        self.select_first_result().click()
        expect(self.active_first_step()).to_be_visible(timeout=self.timeout)
        self.check_virus_total_connection(connection_name)
        self.virus_total_action_type_dropdown().click()
        self.virus_total_action_submit_for_scan_type().click()
        self.virus_total_column_name().fill(column_name)

    def run_a_node_wait_for_result(self):
        expect(self.node_run_btn()).to_be_visible(timeout=self.timeout)
        self.node_run_btn().click()
        out_json_result = self.node_json_result_output()
        out_json_result.first.wait_for(state="visible", timeout=600000)
        expect(out_json_result.first).to_be_visible()

    def add_split_array_node(self, split_array_field, name_of_new_field):
        self.page.get_by_text("Transform", exact=True).click()
        self.search_tool("Split Array Operator")
        self.select_first_result().click()
        expect(self.split_array_field()).to_be_visible(timeout=self.timeout)
        self.split_array_field().fill(split_array_field)
        self.split_array_name_of_new_field().fill(name_of_new_field)

    def create_virus_total_connection(self, label_name, vt_api_key):
        self.connection_dropdown().click()
        self.create_new_virus_total_connection().click()
        self.next_page_btn().click()
        self.connection_label_input().fill(label_name)
        self.virus_total_api_key().fill(vt_api_key)
        self.next_page_btn().click()
        expect(self.active_third_step()).to_be_visible(timeout=self.timeout)

    def create_imap_connection(self, label_name, server_name, server_port, email, password):
        self.connection_dropdown().click()
        self.create_new_imap_connection().click()
        self.next_page_btn().click()
        self.connection_label_input().fill(label_name)
        self.imap_connection_server_name_input().fill(server_name)
        self.imap_connection_server_port().fill(server_port)
        self.imap_email_input().fill(email)
        self.imap_email_password().fill(password)
        self.next_page_btn().click()
        expect(self.active_third_step()).to_be_visible(timeout=self.timeout)

    def run_node_add_next_step(self):
        self.run_dropdown_btn().click()
        expect(self.run_and_next_step()).to_be_visible(timeout=self.timeout)
        self.page.get_by_text("Run and add next step").click()
        expect(self.side_drawer()).to_be_visible(timeout=self.timeout)
        expect(self.tool_search_box()).to_be_visible(timeout=self.timeout)

    def create_smtp_connection(
        self, smtp_label, smtp_server_name, smtp_server_port, smtp_email, password
    ):
        self.connection_dropdown().click()
        self.create_new_smtp_connection().click()
        self.next_page_btn().click()
        self.connection_label_input().fill(smtp_label)
        self.smtp_connection_server_name_input().fill(smtp_server_name)
        self.smtp_connection_server_port().fill(smtp_server_port)
        self.smtp_auth_type_dropdown().click()
        self.smtp_auth_type().click()
        self.smtp_encrption_dropdown().click()
        self.smtp_encrption().click()
        self.smtp_email_input().fill(smtp_email)
        self.page.get_by_role("button", name="Show Optional Fields").click()
        self.page.get_by_role("textbox", name="Password").fill(password)
        self.next_page_btn().click()
        expect(self.active_third_step()).to_be_visible(timeout=self.timeout)

    def add_output_node(self):
        self.three_dots_node_menu().first.click()
        self.page.get_by_role("switch").click()
        output_node = self.output_node()
        output_node.first.wait_for(state="visible")
        expect(output_node.first).to_be_visible(timeout=self.timeout)

    def schedule_a_pb_stream(self, stream_name, batch_length, execution_delay):
        expect(self.streams_btn_state()).to_be_visible(timeout=self.timeout)
        self.streams_btn().click()
        self.stream_name().clear()
        self.stream_name().fill(stream_name)
        self.stream_batch_length().clear()
        self.stream_batch_length().fill(batch_length)
        self.stream_execution_delay().clear()
        self.stream_execution_delay().fill(execution_delay)
        self.stream_save_btn().click()
        expect(self.stream_creation_notification_msg()).to_be_visible(timeout=self.timeout)
        notification_msg = self.stream_creation_notification_msg().text_content()
        self.close_notification_btn().click()
        return notification_msg

    def pb_node_search(self, search_node_txt):
        self.playbook_nodes_search_btn().click()
        self.node_search_input_box().fill(search_node_txt)
        expect(self.search_result_length()).to_be_visible(timeout=self.timeout)
        search_result_length = self.search_result_length().text_content()
        search_results = self.page.query_selector_all(Helper.get_css_locator(self.search_results))
        return search_result_length, search_results

    def check_smtp_connection(self, connection_name):
        if self.check_connection_exists(connection_name):
            connection_locator = self.page.locator(
                f".rc-select-dropdown-menu-item[text='{connection_name}']"
            )
            connection_locator.click()
            self.next_page_btn().click()
        else:
            self.create_smtp_connection(
                Helper.random_string_generator_with_preset("Test_SMTP_Connection_"),
                "smtp.gmail.com",
                "587",
                "testvarun2611@gmail.com",
                "rtgvlsfasfapmntg",
            )

    def check_connection_exists(self, connection_name):
        self.connection_dropdown().click()
        return self.page.locator(
            f".rc-select-dropdown-menu-item[text='{connection_name}']"
        ).is_visible()

    def check_imap_connection(self, connection_name):
        if self.check_connection_exists(connection_name):
            connection_locator = self.page.locator(
                f".rc-select-dropdown-menu-item[text='{connection_name}']"
            )
            connection_locator.click()
            self.next_page_btn().click()
        else:
            self.create_imap_connection(
                Helper.random_string_generator_with_preset("Test_IMAP_Connection_"),
                "imap.gmail.com",
                "993",
                "testvarun2611@gmail.com",
                "rtgvlsfasfapmntg",
            )

    def check_virus_total_connection(self, connection_name):
        if self.check_connection_exists(connection_name):
            connection_locator = self.page.locator(
                f".rc-select-dropdown-menu-item[text='{connection_name}']"
            )
            connection_locator.click()
            self.next_page_btn().click()
        else:
            self.create_virus_total_connection(
                Helper.random_string_generator_with_preset("Test_Virus_Total_Connection_"),
                "91893522dd5d874c9d5431a154f045fda695cebb8fbc853abd4ec91a43f8117e",
            )

    def get_versions_of_pb(self):
        self.version_btn().click()
        expect(self.latest_version()).to_be_visible(timeout=self.timeout)
        version_list = self.page.query_selector_all(Helper.get_css_locator(self.version_list))
        return version_list

    def get_total_number_of_nodes(self):
        total_number_of_nodes = self.page.query_selector_all(
            Helper.get_css_locator(self.total_number_of_nodes)
        )
        return len(total_number_of_nodes)

    def add_sql_node(self, sql):
        self.search_tool("sql")
        self.select_first_result().click()
        expect(self.sql_node_text_area_input()).to_be_visible(timeout=self.timeout)
        self.sql_node_text_area_input().fill(sql)
        self.run_sql_node_wait_for_result()

    def update_sql_node(self, update_sql):
        self.sql_node_text_area_input().fill("")
        self.sql_node_text_area_input().fill(update_sql)
        self.run_sql_node_wait_for_result()

    def run_sql_node_wait_for_result(self):
        self.sql_run_btn().click()
        out_json_result = self.node_json_result_output()
        out_json_result.first.wait_for(state="visible", timeout=self.timeout)
        expect(out_json_result.first).to_be_visible()

    def validate_batch_before_update(self):
        btn_txt = "Streaming (1)"
        streaming_btn = self.page.locator(
            (Helper.get_css_locator(self.streaming_btn)).format(btn_txt=btn_txt)
        )
        expect(streaming_btn).to_be_visible(timeout=self.timeout)
        streaming_btn.click()
        self.stream_name_menu().wait_for(state="visible", timeout=self.timeout)
        self.stream_name_menu().hover()
        with self.page.expect_popup() as page2:
            self.open_stream_batch_detail_page().click()
        self.batch_detail_page = page2.value
        return self.batch_detail_page.locator(
            "tr[total='2'][state='ready'][version='3'][type='Batch']:last-child > td:nth-of-type(5)"
        ).text_content()

    def validate_batch_after_update(self):
        batches = self.batch_detail_page.query_selector_all("tr[type='Batch']")
        no_of_batches = len(batches)
        LOGGER.info(f"No of current batches {no_of_batches}")
        self.batch_detail_page.locator("button.lh-stream-refresh-btn").wait_for(
            state="visible", timeout=2.5 * self.timeout
        )
        self.batch_detail_page.locator("button.lh-stream-refresh-btn").click()
        expect(self.batch_detail_page.locator("button.lh-stream-refresh-btn")).to_be_hidden(
            timeout=self.timeout
        )
        if no_of_batches == 1:
            row_locator_1 = self.batch_detail_page.locator(
                "tr[total='0'][state='ready'][version='5'][type='Batch']:nth-last-of-type(2) td:nth-of-type(5)"
            )
            expect(row_locator_1).to_be_visible(timeout=self.timeout)
            return row_locator_1.text_content()
        elif no_of_batches == 2:
            row_locator_2 = self.batch_detail_page.locator(
                "tr[total='0'][state='ready'][version='5'][type='Batch']:nth-last-of-type(3) td:nth-of-type(5)"
            )
            expect(row_locator_2).to_be_visible(timeout=self.timeout)
            return row_locator_2.text_content()

    def select_playbook_on_listing_page(self, pb_name):
        self.playbook_listing_searchbox().fill(pb_name)
        self.playbook_listing_first_row().click()

    def validate_batch_after_publish_update(self):
        refresh_button = self.batch_detail_page.locator("button.lh-stream-refresh-btn")
        refresh_button.wait_for(state="visible", timeout=2.5 * self.timeout)
        refresh_button.click()
        expect(self.batch_detail_page.locator("button.lh-stream-refresh-btn")).to_be_hidden(
            timeout=self.timeout
        )
        return self.batch_detail_page.locator(
            "tr[total='2'][state='ready'][version='3'][type='Batch']:nth-last-of-type(3) td:nth-of-type(5)"
        ).text_content()

    def publish_old_version(self):
        self.version_btn().click()
        expect(self.latest_version()).to_be_visible(timeout=self.timeout)
        self.publish_to_old_version().click()
        used_by_stream_tag = self.used_by_stream_tag()
        used_by_stream_tag.wait_for(state="visible", timeout=self.timeout)
        expect(used_by_stream_tag).to_be_visible()
        return self.used_by_stream_tag().text_content()

    def pb_node_set_as_latest_version(self, node_version):
        self.version_btn().click()
        expect(self.latest_version()).to_be_visible(timeout=self.timeout)
        self.page.locator(
            (Helper.get_css_locator(self.set_as_latest)).format(node_version=node_version)
        ).click()
        output_node = self.output_node()
        output_node.first.wait_for(state="hidden")
        expect(output_node.first).to_be_hidden()

    def validate_reverted_on_pb_version(self, node_version):
        return self.page.locator(
            (Helper.get_css_locator(self.reverted_version)).format(node_version=node_version)
        ).text_content()

    def validate_batch_data_with_pb(self):
        self.batch_detail_page.locator("tr[type='Batch']:last-child").click()
        self.batch_detail_page.wait_for_timeout(2000)
        table = self.batch_detail_page.locator("table.lh-dynamic-grid-table")
        rows = table.locator("tr.lh-dynamic-grid-table-row")
        for i in range(rows.count()):
            row = rows.nth(i)
            col1_value = row.locator(
                "td:nth-child(1) .lh-dynamic-grid-table-cell-value"
            ).text_content()
            col2_value = row.locator(
                "td:nth-child(2) .lh-dynamic-grid-table-cell-value"
            ).text_content()
            if i == 0:
                assert col1_value == "15" and col2_value == "16"
            elif i == 1:
                assert col1_value == "17" and col2_value == "18"

    def add_extract_json(self):
        self.page.get_by_text("Transform", exact=True).click()
        self.search_tool("Extract JSON")
        self.select_first_result().click()
        self.extract_json_table_row().wait_for(state="visible", timeout=self.timeout)
        self.extract_json_table_row().hover()
        self.page.get_by_role("button", name="", exact=True).click()
        self.page.locator("form").get_by_role("combobox").locator("div").first.click()
        self.extract_json_url().click()
        self.page.locator("form").get_by_role("combobox").locator("div").first.click()
        self.extract_json_subject().click()

    def delete_playbook(self):
        while self.pb_action_list().is_hidden():
            self.pb_action_dropdown_btn().dispatch_event("click")
        self.pb_delete_menu_btn().dispatch_event("click")
        self.delete_pb_confirmation_btn().click()
