from playwright.sync_api import Page
from luna.driver.ui.wuipage import WUIPage
from src.page_objects.soar.automations.locators import command_details_config
from playwright.sync_api import expect
from src.page_objects.soar.automations.playbook_details import PlaybookDetails
from src.utils.helper import Helper
from luna.config import config
from src.constants import STAGE_COMMAND_DELETE_API, STAGE_COMMAND_SEARCH_API
import requests


class CommandDetails(WUIPage):
    def __init__(self, page: Page, launch: bool = True):
        super().__init__(
            config=command_details_config, page=page, launch=launch, runvalidations=True
        )
        self.playbook_details = PlaybookDetails(page=self.page, launch=False)
        self.timeout = self.master_config["common"]["timeout"] * 1000
        self.url = config["servers"]["soar"]["url"]
        self.headers = {
            "Content-Type": "application/json",
            "authorization": f"Bearer {config['servers']['soar']['authorization']}",
        }

    def goto_automation_page(self):
        if "#/automations" not in self.page.url:
            self.goto_automations_leftmenu_btn().click()
            expect(self.playbook_listing_table()).to_be_visible()
            assert self.playbook_listing_table().is_visible()

    def create_command(self):
        self.create_new_command_btn().click()
        return self.command_name().text_content()

    def edit_command_name(self, command_name):
        self.command_name().click()
        self.command_name_input().clear()
        self.command_name_input().fill(command_name)
        self.page.get_by_role("button", name="check").click()
        expect(self.command_name()).to_be_visible(timeout=self.timeout)
        expect(self.command_name()).to_have_text(command_name)

    def edit_description(self, description):
        self.command_description().click()
        self.command_description_input().fill(description)
        self.page.get_by_role("button", name="check").click()
        expect(self.command_description()).to_have_text(description)

    def add_command_name_description(
        self, command_name, command_description="This is ui test command"
    ):
        self.create_new_command_btn().click()
        self.edit_command_name(command_name)
        self.edit_description(command_description)
        return self.command_name().text_content(), self.command_description().text_content()

    def goto_tab_on_automation(self, tab_name_on_case_settings):
        self.page.get_by_role("tab", name=tab_name_on_case_settings).click()

    def add_parameter_node(
        self,
        parameter0,
        description0,
        parameter1,
        description1,
        parameter2,
        description2,
        default2,
        sample1,
        sample2,
    ):
        self.parameter_node().click()
        self.fill_parameter_fields(
            self.parameter0,
            self.parameter_desc0,
            self.required_parameter0,
            parameter0,
            description0,
        )
        self.fill_parameter_fields(
            self.parameter1,
            self.parameter_desc1,
            self.required_parameter1,
            parameter1,
            description1,
        )
        self.parameter2().fill(parameter2)
        self.parameter_desc2().fill(description2)
        self.parameter_default2().fill(default2)
        self.next_page_btn().click()
        self.enter_param0_sample().fill(sample1)
        self.enter_param1_sample().fill(sample2)
        self.playbook_details.run_a_node_wait_for_result()
        json_result = self.page.query_selector_all(
            Helper.get_css_locator(self.parameter_node_json_result)
        )
        json_texts = [locator.text_content() for locator in json_result]
        return json_texts

    def fill_parameter_fields(
        self, parameter_locator, description_locator, required_locator, parameter, description
    ):
        parameter_locator().fill(parameter)
        description_locator().fill(description)
        required_locator().click()
        self.add_param().click()

    def mark_command_done(self, node_name):
        expect(self.mark_as_down_command()).to_be_visible()
        self.mark_as_down_command().click()
        self.step_to_outputs_dropdown().click()
        if node_name == "parameter":
            self.parameter_node_option().click()
        elif node_name == "arin":
            self.arin_node_option().click()
        self.save_btn().click()
        return self.get_created_command_status()

    def get_created_command_status(self):
        return self.command_listing_first_configured_row().text_content()

    def save_help_link(self, help_link):
        self.command_help_link().click()
        expect(self.help_link_menu()).to_be_visible()
        self.help_link_input().fill(help_link)
        self.help_link_save_btn().click()
        expect(self.help_link_menu()).to_be_hidden()

    def help_link_save(self):
        self.command_help_link().click()
        expect(self.help_link_menu()).to_be_visible()
        return self.help_link_input().input_value()

    def click_navigate_back_btn(self):
        self.command_back_btn().click()
        return self.page.url

    def click_create_command_btn(self):
        self.create_new_command_btn().click()

    def add_arin_whois_node(self, column_name):
        self.parameter_node().hover()
        self.add_node_btn().first.click()
        expect(self.side_drawer()).to_be_visible()
        self.playbook_details.search_tool("arin")
        expect(self.search_result_title()).to_be_visible()
        self.search_first_result().click()
        self.connection_dropdown().click()
        self.arin_whois_connection().click()
        self.next_page_btn().click()
        connection_name = Helper.random_string_generator_with_preset("Test_Arin_Connection_")
        self.connection_label_input().fill(connection_name)
        self.next_page_btn().click()
        expect(self.active_third_step()).to_be_visible(timeout=self.timeout)
        self.arin_whois_column_name().fill(column_name)
        self.run_a_node_wait_for_result()

    def run_a_node_wait_for_result(self):
        self.node_run_btn().click()
        expect(self.skeleton_after_run()).to_be_visible(timeout=self.timeout)
        out_json_result = self.node_json_result_output()
        out_json_result.first.wait_for(state="visible", timeout=self.timeout)
        expect(out_json_result.first).to_be_visible()

    def validate_delete_error_message_of_parameter_node(self):
        self.parameter_node().hover()
        self.parameter_node_action_menu_btn().click()
        expect(self.parameter_node_action_menu()).to_be_visible(timeout=self.timeout)
        self.page.get_by_text("Delete this step").click()
        self.page.get_by_role("button", name="Yes delete").click()
        return (
            self.notification_msg().text_content(),
            self.pn_delete_error_description().text_content(),
        )

    def get_command_name_on_command_details(self):
        return self.command_name().text_content()

    def delete_playbook(self):
        while self.pb_action_list().is_hidden():
            self.pb_action_dropdown_btn().dispatch_event("click")
        self.pb_delete_menu_btn().dispatch_event("click")
        self.delete_pb_confirmation_btn().click()

    def command_api_cleanup(self, command_name):
        response = requests.post(
            self.url + STAGE_COMMAND_SEARCH_API,
            json={
                "filters": [{"searchText": command_name}],
                "offset": 0,
                "pageSize": 50,
                "sortColumn": "lastUpdated",
                "sortOrder": "DESC",
            },
            headers=self.headers,
        )
        search_data = response.json()["result"]["data"]["data"]
        flow_id = search_data[0]["flowId"]
        command_delete_payload = self.create_command_delete_payload(flow_id)
        requests.post(
            self.url + STAGE_COMMAND_DELETE_API, json=command_delete_payload, headers=self.headers
        )

    @staticmethod
    def create_command_delete_payload(flow_id):
        payload = [{"id": flow_id.split("-")[1], "entityTypeId": "command"}]
        return payload
