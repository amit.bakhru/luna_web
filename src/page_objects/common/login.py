from src.utils.helper import Helper
from playwright.sync_api import Page
from luna.driver.ui.wuipage import WUIPage
from src.base.base_config import BaseConfig
from src.page_objects.common import login_config


class Login(WUIPage):
    def __init__(self, page: Page):
        super().__init__(config=login_config, page=page, launch=True, runvalidations=True)
        self.left_nav_bar = page.locator("nav[id$='main-navigation']")
        self.applications_open_button = page.locator("li[id$='menu.applications']")

    def login(self, username, password):
        _url = self.master_config["servers"]["soar"]["url"]
        self.page.wait_for_load_state("networkidle")
        self.username().fill(username)
        self.password().fill(password)
        self.login_button().click()
        if _url.find("mr") == -1:
            self.page.locator(
                Helper.get_css_locator(self.domain).format(domain=BaseConfig.get_domain_name())
            ).click()
        self.page.wait_for_url(_url)

    def got_to_soar_application(self, deployment_name):
        self.application_tab().click()
        path = f"//*[text()='{deployment_name}']//parent::a"
        self.page.wait_for_selector(path)
        self.page.locator(path).click()
        self.page.wait_for_timeout(3000)
        self.application_tab().click()
        self.page.locator(path).click()

    def go_to_ueba_application(self, deployment_name):
        # self.page.wait_for_url(UEBA_HOME_URL, timeout=120000)
        self.left_nav_bar.wait_for(state="visible")
        self.left_nav_bar.hover()
        self.applications_open_button.click()
        self.page.locator("li[id$='app.custom.Behavior_Analytics_V2'] > a").first.dblclick()
        self.page.wait_for_timeout(5000)
