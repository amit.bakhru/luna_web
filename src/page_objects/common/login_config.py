title = "Log in | Devo"
startUrl = "/login"

elements = dict(
    username=("css", 'input[id="loginEmail"]'),
    password=("css", 'input[id="loginPass"]'),
    login_button=("css", 'button[id="btSignIn"]'),
    domain=("css", 'li[title="{domain}"]'),
    application_tab=("css", 'li[id="menu.applications"]'),
)
