const fs = require('fs');

/**
 * This function will pause the lighthouse execution for the specifed timeout
 * During this time interval, playwright will connect to the existing browser context
 * By establishing the CDP session
 * And will perform the login
 */

const INTERVAL_TIMEOUT = 1000;       // 1000ms
const MAX_TIMEOUT = 60;              // 60s
const DATA_FILE_PATH = "o/output/performance/data.txt";

const custom_wait = async () => {
    console.log('Starting wait...');
    for (let i = 0; i <= MAX_TIMEOUT; i ++) {
        try {
            if (fs.readFileSync(DATA_FILE_PATH, 'utf8') === "true") {
                console.log(`+${i}s`)
                break;
            }
        } catch (err) {
            console.error('Error reading file:', err);
            break;
        }
        await wait(INTERVAL_TIMEOUT);
        console.log(".")
        if (i === MAX_TIMEOUT) console.log("Max timeout reached");
    }
    console.log('...Ending wait');
    /**
     * Once this function ends, lighthouse will start generating the report
     */
}

const wait = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms))

module.exports = custom_wait