# Case Configuration - Status Workflow
STATUS_WORKFLOW_SUCCESSFULLY_CREATED_MESSAGE = "Workflow has been created."
STATUS_WORKFLOW_SUCCESSFULLY_DELETED_MESSAGE = "Workflow has been deleted."
STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE = "Workflow has been saved."
STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_ADDED_MESSAGE = "Status has been added."
STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_UPDATED_MESSAGE = "Status has been updated."
STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_DELETED_MESSAGE = '"{new_status_name}" has been deleted.'
STATUS_WORKFLOW_DETAILS_PAGE_HEADING_TEMPLATE = "Edit {status_workflow_name}"
STATUS_WORKFLOW_DETAILS_PAGE_HEADING_TEMPLATE_CLONED = "Edit {status_workflow_name} Copy"
STREAMS_CREATION_TOAST_MSG = (
    "The stream has been created successfully. You can view it on theStreams page"
)

# Case Configuration - Priority
NEW_PRIORITY_SUCCESSFULLY_ADDED_MESSAGE = "Priority has been added."
NEW_PRIORITY_SUCCESSFULLY_DELETED_MESSAGE = "Priority has been deleted."

# Case Configuration - Fields
NEW_FIELD_SUCCESSFULLY_ADDED_MESSAGE = '"{new_field_name}" has been added.'
NEW_FIELD_SUCCESSFULLY_DELETED_MESSAGE = '"{new_field_name}" has been deleted.'
CASE_PREFIX_SUCCESSFULLY_UPDATED_MESSAGE = "Case prefix has been updated."

# Case Configuration - Case Types
NEW_CASE_TYPE_SUCCESSFULLY_CREATED_MESSAGE = "Case type has been created."
NEW_CASE_TYPE_SUCCESSFULLY_UPDATED_MESSAGE = "Case type has been updated."
NEW_CASE_TYPE_SUCCESSFULLY_DELETED_MESSAGE = '"{case_type_name}" has been deleted.'
NEW_CASE_TYPE_TASK_SUCCESSFULLY_CREATED_MESSAGE = "Task template has been created."

PARAMETER_NODE_DELETE_ERROR_MSG = "An error occurred while deleting the node."
PARAMETER_NODE_DELETE_DESCRIPTION_MSG = " Error: Cannot delete node: `Parameter_Node`. Reason: Command cannot exist without parameter node! "

STAGE_COMMAND_SEARCH_API = "/case-management/api/content-management/content/command?libraryView=all"
STAGE_COMMAND_DELETE_API = "/case-management/api/content-management/delete"
