import os
import unittest
from ast import literal_eval

import flaky
from playwright.sync_api import sync_playwright, Playwright

from luna.driver.ui.wuibase import WUIBase
from luna.driver.ui import base
from src.base.base_config import BaseConfig
from src.page_objects.common.login import Login

HEADLESS = literal_eval(os.getenv("HEADLESS", "False"))


class BaseSetup(unittest.TestCase):
    browser = None
    context = None
    page = None
    playwright = None

    @classmethod
    def setUpClass(cls):
        cls.playwright = sync_playwright().start()
        BaseSetup.launch_browser(cls, playwright=cls.playwright)
        cls.page.goto(BaseConfig.get_base_url())
        login_page = Login(cls.page)
        credentials = {"username": BaseConfig.get_username(), "password": BaseConfig.get_password()}
        login_page.login(credentials, BaseConfig.get_domain_name())
        login_page.got_to_ueba_application(BaseConfig.get_deployment_name())

    @classmethod
    def tearDownClass(cls) -> None:
        cls.close(cls)
        cls.playwright.stop()

    @staticmethod
    def launch_browser(self, playwright: Playwright, devtools=False, slow_mo=None):
        playwright.chromium.launch()
        self.browser = playwright.chromium.launch(
            headless=HEADLESS, devtools=devtools, slow_mo=slow_mo
        )
        self.context = self.browser.new_context()
        self.page = self.context.new_page()
        self.page.set_viewport_size({"width": 1500, "height": 1000})

    @staticmethod
    def close(self):
        self.context.close()
        self.browser.close()


# @flaky(max_runs=3, min_passes=1)
class DevoUEBATestCase(WUIBase):
    """Base test case"""

    # @classmethod
    # def setUpClass(cls):
    #     super().setUpClass()
    # cls.page.goto(BaseConfig.get_base_url())

    def setUp(self):
        super().setUp()
        self.login_page = Login(self.page)
        self.login_page.login(
            username=BaseConfig.get_username(),
            password=BaseConfig.get_password(),
            domain=BaseConfig.get_domain_name(),
        )
        self.login_page.got_to_ueba_application(BaseConfig.get_deployment_name())

    def get_list_element_text_by_path(self, path):
        self.page.wait_for_selector(path)
        results = self.page.query_selector_all(path)
        list_element_text = []
        for result in results:
            list_element_text.append(result.inner_text())
        return list_element_text

    def await_for_element_presence_by_path(self, path):
        try:
            self.page.wait_for_selector(path)
        except Exception as e:
            print(e)

    def get_list_element_size_by_path(self, path):
        self.page.wait_for_selector(path)
        results = self.page.query_selector_all(path)
        return len(results)

    def await_for_element_presence(self, selector, time_out):
        try:
            self.page.wait_for_selector(selector, time_out)
            return True
        except Exception:
            return False

    def verify_notification_message(self, expected_message):
        assert self.notification_message.inner_text() == expected_message, (
            "Incorrect notification message is shown"
        )

    def wait_for_notification_message_to_be_shown(self):
        self.page.wait_for_selector(self.notification_message_locator, state="visible")

    def get_notification_message(self):
        return self.notification_message.inner_text()

    def verify_no_data_found_message(self):
        assert self.no_data_found_message.inner_text() == "No data found", (
            "Incorrect message is shown"
        )
