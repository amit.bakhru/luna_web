import os
from luna.config import config


class BaseConfig:
    @staticmethod
    def get_base_url():
        return config["servers"]["portal"]["url"]

    @staticmethod
    def get_domain_name():
        return config["servers"]["soar"]["domain"]

    @staticmethod
    def get_username():
        return config["servers"]["soar"]["username"]

    @staticmethod
    def get_password():
        return config["servers"]["soar"]["password"]

    @staticmethod
    def get_deployment_name():
        return config["servers"]["portal"]["deployment"]

    @staticmethod
    def get_use_case_api_key():
        return config["servers"]["portal"]["api_key"]

    @staticmethod
    def get_use_case_api_secret():
        return config["servers"]["portal"]["api_secret"]
