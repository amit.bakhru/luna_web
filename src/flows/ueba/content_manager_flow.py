from src.page_objects.ueba.content_manager import ContentManagerPage
from src.page_objects.ueba.entity_analysis import EntityAnalysisPage
from src.page_objects.ueba.overview import OverviewPage
import random
from src.base.base_config import BaseConfig


class ContentManagerFlow:
    def __init__(self, page):
        self.page = page
        self.content_manager_page = ContentManagerPage(self.page)
        self.entity_analysis_page = EntityAnalysisPage(self.page)
        self.overview_page = OverviewPage(self.page)

    def add_and_edit_verify_new_entity_group(self):
        self.content_manager_page.open_entity_group_page()
        risk_group_name = f"risk_group_{random.randint(1000, 9990)}"
        self.page.wait_for_timeout(5000)
        self.content_manager_page.add_new_entity_group(risk_group_name, "2")
        self.content_manager_page.verify_notification_message(
            f"The settings for Risk Group {risk_group_name} were saved successfully."
        )
        self.page.wait_for_timeout(5000)
        self.content_manager_page.verify_group_entity_details_on_listing_page(risk_group_name, "2")
        self.content_manager_page.edit_entity_group(risk_group_name, "3")
        self.content_manager_page.verify_notification_message(
            f"The settings for Risk Group {risk_group_name} were saved successfully."
        )
        self.content_manager_page.verify_group_entity_details_on_listing_page(risk_group_name, "3")
        return risk_group_name

    def delete_and_verify_entity_group(self, risk_group_name):
        self.content_manager_page.open_entity_group_page()
        self.content_manager_page.delete_entity_group(risk_group_name)
        self.content_manager_page.verify_notification_message(
            f'The Risk Group "{risk_group_name}" has been deleted.'
        )
        self.content_manager_page.search_entity_group_on_listing_page(risk_group_name)
        self.content_manager_page.verify_no_data_found_message()

    def add_and_verify_entity_to_group_from_listing_page(self):
        self.content_manager_page.open_entity_group_page()
        risk_group_name = f"risk_group_{random.randint(1000, 9990)}"
        self.content_manager_page.add_new_entity_group(risk_group_name, "2")
        self.content_manager_page.wait_for_notification_message_to_be_shown()
        self.entity_analysis_page.add_and_verify_entity_to_group_from_listing_page(risk_group_name)
        entity_name = self.entity_analysis_page.get_first_entity_name()
        self.content_manager_page.verify_entity_in_group_list(risk_group_name, entity_name)
        return risk_group_name

    def add_and_verify_entity_to_group_from_detail_page(self):
        self.content_manager_page.open_entity_group_page()
        risk_group_name = f"risk_group_{random.randint(1000, 9990)}"
        self.content_manager_page.add_new_entity_group(risk_group_name, "2")
        self.content_manager_page.wait_for_notification_message_to_be_shown()
        entity_name = self.entity_analysis_page.get_first_entity_name()
        self.entity_analysis_page.add_and_verify_entity_to_group_from_detail_page(risk_group_name)
        self.content_manager_page.verify_entity_in_group_list(risk_group_name, entity_name)
        return risk_group_name

    def edit_risk_score_multiplier_from_detail_page(self):
        self.content_manager_page.open_entity_group_page()
        risk_group_name = self.content_manager_page.get_first_entity_group_name()
        risk_score_multiplier = str(random.randint(2, 9))
        self.content_manager_page.edit_risk_score_multiplier_from_detail_page(
            risk_group_name, risk_score_multiplier
        )
        self.content_manager_page.verify_notification_message(
            f"The settings for Risk Group {risk_group_name} were saved successfully."
        )
        self.page.wait_for_timeout(2000)
        assert (
            self.content_manager_page.get_risk_score_multiplier_on_detail_page()
            == f"Risk Score Multiplier: {risk_score_multiplier}"
        ), "Incorrect risk score multiplier is shown"

    def add_delete_and_verify_entity_to_risk_group_from_detail_page(self):
        self.content_manager_page.open_entity_group_page()
        risk_group_name = f"risk_group_{random.randint(1000, 9990)}"
        self.content_manager_page.add_new_entity_group(risk_group_name, "2")
        self.content_manager_page.wait_for_notification_message_to_be_shown()
        entity_name = self.entity_analysis_page.get_first_entity_name()
        self.content_manager_page.add_entity_in_risk_group_on_detail_page(
            risk_group_name, entity_name
        )
        assert self.content_manager_page.get_entity_in_group_detail_page() == entity_name, (
            "Entity Name is not added"
        )
        self.entity_analysis_page.verify_entity_to_group_from_listing_page(
            risk_group_name, entity_name
        )
        self.content_manager_page.delete_entity_in_risk_group_on_detail_page()
        self.entity_analysis_page.verify_entity_to_group_from_listing_page("(none)", entity_name)
        return risk_group_name

    def add_and_verify_entity_to_notable_from_listing_page(self):
        self.page.wait_for_timeout(2000)
        entity_name = self.entity_analysis_page.get_entity_name_by_index(random.randint(1, 10))
        self.entity_analysis_page.add_entity_to_notable_from_listing_page(entity_name)
        self.content_manager_page.verify_entity_in_notables_list(entity_name)
        self.overview_page.verify_entity_in_notables_list(entity_name)

    def add_and_verify_entity_to_notable_from_detail_page(self):
        # self.page.pause()
        entity_name = self.entity_analysis_page.get_entity_name_by_index(random.randint(9, 16))
        print(entity_name)
        self.entity_analysis_page.add_entity_to_notable_from_detail_page(entity_name)
        self.content_manager_page.verify_entity_in_notables_list(entity_name)
        self.overview_page.verify_entity_in_notables_list(entity_name)

    def add_delete_and_verify_entity_from_notable_detail_page(self):
        self.page.wait_for_timeout(2000)
        entity_name = self.entity_analysis_page.get_entity_name_by_index(random.randint(11, 20))
        self.content_manager_page.add_entity_in_notable_detail_page(entity_name)
        self.entity_analysis_page.verify_entity_to_notable_from_listing_page(
            entity_name, "Marked as Notable"
        )
        self.overview_page.verify_entity_in_notables_list(entity_name)
        self.content_manager_page.delete_entity_in_notable_detail_page(entity_name)
        self.entity_analysis_page.verify_entity_to_notable_from_listing_page(
            entity_name, "Click to add to Notables"
        )
        self.overview_page.verify_entity_not_in_notables_list(entity_name)

    def add_and_verify_risk_threshold_based_alert(self):
        risk_alert_name = f"_risk_alert_{random.randint(1000, 9990)}"
        self.content_manager_page.add_risk_threshold_based_alert(
            risk_alert_name, "70", "10", "11", "12"
        )
        self.content_manager_page.verify_notification_message(
            f'Alert "SecOpsRisk{risk_alert_name}" was created successfully.'
        )
        self.content_manager_page.verify_risk_based_alerts_details_on_listing_page(
            risk_alert_name, "75"
        )
        return risk_alert_name

    def add_and_verify_risk_rate_of_change_based_alert(self):
        risk_alert_name = f"_risk_alert_{random.randint(1000, 9990)}"
        self.content_manager_page.add_risk_rate_of_change_based_alert(
            risk_alert_name, "5", "3", "10", "11", "12"
        )
        self.content_manager_page.verify_notification_message(
            f'Alert "SecOpsRisk{risk_alert_name}" was created successfully.'
        )
        self.content_manager_page.verify_rate_of_change_risk_based_alerts_details_on_listing_page(
            risk_alert_name, "5"
        )
        self.content_manager_page.edit_risk_score_change(risk_alert_name, "10")
        self.content_manager_page.verify_notification_message(
            f'Alert "SecOpsRisk{risk_alert_name}" was updated successfully.'
        )
        self.content_manager_page.verify_rate_of_change_risk_based_alerts_details_on_listing_page(
            risk_alert_name, "10"
        )
        return risk_alert_name

    def edit_and_remove_verify_secops_alerts_risk_score(self):
        risk_score = str(random.randint(2, 9))
        secops_alert_name = self.content_manager_page.get_first_secops_alerts_name()
        self.content_manager_page.edit_secops_alerts_risk_score(risk_score, secops_alert_name)
        assert (
            self.content_manager_page.get_notification_message()
            == f"Risk score for {secops_alert_name} was updated successfully."
        ), "Incorrect notification message is shown"
        self.content_manager_page.verify_secops_alert_risk_score_on_listing_page(
            secops_alert_name, risk_score
        )
        self.content_manager_page.remove_secops_alerts_risk_score(secops_alert_name)
        assert (
            self.content_manager_page.get_notification_message()
            == f"The risk score was successfully removed from alert {secops_alert_name}."
        ), "Incorrect notification message is shown"
        self.content_manager_page.verify_secops_alert_risk_score_on_listing_page(
            secops_alert_name, "0"
        )

    def delete_risk_based_alerts(self, risk_alert_name):
        self.content_manager_page.delete_risk_based_alerts(risk_alert_name)
        self.content_manager_page.verify_notification_message(
            f"The alert SecOpsRisk{risk_alert_name} was deleted."
        )
        self.content_manager_page.search_risk_based_alerts_on_listing_page(risk_alert_name)
        self.content_manager_page.verify_no_data_found_message()

    def configure_and_disable_use_case(self):
        self.content_manager_page.go_to_disabled_use_case_listing_page()
        use_case_name = self.content_manager_page.get_first_usecase_name()
        self.content_manager_page.configure_use_case_V2(use_case_name)
        # assert self.content_manager_page.get_notification_message() == f"Configuration {use_case_name} was updated successfully.", "Incorrect notification message is shown"
        self.content_manager_page.clear_use_case_status_on_listing_page()
        self.content_manager_page.verify_use_case_status_on_listing_page(use_case_name, "Started")
        self.content_manager_page.wait_for_usecase_to_run()
        self.content_manager_page.disable_use_case(use_case_name)
        self.content_manager_page.verify_use_case_status_on_listing_page(use_case_name, "Stopping")

    def configure_and_pause_use_case(self):
        self.content_manager_page.go_to_disabled_use_case_listing_page()
        use_case_name = self.content_manager_page.get_first_usecase_name()
        self.content_manager_page.configure_use_case_V2(use_case_name)
        self.content_manager_page.clear_use_case_status_on_listing_page()
        self.content_manager_page.wait_for_usecase_to_run()
        self.content_manager_page.pause_use_case(use_case_name)
        self.content_manager_page.verify_use_case_status_on_listing_page(use_case_name, "Pausing")

    def resume_a_pause_use_case(self):
        self.content_manager_page.go_to_pause_use_case_listing_page()
        use_case_name = self.content_manager_page.get_first_usecase_name()
        self.content_manager_page.resume_use_case(use_case_name)
        self.content_manager_page.clear_use_case_status_on_listing_page()
        self.content_manager_page.verify_use_case_status_on_listing_page(use_case_name, "Started")

    def upload_and_download_csv_on_notable_entities(self):
        download_filepath = "src/files/download.csv"
        self.content_manager_page.upload_and_download_notable_entities(download_filepath)
