import time
from src.page_objects.ueba.entity_analysis import EntityAnalysisPage
from src.page_objects.ueba.overview import OverviewPage


class OverviewFlow:
    def __init__(self, page):
        self.page = page
        self.overview_page = OverviewPage(page=self.page)
        self.entity_analysis_page = EntityAnalysisPage(page=self.page)

    def verify1(self):
        self.overview_page.verify_1()

    def verify_total_entities_tracked_last_7_days(self):
        self.overview_page.go_to_entities_by_index(0)
        self.entity_analysis_page.verify_total_entities_tracked_last_7_days_filters()

    def verify_critical_entities_tracked_last_7_days(self):
        critical_risk_entities_count = self.overview_page.get_critical_risk_entities_count()
        if critical_risk_entities_count != "0":
            self.overview_page.go_to_entities_by_index(1)
            self.entity_analysis_page.verify_critical_entities_filters()

    def verify_high_entities_tracked_last_7_days(self):
        high_risk_entities_count = self.overview_page.get_high_risk_entities_count()
        if high_risk_entities_count != "0":
            self.overview_page.go_to_entities_by_index(2)
            self.entity_analysis_page.verify_high_entities_filters()

    def verify_medium_entities_tracked_last_7_days(self):
        medium_risk_entities_count = self.overview_page.get_medium_risk_entities_count()
        if medium_risk_entities_count != "0":
            self.overview_page.go_to_entities_by_index(3)
            self.entity_analysis_page.verify_medium_entities_filters()

    def verify_total_entities_tracked_last_24_hours(self):
        self.overview_page.go_to_entities_by_index(4)
        self.entity_analysis_page.verify_total_entities_tracked_last_24_hours_filters()

    def verify_notable_entities(self):
        self.overview_page.got_to_notable_entities()
        self.entity_analysis_page.verify_notable_entities_filters()

    def verify_top_users_filters_last_7_days(self):
        self.overview_page.got_to_top_users()
        self.entity_analysis_page.verify_top_users_filters()

    def verify_top_devices_last_7_days(self):
        self.overview_page.got_to_top_devices()
        self.entity_analysis_page.verify_top_devices_filters()

    def verify_top_domains_last_7_days(self):
        self.overview_page.got_to_top_domains()
        self.entity_analysis_page.verify_top_domains_filters()

    def verify_top_unique_alert_last_7_days(self):
        self.overview_page.got_to_top_unique_alerts()
        self.entity_analysis_page.verify_top_unique_alert_filters()

    def verify_top_unique_tactics_last_7_days(self):
        self.overview_page.got_to_top_tactics()
        self.entity_analysis_page.verify_top_unique_tactics_filters()

    def verify_top_unique_techniques_last_7_days(self):
        self.overview_page.go_to_top_techniques()
        self.entity_analysis_page.verify_top_unique_techniques_filters()

    def verify_navigation_and_filters_of_all_entities(self):
        time.sleep(15)
        self.verify_total_entities_tracked_last_7_days()
        self.verify_medium_entities_tracked_last_7_days()
        self.verify_critical_entities_tracked_last_7_days()
        self.verify_medium_entities_tracked_last_7_days()
        self.verify_total_entities_tracked_last_24_hours()
        self.verify_notable_entities()
        self.verify_top_users_filters_last_7_days()
        self.verify_top_devices_last_7_days()
        self.verify_top_domains_last_7_days()
        self.verify_top_unique_alert_last_7_days()
        self.verify_top_unique_tactics_last_7_days()
        self.verify_top_unique_techniques_last_7_days()

    def open_and_verify_entity_from_overview_page(self):
        entity_name = self.overview_page.get_top_users_first_entity_name()
        risk_score = self.overview_page.get_top_users_first_entity_risk_score()
        self.overview_page.go_to_top_users_first_entity_name()
        self.entity_analysis_page.verify_details_on_entities_detail_page(entity_name, risk_score)
