## Luna Web Automation Framework

### Setup
- Install `make` and `pyenv` and install the required python version
```bash
brew install cmake uv
make venv
```

### Run
 - Run the tests with the following command
```bash
pytest -p no:logging -sv tests/test_login.py
```

### Test Execution with Runner
- Run the tests with the following command
```bash
python bin/runner.py --filepath="tests/soar/test_performance.py" --n=3 --tags="smoke"
```
- For more information about the run configurations, refer to the `runner.py` file

### Performance Testing
- Install NodeJs and then install `lighthouse` and `@lhci/cli` packages
```bash
npm i lighthouse @lhci/cli
```
- Run the performance test with the following command
```bash
python bin/performance_runner.py
```
- For more information about the run configurations, refer to the `performance_runner.py` file


### References:
- https://github.com/GoogleChrome/lighthouse