import pytest


def pytest_addoption(parser):
    parser.addoption("--ws", action="store", default=None, help="Custom flag for CDP session")


@pytest.fixture
def ws_flag(request):
    return request.config.getoption("--ws")
