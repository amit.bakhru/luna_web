from luna.driver.ui.wuibase import WUIBase
from src.base.base_config import BaseConfig
from src.page_objects.common.login import Login
from src.page_objects.soar.case_management.case_listing import CaseListing


class PerformanceTest(WUIBase):
    def setUp(self):
        super().setUp()

    def test_login(self):
        self.login_page = Login(page=self.page)
        self.login_page.login(
            username=BaseConfig.get_username(), password=BaseConfig.get_password()
        )
        self.case_listing = CaseListing(page=self.page)
        print("Login completed")

    def tearDown(self):
        super().tearDown()
