import pytest
from src.utils.helper import Helper
from luna.driver.ui.wuibase import WUIBase
from src.base.base_config import BaseConfig
from src.page_objects.common.login import Login
from src.page_objects.soar.case_management.case_listing import CaseListing


class CaseListingTestCase(WUIBase):
    def setUp(self):
        super().setUp()
        self.login_page = Login(page=self.page)
        self.login_page.login(
            username=BaseConfig.get_username(), password=BaseConfig.get_password()
        )
        self.case_listing = CaseListing(page=self.page)

    @pytest.mark.smoke
    def test_create_and_delete_case_on_case_listing_page(self):
        """
        Create and delete a case on case listing page
        """
        case_title = Helper.random_string_generator()
        self.case_listing.create_a_case(case_title)
        self.case_listing.click_case_details_back_btn()
        self.case_listing.delete_a_case(case_title)
        assert f"Successfully deleted {case_title}." == self.case_listing.get_toast_msg(), (
            "Assertion failed: Creation and Deletion of a case on case listing page."
        )

    def test_delete_multiple_cases_on_case_listing_page(self):
        """
        Creates and deletes multiple cases on case listing page
        """
        case_titles = self.case_listing.create_multiple_cases(2)
        self.case_listing.delete_multiple_cases_on_case_listing_page(case_titles)
        assert f"Successfully deleted 2 cases." == self.case_listing.get_toast_msg(), (
            "Assertion failed: Creation and Deletion of multiple cases on case listing page."
        )

    def test_download_on_case_listing_page(self):
        """
        Download file as csv on case listing page
        """
        download_filepath = "src/files/download.csv"
        assert True == self.case_listing.download_and_validate_csv_on_case_listing_page(
            download_filepath
        ), "Assertion failed: Download CSV file."

    @pytest.mark.skip
    def test_filters_on_case_listing_page(self):
        case_titles = []
        priority_filters = ["Very low", "Very high", "Normal"]

        def create_test_data():
            nonlocal case_titles
            case_titles = self.case_listing.create_multiple_cases(
                n=3,
                title=[
                    Helper.random_string_generator(),
                    Helper.random_string_generator(),
                    Helper.random_string_generator(),
                ],
                priority=priority_filters,
            )

        def delete_test_data():
            self.case_listing.delete_multiple_cases_on_case_listing_page(case_titles)

        create_test_data()
        try:
            # To check if filtering is correct
            self.case_listing.wait_until_case_loading_is_complete()
            for i in range(0, len(priority_filters)):
                self.case_listing.select_a_filter(
                    "Priority", priority_filters[i], click=False if i > 0 else True
                )
                self.case_listing.click_filter_button()
                self.case_listing.wait_until_case_loading_is_complete()
                assert case_titles[i] == self.case_listing.get_first_case_attribute()
                self.case_listing.click_filter_button()
                self.case_listing.clear_all_filters()
            # To check other filters
            self.case_listing.select_a_filter("Status", "New", False)
            assert case_titles[2] == self.case_listing.get_first_case_attribute()
            self.case_listing.clear_all_filters()
            self.case_listing.select_a_filter("Types", "Default", False)
            assert case_titles[2] == self.case_listing.get_first_case_attribute()
            self.case_listing.clear_all_filters()
            for _ in range(2):
                self.case_listing.click_filter_button()
            self.case_listing.click_add_more_filters_btn()
            assert self.case_listing.is_add_more_filter_container_visible() == True
        finally:
            delete_test_data()

    def test_user_dropdown_card_on_case_listing_page(self):
        def create_test_data(case_title):
            self.case_listing.create_a_case(case_title)

        def delete_test_data(case_title):
            self.case_listing.delete_a_case(case_title)

        case_title = Helper.random_string_generator()
        create_test_data(case_title)
        try:
            self.case_listing.click_case_details_back_btn()
            self.case_listing.wait_until_case_loading_is_complete()
            self.case_listing.populate_search_box(case_title)
            self.card_username = self.case_listing.get_user_card_on_case_listing_page()
            assert BaseConfig.get_username() == self.card_username, (
                "Assertion failed: Card Username"
            )
        finally:
            delete_test_data(case_title)

    def test_save_preset_functionality_on_case_listing_page(self):
        preset_title = Helper.random_string_generator()
        self.case_listing.wait_until_case_loading_is_complete()
        self.case_listing.click_preset_reset_button()
        self.case_listing.select_a_filter("Priority", "High")
        self.case_listing.save_preset(preset_title, f"desc {preset_title}")
        self.case_listing.preset_toast_message_close_button()
        try:
            assert self.case_listing.validate_saved_preset(preset_title) == True
        finally:
            self.case_listing.delete_saved_preset(preset_title)

    def test_user_can_switch_between_multiple_saved_presets(self):
        preset_title_1 = Helper.random_string_generator()
        preset_title_2 = Helper.random_string_generator()

        # Creating Test Data
        def create_test_data(filter_key, filter_value, title, desc, click=True):
            self.case_listing.wait_until_case_loading_is_complete()
            self.case_listing.click_preset_reset_button()
            self.case_listing.select_a_filter(filter_key, filter_value, click)
            self.case_listing.save_preset(title, desc)
            self.case_listing.preset_toast_message_close_button()

        # Deleting Test Data
        def delete_test_data():
            self.case_listing.delete_saved_preset(preset_title_1)
            self.case_listing.delete_saved_preset(preset_title_2)

        # Setting up the test data
        create_test_data("Priority", "Low", preset_title_1, f"desc {preset_title_1}")
        create_test_data("Status", "Done", preset_title_2, f"desc {preset_title_2}", False)
        try:
            self.case_listing.click_filter_button()
            self.case_listing.select_a_preset_from_saved_presets(preset_title_1)
            tags = self.case_listing.get_list_of_selected_tags_of_a_saved_preset()
            assert tags == ["Priority: Low"]
            self.case_listing.select_a_preset_from_saved_presets(preset_title_2)
            tags = self.case_listing.get_list_of_selected_tags_of_a_saved_preset()
            assert tags == ["Status: Done"]
        finally:
            delete_test_data()

    def test_search_box(self):
        def create_test_data(case_title):
            self.case_listing.create_a_case(case_title)

        def delete_test_data(case_title):
            self.case_listing.delete_a_case(case_title)

        case_title = Helper.random_string_generator()
        create_test_data(case_title)
        try:
            self.case_listing.click_case_details_back_btn()
            self.case_listing.wait_until_case_loading_is_complete()
            self.case_listing.populate_search_box(case_title)
            self.case_listing.wait_for_search_results_to_be_displayed()
            assert case_title == self.case_listing.get_first_case_attribute()
        finally:
            delete_test_data(case_title)

    def tearDown(self):
        super().tearDown()
