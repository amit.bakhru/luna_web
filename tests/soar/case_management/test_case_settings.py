from src.utils.helper import Helper
from luna.driver.ui.wuibase import WUIBase
from src.base.base_config import BaseConfig
from src.page_objects.common.login import Login
from src.page_objects.soar.case_management.case_listing import CaseListing
from src.page_objects.soar.case_management.case_details import CaseDetails
from src.page_objects.soar.case_management.case_settings import CaseSettings

from src.constants import (
    STATUS_WORKFLOW_SUCCESSFULLY_CREATED_MESSAGE,
    STATUS_WORKFLOW_SUCCESSFULLY_DELETED_MESSAGE,
    STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE,
    STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_ADDED_MESSAGE,
    STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_UPDATED_MESSAGE,
    STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_DELETED_MESSAGE,
    STATUS_WORKFLOW_DETAILS_PAGE_HEADING_TEMPLATE,
    STATUS_WORKFLOW_DETAILS_PAGE_HEADING_TEMPLATE_CLONED,
    NEW_PRIORITY_SUCCESSFULLY_ADDED_MESSAGE,
    NEW_PRIORITY_SUCCESSFULLY_DELETED_MESSAGE,
    NEW_FIELD_SUCCESSFULLY_ADDED_MESSAGE,
    NEW_FIELD_SUCCESSFULLY_DELETED_MESSAGE,
    CASE_PREFIX_SUCCESSFULLY_UPDATED_MESSAGE,
    NEW_CASE_TYPE_SUCCESSFULLY_CREATED_MESSAGE,
    NEW_CASE_TYPE_SUCCESSFULLY_UPDATED_MESSAGE,
    NEW_CASE_TYPE_SUCCESSFULLY_DELETED_MESSAGE,
)


class CaseSettingsTestCase(WUIBase):
    def setUp(self):
        super().setUp()
        self.login_page = Login(page=self.page)
        self.login_page.login(
            username=BaseConfig.get_username(), password=BaseConfig.get_password()
        )
        self.case_settings = CaseSettings(page=self.page)
        self.case_listing = CaseListing(page=self.page, launch=False)
        self.case_details = CaseDetails(page=self.page, launch=False)

    def test_create_verify_and_delete_case_types(self):
        """Tests case type, by adding new field, status workflow, and task and verifies everything on case details"""
        # Create a new status workflow
        self.case_settings.navigate_to_status_workflow_tab()
        self.case_settings.create_new_status_workflow()
        assert (
            STATUS_WORKFLOW_SUCCESSFULLY_CREATED_MESSAGE == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        _status_workflow_name = self.case_settings.get_status_workflow_name()
        self.case_settings.click_status_workflow_save_btn()
        self.case_settings.wait_until_status_workflow_loading_is_complete()
        assert STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()

        # Create a new field
        self.case_settings.navigate_to_fields_tab()
        _field_name = Helper.random_string_generator()
        self.case_settings.create_new_field(_field_name)
        assert (
            NEW_FIELD_SUCCESSFULLY_ADDED_MESSAGE.format(new_field_name=_field_name)
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()

        # Create case type
        self.case_settings.navigate_to_case_types_tab()
        _case_type_name = Helper.random_string_generator()
        _case_type_desc = Helper.random_string_generator()
        self.case_settings.create_case_type(_case_type_name, _case_type_desc)
        assert NEW_CASE_TYPE_SUCCESSFULLY_CREATED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()

        # Edit case type
        _task_name = Helper.random_string_generator()
        self.case_settings.edit_case_type(
            name=_case_type_name,
            nth_child=7,
            case_details=self.case_details,
            field=_field_name,
            status_workflow=_status_workflow_name,
            task=_task_name,
        )
        assert NEW_CASE_TYPE_SUCCESSFULLY_UPDATED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()

        # Verify in case details page
        self.case_settings.navigate_to_case_listing_page()
        self.case_listing.wait_until_case_loading_is_complete()
        _case_name = Helper.random_string_generator()
        self.case_listing.create_a_case(title=_case_name, case_type=_case_type_name)
        assert _case_type_name == self.case_details.get_case_type()
        assert "ToDo" == self.case_details.get_status_on_case_details()
        assert BaseConfig.get_username() == self.case_details.get_assigned_to_on_case_details()
        assert True == self.case_details.verify_task_names(_task_name)
        self.case_listing.navigate_to_additional_fields_tab()
        self.case_listing.click_show_empty_fields_btn()
        assert True == self.case_details.verify_newly_created_field(_field_name)

        # Delete case type
        self.case_listing.click_case_details_back_btn()
        self.case_listing.wait_until_case_loading_is_complete()
        self.case_listing.delete_a_case(_case_name)
        self.case_settings.close_toast_message()
        self.case_settings.navigate_to_case_configuration_page()
        self.case_settings.navigate_to_fields_tab()
        self.case_settings.delete_field(_field_name)
        assert (
            NEW_FIELD_SUCCESSFULLY_DELETED_MESSAGE.format(new_field_name=_field_name)
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        self.case_settings.navigate_to_case_types_tab()
        self.case_settings.delete_case_type(_case_type_name)
        assert (
            NEW_CASE_TYPE_SUCCESSFULLY_DELETED_MESSAGE.format(case_type_name=_case_type_name)
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        self.case_settings.navigate_to_status_workflow_tab(wait=False)
        self.case_settings.delete_status_workflow(_status_workflow_name)
        assert (
            STATUS_WORKFLOW_SUCCESSFULLY_DELETED_MESSAGE == self.case_settings.get_toast_msg_text()
        )

    def test_case_id_prefix(self):
        """Tests case id prefix"""

        def update_case_id(id):
            self.case_settings.change_case_id(id)
            assert (
                CASE_PREFIX_SUCCESSFULLY_UPDATED_MESSAGE == self.case_settings.get_toast_msg_text()
            )
            self.case_settings.close_toast_message()
            self.case_settings.navigate_to_case_listing_page()
            self.case_listing.wait_until_case_loading_is_complete()
            _name = Helper.random_string_generator()
            self.case_listing.create_a_case(_name)
            assert id.upper() == self.case_listing.get_case_id_on_case_details_page().upper()
            self.case_listing.click_case_details_back_btn()
            self.case_listing.wait_until_case_loading_is_complete()
            assert id.upper() == self.case_listing.get_case_id_on_case_listing_page()
            self.case_listing.delete_a_case(_name)
            self.case_settings.close_toast_message()

        # Test by using a random id
        update_case_id(Helper.random_string_generator())
        self.case_settings.navigate_to_case_configuration_page()
        # Revert back to the original id
        update_case_id("case")

    def test_verify_field_types_during_edit_field(self):
        """Tests valid field types during edit field flow"""

        def _create_new_field(name, type):
            self.case_settings.create_new_field(name, type)
            assert (
                NEW_FIELD_SUCCESSFULLY_ADDED_MESSAGE.format(new_field_name=name)
                == self.case_settings.get_toast_msg_text()
            )
            self.case_settings.close_toast_message()

        def _delete_field(names):
            for name in names:
                self.case_settings.delete_field(name)
                assert (
                    NEW_FIELD_SUCCESSFULLY_DELETED_MESSAGE.format(new_field_name=name)
                    == self.case_settings.get_toast_msg_text()
                )
                self.case_settings.close_toast_message()

        self.case_settings.navigate_to_fields_tab()
        _names = [Helper.random_string_generator(), Helper.random_string_generator()]
        _create_new_field(_names[0], "Text")
        _create_new_field(_names[1], "DateTime")
        self.case_settings.verify_edit_field_types(_names[0])
        self.case_settings.click_new_field_save_btn()
        self.case_settings.verify_edit_field_types(_names[1])
        self.case_settings.click_new_field_save_btn()
        _delete_field(_names)

    def test_hide_system_fields_toggle(self):
        """Tests hide system fields toggle button"""
        self.case_settings.navigate_to_fields_tab()
        self.case_settings.click_hide_system_fields_toggle()
        assert True == self.case_settings.verify_hide_system_fields_toggle()

    def test_field_search_box(self):
        """Tests field search box"""
        self.case_settings.navigate_to_fields_tab()
        _name = Helper.random_string_generator()
        self.case_settings.create_new_field(_name)
        assert (
            NEW_FIELD_SUCCESSFULLY_ADDED_MESSAGE.format(new_field_name=_name)
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        self.case_settings.fill_field_search_box(_name)
        self.case_settings.wait_for_field_search_results_to_be_displayed()
        assert _name == self.case_settings.get_field_name()
        self.case_settings.delete_field(_name)
        assert (
            NEW_FIELD_SUCCESSFULLY_DELETED_MESSAGE.format(new_field_name=_name)
            == self.case_settings.get_toast_msg_text()
        )

    def test_create_delete_field(self):
        """Tests create and delete field"""
        # Create Field
        self.case_settings.navigate_to_fields_tab()
        _name = Helper.random_string_generator()
        self.case_settings.create_new_field(_name)
        assert (
            NEW_FIELD_SUCCESSFULLY_ADDED_MESSAGE.format(new_field_name=_name)
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()

        # Verify newly create field
        self.case_settings.navigate_to_case_listing_page()
        self.case_listing.wait_until_case_loading_is_complete()
        _case_title = Helper.random_string_generator()
        self.case_listing.create_a_case(title=_case_title)
        self.case_listing.navigate_to_additional_fields_tab()
        self.case_listing.click_show_empty_fields_btn()
        assert True == self.case_details.verify_newly_created_field(_name)
        self.case_listing.click_case_details_back_btn()
        self.case_listing.wait_until_case_loading_is_complete()
        self.case_listing.delete_a_case(_case_title)
        self.case_settings.close_toast_message()

        # Delete Field
        self.case_settings.navigate_to_case_configuration_page()
        self.case_settings.navigate_to_fields_tab()
        self.case_settings.delete_field(_name)
        assert (
            NEW_FIELD_SUCCESSFULLY_DELETED_MESSAGE.format(new_field_name=_name)
            == self.case_settings.get_toast_msg_text()
        )

    def test_priority(self):
        """Tests create and delete operations on priority"""
        # Create new priority
        self.case_settings.navigate_to_priority_tab()
        self.case_settings.wait_until_priority_loading_is_complete()
        _name = Helper.random_string_generator()
        self.case_settings.create_new_priority(_name)
        assert NEW_PRIORITY_SUCCESSFULLY_ADDED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()

        # Verify newly created priority
        self.case_settings.navigate_to_case_listing_page()
        self.case_listing.wait_until_case_loading_is_complete()
        _case_title = Helper.random_string_generator()
        self.case_listing.create_a_case(title=_case_title, priority=_name)
        self.case_listing.click_case_details_back_btn()
        self.case_listing.wait_until_case_loading_is_complete()
        self.case_listing.delete_a_case(_case_title)
        self.case_settings.close_toast_message()

        # Delete priority
        self.case_settings.navigate_to_case_configuration_page()
        self.case_settings.navigate_to_priority_tab()
        self.case_settings.delete_priority(_name)
        assert NEW_PRIORITY_SUCCESSFULLY_DELETED_MESSAGE == self.case_settings.get_toast_msg_text()

    def test_status_workflow(self):
        """Tests create, edit, clone and delete operations on status workflow and new statuses"""
        # Create status workflow
        self.case_settings.navigate_to_status_workflow_tab()
        self.case_settings.create_new_status_workflow()
        assert (
            STATUS_WORKFLOW_SUCCESSFULLY_CREATED_MESSAGE == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        status_workflow_name = self.case_settings.get_status_workflow_name()
        self.case_settings.click_status_workflow_save_btn()
        self.case_settings.wait_until_status_workflow_loading_is_complete()
        assert STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()

        # Edit status workflow
        _name = Helper.random_string_generator()
        _description = Helper.random_string_generator()
        _new_status_type = "In Progress"
        _new_status_name = Helper.random_string_generator()
        self.case_settings.navigate_to_status_workflow_details_page(status_workflow_name)
        self.case_settings.edit_status_workflow(_name, _description)
        self.case_settings.click_status_workflow_save_btn()
        assert STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()
        self.case_settings.wait_until_status_workflow_loading_is_complete()
        self.case_settings.navigate_to_status_workflow_details_page(_name)
        assert _name == self.case_settings.get_status_workflow_name()
        assert _description == self.case_settings.get_status_workflow_description()
        assert (
            STATUS_WORKFLOW_DETAILS_PAGE_HEADING_TEMPLATE.format(status_workflow_name=_name)
            == self.case_settings.get_status_workflow_heading()
        )

        # Add new status
        self.case_settings.add_new_status(_new_status_name, _new_status_type)
        assert (
            STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_ADDED_MESSAGE
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        assert _new_status_name == self.case_settings.get_new_status_display_name()

        # Edit new status
        _new_status_name_edited = Helper.random_string_generator()
        self.case_settings.edit_new_status(_new_status_name, _new_status_name_edited)
        assert (
            STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_UPDATED_MESSAGE
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        assert _new_status_name_edited == self.case_settings.get_new_status_display_name()

        # Clone new status
        _new_status_name_cloned = Helper.random_string_generator()
        self.case_settings.clone_new_status(_new_status_name_edited, _new_status_name_cloned)
        assert (
            STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_ADDED_MESSAGE
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        assert _new_status_name_cloned == self.case_settings.get_new_status_display_name(
            nth_child=4
        )

        # Delete new status
        self.case_settings.delete_new_status(_new_status_name_cloned)
        assert (
            STATUS_WORKFLOW_NEW_STATUS_SUCCESSFULLY_DELETED_MESSAGE.format(
                new_status_name=_new_status_name_cloned
            )
            == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        self.case_settings.click_status_workflow_save_btn()
        assert STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()
        self.case_settings.wait_until_status_workflow_loading_is_complete()

        # Clone status workflow
        self.case_settings.clone_status_workflow(_name)
        self.case_settings.wait_until_status_workflow_details_page_heading_is_loaded()
        assert (
            STATUS_WORKFLOW_DETAILS_PAGE_HEADING_TEMPLATE_CLONED.format(status_workflow_name=_name)
            == self.case_settings.get_status_workflow_heading()
        )
        _cloned_name = self.case_settings.get_status_workflow_name()
        self.case_settings.click_status_workflow_save_btn()
        assert STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()
        self.case_settings.wait_until_status_workflow_loading_is_complete()

        # Delete status workflow
        self.case_settings.delete_status_workflow(_name)
        assert (
            STATUS_WORKFLOW_SUCCESSFULLY_DELETED_MESSAGE == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        self.case_settings.delete_status_workflow(_cloned_name)
        assert (
            STATUS_WORKFLOW_SUCCESSFULLY_DELETED_MESSAGE == self.case_settings.get_toast_msg_text()
        )

    def test_status_workflow_search_box(self):
        """Tests status workflow search box"""
        self.case_settings.navigate_to_status_workflow_tab()
        self.case_settings.create_new_status_workflow()
        assert (
            STATUS_WORKFLOW_SUCCESSFULLY_CREATED_MESSAGE == self.case_settings.get_toast_msg_text()
        )
        self.case_settings.close_toast_message()
        _name = self.case_settings.get_status_workflow_name()
        self.case_settings.click_status_workflow_save_btn()
        assert STATUS_WORKFLOW_SUCCESSFULLY_SAVED_MESSAGE == self.case_settings.get_toast_msg_text()
        self.case_settings.close_toast_message()
        self.case_settings.wait_until_status_workflow_loading_is_complete()
        self.case_settings.fill_status_workflow_search_box(_name)
        self.case_settings.wait_for_status_workflow_table_to_update()
        assert _name == self.case_settings.get_status_workflow_row_display_name()
        self.case_settings.delete_status_workflow(_name)
        assert (
            STATUS_WORKFLOW_SUCCESSFULLY_DELETED_MESSAGE == self.case_settings.get_toast_msg_text()
        )

    def tearDown(self):
        super().tearDown()
