from src.utils.helper import Helper
from luna.driver.ui.wuibase import WUIBase
from src.base.base_config import BaseConfig
from src.page_objects.common.login import Login
from src.page_objects.soar.case_management.case_details import CaseDetails
from src.page_objects.soar.case_management.case_listing import CaseListing
from src.page_objects.soar.case_management.case_settings import CaseSettings


class CaseDetailsTestCase(WUIBase):
    def setUp(self):
        super().setUp()
        self.login_page = Login(page=self.page)
        self.login_page.login(
            username=BaseConfig.get_username(), password=BaseConfig.get_password()
        )
        self.case_details = CaseDetails(page=self.page)
        self.case_listing = CaseListing(page=self.page, launch=False)
        self.case_settings = CaseSettings(page=self.page, launch=False)

    def test_update_title_priority_status_assignee_on_case_details(self):
        """Updates title, priority, status, assignee on case details page"""
        case_title = Helper.random_string_generator()
        updated_case_title = Helper.random_string_generator()
        updated_priority = "High"
        updated_status = "In Progress"
        updated_assigned_to_email = BaseConfig.get_username()

        self.case_listing.create_a_case(title=case_title)

        self.case_details.update_title_case_details(updated_case_title)
        self.case_details.update_priority_case_details(updated_priority)
        self.case_details.update_status_case_details(updated_status)
        self.case_details.update_assigned_to_case_details(updated_assigned_to_email)

        assert self.case_details.get_title_on_case_details() == updated_case_title
        assert self.case_details.get_priority_on_case_details() == updated_priority
        assert self.case_details.get_status_on_case_details() == updated_status
        assert self.case_details.get_assigned_to_on_case_details() == updated_assigned_to_email

        self.case_listing.click_case_details_back_btn()
        self.case_listing.wait_until_case_loading_is_complete()
        self.case_listing.delete_a_case(case_title=updated_case_title)

    def test_add_integration_task(self):
        case_title = Helper.random_string_generator()
        self.case_listing.create_a_case(case_title)
        self.case_details.add_integration_task(
            name=Helper.random_string_generator(), data={"ip": "192.168.0.1"}
        )
        self.case_listing.click_case_details_back_btn()
        self.case_listing.delete_a_case(case_title)

    def test_add_command_task_on_click(self):
        case_title = Helper.random_string_generator()
        self.case_listing.create_a_case(case_title)
        self.case_details.add_command_task_on_click(
            name=Helper.random_string_generator(), data={"ip": "192.168.0.1"}
        )
        self.case_listing.click_case_details_back_btn()
        self.case_listing.delete_a_case(case_title)

    def tearDown(self):
        super().tearDown()
