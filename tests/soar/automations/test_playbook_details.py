from luna.driver.ui.wuibase import WUIBase
from src.base.base_config import BaseConfig
from src.constants import STREAMS_CREATION_TOAST_MSG
from src.page_objects.common.login import Login
from src.page_objects.soar.automations.playbook_details import PlaybookDetails
import random
import pytest
from src.page_objects.soar.automations.playbook_listing import PlaybookListing
from src.utils.helper import Helper


class PlaybookDetailsTestcase(WUIBase):
    def setUp(self):
        super().setUp()
        self.login_page = Login(page=self.page)
        self.login_page.login(
            username=BaseConfig.get_username(),
            password=BaseConfig.get_password(),
        )
        self.playbook_details = PlaybookDetails(page=self.page, launch=False)
        self.playbook_listing = PlaybookListing(page=self.page)

    def test_e2e_case1_playbook(self):
        """Create a replication of e2e Playbook Using IMAP, Extract, Split, SMTP, and Generate Output Node with Stream"""
        self.playbook_details.goto_automation_page()
        pb_name = Helper.random_string_generator_with_preset("Test_Playbook_")
        self.playbook_listing.create_a_playbook(pb_name)
        self.playbook_details.click_connect_to_tool_btn()
        self.playbook_details.add_imap_read_email_node("Inbox", "ALL")
        self.playbook_details.run_node_add_next_step()
        self.playbook_details.add_extract_json()
        self.playbook_details.run_node_add_next_step()
        self.playbook_details.add_split_array_node("$.URLs", "URL")
        self.playbook_details.run_node_add_next_step()
        self.playbook_details.add_smtp_node(
            "testvarun2611@gmail.com", "Playbook UI Test Email", "Playbook UI Test Email"
        )
        self.playbook_details.run_a_node_wait_for_result()
        self.playbook_details.add_output_node()
        stream_creation_msg = self.playbook_details.schedule_a_pb_stream(
            f"Test Stream {random.randint(10000, 9999999)}", "1", "0"
        )
        assert stream_creation_msg == STREAMS_CREATION_TOAST_MSG, "Stream creation assertion failed"

    def test_version_of_playbook_details(self):
        pb_name = Helper.random_string_generator_with_preset("Test_Playbook_Version_")
        self.playbook_details.goto_automation_page()
        self.playbook_listing.create_a_playbook(pb_name)
        self.playbook_details.click_connect_to_tool_btn()
        self.playbook_details.add_imap_read_email_node("Inbox", "ALL")
        self.playbook_details.run_node_add_next_step()
        self.playbook_details.add_split_array_node("$.result.URLs", "URL")
        self.playbook_details.run_a_node_wait_for_result()
        self.playbook_details.add_output_node()
        versions_list = self.playbook_details.get_versions_of_pb()
        assert len(versions_list) == 5, "Playbook versions list length assertion failed"

    def test_search_of_playbook(self):
        "Created IMAP and split array nodes, performed pb nodes search, and validated the results."
        pb_name = Helper.random_string_generator_with_preset("Test_Playbook_Search_")
        self.playbook_details.goto_automation_page()
        self.playbook_listing.create_a_playbook(pb_name)
        self.playbook_details.click_connect_to_tool_btn()
        self.playbook_details.add_imap_read_email_node("Inbox", "ALL")
        self.playbook_details.run_node_add_next_step()
        self.playbook_details.add_split_array_node("$.result.URLs", "URL")
        self.playbook_details.run_a_node_wait_for_result()
        search_result_length, search_results = self.playbook_details.pb_node_search("imap")
        assert search_result_length == "2 Nodes Found"
        for search in search_results:
            search_result_txt = search.text_content()
            assert "imap" in search_result_txt.lower(), (
                f"Assertion Failed: Expected imap in {search_result_txt.lower()}"
            )
            assert search_result_txt in [
                "IMAP_Read_Emails",
                "IMAP - Read Emails",
                "Internet Message Access Protocol is a standard protocol used by e-mail clients to retrieve messages from a mail server over a TCP/IP connection. IMAP is defined by RFC 3501",
                'splitArray(IMAP_Read_Emails, $.result.URLs, "URL")',
            ], "Assertion Failed"

    def test_streams_batch_data_from_pb(self):
        """Create a stream from a sql node playbook and validates batch data"""
        pb_name = Helper.random_string_generator_with_preset("Test_Playbook_Stream_Batch_Data_")
        self.playbook_details.goto_automation_page()
        self.playbook_listing.create_a_playbook(pb_name)
        self.playbook_details.click_connect_to_tool_btn()
        self.playbook_details.add_sql_node(
            "select 15 as col1,16 as col2 from _Start_Node union select 17 as col3,18 as col4 from _Start_Node"
        )
        self.playbook_details.add_output_node()
        stream_creation_msg = self.playbook_details.schedule_a_pb_stream(pb_name, "1", "0")
        assert stream_creation_msg == STREAMS_CREATION_TOAST_MSG, "Stream creation assertion failed"
        assert self.playbook_details.validate_batch_before_update() == "2", (
            "Batch detail before update assertion failed"
        )
        self.playbook_details.validate_batch_data_with_pb()

    def test_pb_version_publish(self):
        """Create a playbook with SQL node, output node, and stream. Update SQL node, publish the last state,
        and validate if the stream batches reflect the updates
        and the correct tag used for publishing data to the stream."""
        pb_name = Helper.random_string_generator_with_preset("Test_Playbook_Publish_")
        self.playbook_details.goto_automation_page()
        self.playbook_listing.create_a_playbook(pb_name)
        self.playbook_details.click_connect_to_tool_btn()
        self.playbook_details.add_sql_node(
            "select 15 as col1,16 as col2 from _Start_Node union select 17 as col3,18 as col4 from _Start_Node"
        )
        self.playbook_details.add_output_node()
        stream_name = Helper.random_string_generator_with_preset("Test_Stream_")
        stream_creation_msg = self.playbook_details.schedule_a_pb_stream(stream_name, "1", "0")
        assert stream_creation_msg == STREAMS_CREATION_TOAST_MSG, "Stream creation assertion failed"
        assert self.playbook_details.validate_batch_before_update() == "2", (
            "Batch detail before update assertion failed"
        )
        self.playbook_details.update_sql_node(
            "select 15 as col1,16 as col2 from _Start_Node union select 17 as col3,18 as col4 from _Start_Node LIMIT 0"
        )
        assert self.playbook_details.validate_batch_after_update() == "0", (
            "Batch detail after update assertion failed"
        )
        tag_text = self.playbook_details.publish_old_version()
        assert tag_text == "Used by stream", "Used by stream tag didnt updated"
        assert self.playbook_details.validate_batch_after_publish_update() == "2", (
            "Batch detail after publish old version assertion failed"
        )

    def test_pb_version_set_as_latest(self):
        """Create a playbook with sql and output node, click 'Set as Latest Version' on a node version,
        revert the version, and verify the node count decrease and reverted text on the latest node."""
        pb_name = Helper.random_string_generator_with_preset("Test_Playbook_Set_As_Latest_Version_")
        self.playbook_details.goto_automation_page()
        self.playbook_listing.create_a_playbook(pb_name)
        self.playbook_details.click_connect_to_tool_btn()
        self.playbook_details.add_sql_node(
            "select 15 as col1,16 as col2 from _Start_Node union select 17 as col3,18 as col4 from _Start_Node"
        )
        self.playbook_details.add_output_node()
        self.playbook_details.pb_node_set_as_latest_version(2)
        get_total_number_of_nodes = self.playbook_details.get_total_number_of_nodes()
        assert get_total_number_of_nodes == 2, "Playbook versions list length assertion failed"
        assert (
            self.playbook_details.validate_reverted_on_pb_version(1) == "#4 • Reverted to version 2"
        ), "Reverted version text assertion failed"

    def tearDown(self):
        self.playbook_details.delete_playbook()
        super().tearDown()
