from luna.driver.ui.wuibase import WUIBase
from src.base.base_config import BaseConfig
from src.page_objects.common.login import Login
from src.page_objects.soar.automations.command_details import CommandDetails
from src.page_objects.soar.case_management.case_listing import CaseListing
from src.page_objects.soar.case_management.case_details import CaseDetails
import pytest
import random
from src.constants import PARAMETER_NODE_DELETE_ERROR_MSG, PARAMETER_NODE_DELETE_DESCRIPTION_MSG
import requests
from src.utils.helper import Helper
from luna.config import config


class CommandDetailsTestCase(WUIBase):
    def setUp(self):
        super().setUp()
        self.login_page = Login(page=self.page)
        self.login_page.login(
            username=BaseConfig.get_username(),
            password=BaseConfig.get_password(),
        )
        self.command_details = CommandDetails(page=self.page)
        self.case_listing = CaseListing(page=self.page, launch=False)
        self.case_details = CaseDetails(page=self.page, launch=False)
        self.case_details = CaseDetails(page=self.page, launch=False)
        self.command_name = None

    def test_create_and_configure_command(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        ip = "121.1.2.3"
        url = "https://gitlab.com"
        self.command_details.add_parameter_node(
            "parameter0",
            "description0",
            "parameter1",
            "description1",
            "parameter2",
            "description2",
            "None",
            ip,
            url,
        )

        assert self.command_details.mark_command_done("parameter") == "Configured", (
            "Fail to configure command"
        )

    def test_edit_command_and_description(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = Helper.random_string_generator_with_preset("Command_Test_")
        self.command_name = command_name
        command_description = "This is ui test command"
        command_actual_name, command_actual_description = (
            self.command_details.add_command_name_description(command_name, command_description)
        )
        assert command_actual_name == command_name
        assert command_actual_description == command_description

    def test_e2e_command(self):
        ip = "121.1.2.3"
        url = "https://gitlab.com"
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        command_name = self.command_details.get_command_name_on_command_details()
        self.command_details.add_parameter_node(
            "parameter0",
            "description0",
            "parameter1",
            "description1",
            "parameter2",
            "description2",
            "None",
            ip,
            url,
        )
        self.command_details.add_arin_whois_node("$.parameter0")
        assert self.command_details.mark_command_done("arin") == "Configured", (
            "Fail to configure command"
        )
        self.case_listing.goto_case_listing_page()
        case_name = Helper.random_string_generator_with_preset("Test_Case_")
        self.case_listing.create_a_case(case_name)
        self.case_details.search_a_command_task(command_name)
        assert self.case_details.validate_command_visible_after_search() == command_name
        self.case_details.create_command_task_using_command()
        self.case_details.validate_command_run()

    def test_command_help_link(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        help_link = "https://testhelplink.com"
        self.command_details.save_help_link(help_link)
        assert self.command_details.help_link_save() == help_link, "Help link saved failed"

    def test_navigate_back_command_btn(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        assert (
            self.command_details.click_navigate_back_btn()
            == f"{config['servers']['soar']['url']}/#/automations/commands"
        ), "Navigate back command not working"

    def test_cannot_delete_error_msg_of_parameter_node(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        pn_error_msg, pn_error_description = (
            self.command_details.validate_delete_error_message_of_parameter_node()
        )
        assert pn_error_msg == PARAMETER_NODE_DELETE_ERROR_MSG, (
            "Parameter node delete error message failed"
        )
        assert pn_error_description == PARAMETER_NODE_DELETE_DESCRIPTION_MSG, (
            "Parameter node delete error description failed"
        )

    def test_work_in_progress_for_unconfigured_command(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        self.command_details.click_navigate_back_btn()
        assert self.command_details.get_created_command_status() == "Work in progress", (
            "Work in progress status failed"
        )

    def test_no_work_in_progress_command_visible_on_case_details(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        command_name = self.command_details.get_command_name_on_command_details()
        self.case_listing.goto_case_listing_page()
        case_name = Helper.random_string_generator_with_preset("Test_Case_")
        self.case_listing.create_a_case(case_name)
        self.case_details.search_a_command_task(command_name)
        assert self.case_details.validate_command_no_visible_after_search() == "No commands"

    def test_configured_command_visible_on_case_details(self):
        self.command_details.goto_automation_page()
        self.command_details.goto_tab_on_automation("Commands")
        command_name = self.command_details.create_command()
        self.command_name = command_name
        command_name = self.command_details.get_command_name_on_command_details()
        assert self.command_details.mark_command_done("parameter") == "Configured", (
            "Fail to configure command"
        )
        self.case_listing.goto_case_listing_page()
        case_name = Helper.random_string_generator_with_preset("Test_Case_")
        self.case_listing.create_a_case(case_name)
        self.case_details.search_a_command_task(command_name)
        assert self.case_details.validate_command_visible_after_search() == command_name

    def tearDown(self):
        self.command_details.command_api_cleanup(self.command_name)
        super().tearDown()
