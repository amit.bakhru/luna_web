from src.base.base_setup import DevoUEBATestCase
from src.flows.ueba.overview_flow import OverviewFlow


class Overview(DevoUEBATestCase):
    def setUp(self):
        super().setUp()
        self.overview_flow = OverviewFlow(page=self.page)

    def test_verify_navigation_and_filters_of_all_entities(self):
        self.overview_flow.verify_navigation_and_filters_of_all_entities()

    def test_open_and_verify_entity_from_overview_page(self):
        self.overview_flow.open_and_verify_entity_from_overview_page()
        self.top_users_link().wait_for(timeout=30)
