import pytest
from parameterized import parameterized_class

from luna.config import config

from luna.driver.ui.wuibase import WUIBase
from src.page_objects.common.login import Login

test_users = [
    {
        "username": config["servers"]["portal"]["username"],
        "password": config["servers"]["portal"]["password"],
    },
]


@parameterized_class(
    ("username", "password", "domain"),
    [
        (test_user["username"], test_user["password"], config["servers"]["portal"]["domain"])
        for test_user in test_users
    ],
)
@pytest.mark.smoketest
class AuthenticationTestCase(WUIBase):
    """
    Test cases related to authentication
    """

    def setUp(self):
        super().setUp()
        self.login_page = Login(page=self.page)

    def test_login(self):
        """Verify the login functionality works"""
        landing_page = self.login_page.login(
            username=self.username, password=self.password, domain=self.domain
        )
