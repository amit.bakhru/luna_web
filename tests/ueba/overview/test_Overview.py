from src.flows.ueba.overview_flow import OverviewFlow
from luna.driver.ui.wuibase import WUIBase


class OverviewTest(WUIBase):
    def setUp(self):
        super().setUp()
        self.overview_flow = OverviewFlow(self.page)

    def test_verify_navigation_and_filters_of_all_entities(self):
        self.overview_flow.verify_navigation_and_filters_of_all_entities()

    def test_open_and_verify_entity_from_overview_page(self):
        self.overview_flow.open_and_verify_entity_from_overview_page()
