from src.flows.ueba.content_manager_flow import ContentManagerFlow
import pytest
from luna.driver.ui.wuibase import WUIBase


class NotableEntitiesTest(WUIBase):
    def setUp(self):
        super().setUp()
        self.content_manager_flow = ContentManagerFlow(self.page)

    def test_add_and_verify_entity_to_notable_from_detail_page(self):
        self.content_manager_flow.add_and_verify_entity_to_notable_from_detail_page()

    def test_add_and_verify_entity_to_notable_from_listing_page(self):
        self.content_manager_flow.add_and_verify_entity_to_notable_from_listing_page()

    def test_add_delete_and_verify_entity_from_notable_detail_page(self):
        self.content_manager_flow.add_delete_and_verify_entity_from_notable_detail_page()

    @pytest.mark.skip("BAE-787 Bug reported related to upload")
    def test_upload_and_download_csv_notable_entity(self):
        self.content_manager_flow.upload_and_download_csv_on_notable_entities()
