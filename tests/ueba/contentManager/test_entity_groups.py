from luna.driver.ui.wuibase import WUIBase
from src.flows.ueba.content_manager_flow import ContentManagerFlow


class EntityGroupsTest(WUIBase):
    def setUp(self):
        super().setUp()
        self.content_manager_flow = ContentManagerFlow(self.page)
        self.risk_group_name = ""

    def test_add_and_edit_verify_new_entity_group(self):
        self.risk_group_name = self.content_manager_flow.add_and_edit_verify_new_entity_group()

    def test_add_and_verify_entity_to_group_from_listing_page(self):
        self.risk_group_name = (
            self.content_manager_flow.add_and_verify_entity_to_group_from_listing_page()
        )

    def test_add_and_verify_entity_to_group_from_detail_page(self):
        self.risk_group_name = (
            self.content_manager_flow.add_and_verify_entity_to_group_from_detail_page()
        )

    def test_edit_risk_score_multiplier_from_detail_page(self):
        self.content_manager_flow.edit_risk_score_multiplier_from_detail_page()
        self.risk_group_name = "skip"

    def test_add_delete_and_verify_entity_to_risk_group_from_detail_page(self):
        self.risk_group_name = (
            self.content_manager_flow.add_delete_and_verify_entity_to_risk_group_from_detail_page()
        )

    def tearDown(self):
        if not self.risk_group_name == "skip":
            self.content_manager_flow.delete_and_verify_entity_group(self.risk_group_name)
            super().tearDown()
