from luna.driver.ui.wuibase import WUIBase
from src.flows.ueba.content_manager_flow import ContentManagerFlow


class UseCasesTest(WUIBase):
    def setUp(self):
        super().setUp()
        self.content_manager_flow = ContentManagerFlow(self.page)

    def test_configure_and_disable_use_case(self):
        self.content_manager_flow.configure_and_disable_use_case()

    def test_configure_and_pause_use_case(self):
        self.content_manager_flow.configure_and_pause_use_case()

    def test_resume_a_pause_use_case(self):
        self.content_manager_flow.resume_a_pause_use_case()
