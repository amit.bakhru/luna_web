from src.flows.ueba.content_manager_flow import ContentManagerFlow
from luna.driver.ui.wuibase import WUIBase


class RiskBasedAlertDefinitionsTest(WUIBase):
    def setUp(self):
        super().setUp()
        self.content_manager_flow = ContentManagerFlow(self.page)

    def test_add_and_verify_risk_threshold_based_alert(self):
        self.risk_alert_name = self.content_manager_flow.add_and_verify_risk_threshold_based_alert()

    def test_add_and_verify_risk_rate_of_change_based_alert(self):
        self.risk_alert_name = (
            self.content_manager_flow.add_and_verify_risk_rate_of_change_based_alert()
        )

    def tearDown(self):
        self.content_manager_flow.delete_risk_based_alerts(self.risk_alert_name)
        super().tearDown()
