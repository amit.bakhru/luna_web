from src.flows.ueba.content_manager_flow import ContentManagerFlow
from luna.driver.ui.wuibase import WUIBase


class SecopsAlertsRiskScoreTest(WUIBase):
    def setUp(self):
        super().setUp()
        self.content_manager_flow = ContentManagerFlow(self.page)

    def test_edit_and_remove_verify_secops_alerts_risk_score(self):
        self.content_manager_flow.edit_and_remove_verify_secops_alerts_risk_score()
